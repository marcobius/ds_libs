"""

    12/2/20
    DeepSolutions
    David Perez
    
    
"""

import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="img_utils-deepsolutions", # Replace with your own username
    version="0.0.1",
    author="DeepSolutions",
    author_email="bcn@deepsolutions.io",
    description="NN detectors",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/marcobius/ds_libs",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.5',
    install_requires=[
        'opencv-python>=4.2.0',
        'opencv-contrib-python>=4.2.0',
        'numpy>=1.18.1'
    ],
)