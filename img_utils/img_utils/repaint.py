

import numpy as np



def colorize_grayscale(grayscale, colors):
    color_img = np.zeros((grayscale.shape[0], grayscale.shape[1], 3), dtype='uint8')
    for i in np.unique(grayscale):
        i = int(i)
        if i != 0:
            blob = np.where(grayscale==i)
            color = colors[i]
            color_img[:,:,0][blob] = color[0]
            color_img[:,:,1][blob] = color[1]
            color_img[:,:,2][blob] = color[2]
    return color_img