import cv2
import magic
import re
import numpy as np
import urllib.request
import libtorrent as lt
import time
import os

def get_image_shape(filename):
    # Fast implementation, only reads image metadata
    t = magic.from_file(filename)
    t = t.replace(' ','')
    file_info = t.split(',')
    success = False
    for info in file_info:
        numbers_attemp = info.split('x')
        if len(numbers_attemp) != 2:
            continue
        try:
            width = int(numbers_attemp[0])
            height = int(numbers_attemp[1])
            success = True
            break
        except:
            continue
    if not success:
        try:
            # Classical implementation, too slow because it has to read all the image
            im = cv2.imread(filename)
            shape = np.asarray(im.shape).astype(np.int32)
            return tuple(shape)
        except:
            print(file_info)
            raise(ValueError(f'Could not find the resolutions in image {filename}'))
    shape = (height, width, 3)
    return shape


def download_URL(url, destination_file):
    urllib.request.urlretrieve(url, destination_file)
    # folder, file = os.path.split(destination_file)
    # os.remove(os.path.join(folder, 'meta.bin'))


def download_torrent(magnet_link, destination_folder):
    ses = lt.session()
    ses.listen_on(6881, 6891)
    params = {
        'save_path': destination_folder,
        'storage_mode': lt.storage_mode_t(2)}
    link = magnet_link
    handle = lt.add_magnet_uri(ses, link, params)
    ses.start_dht()

    print ('downloading metadata...')
    while (not handle.has_metadata()):
        time.sleep(1)
    print('got metadata, starting torrent download...')
    while (handle.status().state != lt.torrent_status.seeding):
        s = handle.status()
        state_str = ['queued', 'checking', 'downloading metadata', \
                     'downloading', 'finished', 'seeding', 'allocating']
        print('%.2f%% complete (down: %.1f kb/s up: %.1f kB/s peers: %d) %s' % \
        (s.progress * 100, s.download_rate / 1000, s.upload_rate / 1000, \
         s.num_peers, state_str[s.state]))
        time.sleep(5)