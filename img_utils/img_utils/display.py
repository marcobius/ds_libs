import cv2
import numpy as np

def plot_one_box(img, coord, label=None, color=None, line_thickness=1):
    # tl = max(1,int(line_thickness) or int(round(0.001 * max(img.shape))))  # line thickness
    tl = int(line_thickness)
    c1, c2 = (int(coord[0]), int(coord[1])), (int(coord[2]), int(coord[3]))
    img = cv2.rectangle(img, c1, c2, color, thickness=tl)
    text_scale_factor=2
    if label:
        tf = max(tl - 2, 1)  # font thickness
        t_size = cv2.getTextSize(label, 0, fontScale=(float(tl) / 3)*text_scale_factor, thickness=tf)[0]
        c2 = c1[0] + t_size[0] + 15, c1[1] - t_size[1] - 3
        cv2.rectangle(img, c1, c2, color, -1)  # filled
        cv2.putText(img, label, (c1[0], c1[1] - 2), 0, (float(tl) / 3)*text_scale_factor, [1, 255, 1], thickness=tf, lineType=cv2.FONT_HERSHEY_SIMPLEX)

def display_image_gt(image, bboxes, class_labels, imshow=True, filename=None, size=(800, 600), waitkey=500):
    #TODO: fer que tingui un unic parametre Image, per fer-ho la imatge mare ha de tenir funcions per extreure les regions en format numpy
    """
    Muestra en pantalla y/o guarda una imagen y sus bboxes
    :param image: Opencv format en BGR+HWC acotada en [0..1]
    :param bboxes: XYXY sin acotar a 1
    :param class_labels: [str]
    :param imshow: bool
    :param filename: str
    :param size: tupla
    :return: None
    """
    # image = image[..., ::-1].copy()  # RGB --> BGR
    # img = (image * 255).astype(np.uint8)
    for i in range(len(bboxes)):
        x1, y1, x2, y2 = bboxes[i, :4]
        try:
            label = f'{class_labels[i]}'
        except:
            print('aqui')
        plot_one_box(image, [x1, y1, x2, y2], label=label, color=[125,0,255], line_thickness=1)
    if filename is not None:
        cv2.imwrite(filename, image*255)
        print(f'File saved to: {filename}')
    if imshow:
        img = cv2.resize(image, size)
        cv2.imshow('img', img)
        cv2.waitKey(waitkey)

def display_batch_gt(batch_im, batch_bb, batch_class_labels, waitkey=500, base_filename=None):
    #TODO: documentar
    batch_im = batch_im.numpy()
    batch_im = np.transpose(batch_im, (0,2,3,1))
    batch_im = batch_im[..., ::-1].copy()
    for i in range(len(batch_im)):
        img = batch_im[i].copy()
        if base_filename is None:
            display_image_gt(img, batch_bb[i], batch_class_labels[i], waitkey=waitkey, size=(1024,768))
        else:
            display_image_gt(img, batch_bb[i], batch_class_labels[i], waitkey=waitkey, filename=f'{base_filename}_{i}.jpg')
