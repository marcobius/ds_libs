"""

    6/5/20
    DeepSolutions

"""

import setuptools

# with open("DEPLOY.md", "r") as fh:
#     long_description = fh.read()

long_description = "Repositori de llibreries de Deep Solutions.  "

with open("requirements.txt") as fh:
    install_requires = fh.readlines()
install_requires = [x.strip() for x in install_requires]

extras = {
    'torch': ['torch>=1.3.0', 'torchvision>=0.6.0'],
    'albumentations': ['albumentations>=0.4.5'],
    'other_extras': ['awscli', 'python-libtorrent-bin']
}

# pip install git+https://gitlab.com/marcobius/ds_libs.git

setuptools.setup(
    name="ds_lib",
    version="0.9.13",
    author="DeepSolutions",
    author_email="bcn@deepsolutions.io",
    description="Dataset utils",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/marcobius/ds_libs",
    packages=setuptools.find_packages(include=['ds.dataset', 'ds.img_utils', 'ds.dataset.res'], exclude=['ds.tests', 'detectors', 'monitoring', 'modules']),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6.9',
    install_requires=install_requires,
    # extras_require={
    #     'extra':[install_requires]
    # },
    entry_points={
        'console_scripts': ['video2via=ds.dataset.video2via:main']
    },
    package_data={'':['*.html']},
    # data_files=[('ds/dataset/res', ['via11.html'])]
    extras_require=extras
)
#TODO: minimitzar els paquets instalats i pasar a extra la resta (activar opcio extras_require)