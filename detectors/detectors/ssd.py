
from detectors.base import Detector
from torch.hub import load
from torch import no_grad, squeeze
from detectors.vision.ssd.vgg_ssd import create_vgg_ssd, create_vgg_ssd_predictor
from detectors.vision.ssd.mobilenetv1_ssd import create_mobilenetv1_ssd, create_mobilenetv1_ssd_predictor
from detectors.vision.ssd.mobilenetv1_ssd_lite import create_mobilenetv1_ssd_lite, create_mobilenetv1_ssd_lite_predictor
from detectors.vision.ssd.squeezenet_ssd_lite import create_squeezenet_ssd_lite, create_squeezenet_ssd_lite_predictor
from detectors.vision.ssd.mobilenet_v2_ssd_lite import create_mobilenetv2_ssd_lite, create_mobilenetv2_ssd_lite_predictor
from detectors.vision.utils.misc import Timer
import cv2
import sys

class SsdResnet(Detector):
    def __init__(self, backbone = 'resnet50', input_shape = (3,3,300,300), precision = 'fp32', input_names = ['input'], output_names = ['output', '646']):
        Detector.__init__(self)
        self.backbone = backbone # Other possibilities: resnet18 resnet34 resnet50 resnet101 resnet152
        self.input_names = input_names
        self.output_names = output_names
        self.input_shape = input_shape
        # https://pytorch.org/hub/nvidia_deeplearningexamples_ssd/
        self.network = 'ssd'
        self.model = load('NVIDIA/DeepLearningExamples:torchhub', 'nvidia_ssd', model_math=precision, backbone = backbone)
        self.model.to('cuda')
        self.model.contains()

class SsdMobilenet(Detector):
    def __init__(self, filename ='models/mb2-ssd-lite-mp-0_686.pth', backbone = 'mb2-ssd-lite', input_shape = (1,3,300,300), len_output = 21, input_names = ['input.1'], output_names = ['scores', 'boxes']):
        Detector.__init__(self)
        self.backbone = backbone
        self.input_names = input_names
        self.output_names = output_names
        self.input_shape = input_shape
        # https://github.com/qfgaohao/pytorch-ssd
        if backbone == 'vgg16-ssd':
            self.model = create_vgg_ssd(len_output, is_test=True)
        elif backbone == 'mb1-ssd':
            self.model = create_mobilenetv1_ssd(len_output, is_test=True)
        elif backbone == 'mb1-ssd-lite':
            self.model = create_mobilenetv1_ssd_lite(len_output, is_test=True)
        elif backbone == 'mb2-ssd-lite':
            self.model = create_mobilenetv2_ssd_lite(len_output, is_test=True)
        elif backbone == 'sq-ssd-lite':
            self.model = create_squeezenet_ssd_lite(len_output, is_test=True)
        else:
            print("The backbone type is wrong. It should be one of vgg16-ssd, mb1-ssd, mb1-ssd-lite, mb2-ssd-lite or sq-ssd-lite.")
            sys.exit(1)
        self.model.load(filename)
        # self.model.to('cuda')
        self.model.contains()
        if self.backbone == 'vgg16-ssd':
            self.predictor = create_vgg_ssd_predictor(self.model, candidate_size=200)
        elif self.backbone == 'mb1-ssd':
            self.predictor = create_mobilenetv1_ssd_predictor(self.model, candidate_size=200)
        elif self.backbone == 'mb1-ssd-lite':
            self.predictor = create_mobilenetv1_ssd_lite_predictor(self.model, candidate_size=200)
        elif self.backbone == 'mb2-ssd-lite':
            self.predictor = create_mobilenetv2_ssd_lite_predictor(self.model, candidate_size=200)
        elif self.backbone == 'sq-ssd-lite':
            self.predictor = create_squeezenet_ssd_lite_predictor(self.model, candidate_size=200)
        else:
            print("The backbone type is wrong. It should be one of vgg16-ssd, mb1-ssd and mb1-ssd-lite.")
            sys.exit(1)


    def save_pt(self, filename = 'model.pt'):
        self.model.save(filename)

    def detect(self, inputs):
        ret = []
        with no_grad():
            for input_pt in inputs:
                # input_np = input_pt.detach().cpu().numpy() if input_pt.requires_grad else input_pt.cpu().numpy()
                # ret.append(self.predictor.predict(input_pt, 10, 0.4))
                ret.append(self.predictor.predict(input_pt))
        return ret
