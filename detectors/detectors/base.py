"""

    3/9/19
    DeepSolutions
    David Perez
    
    
"""
from detectors.internals.bbox import Bbox
import cv2
import numpy as np
import torch
from torchsummary import summary
from torch import no_grad

from matplotlib.path import Path
# from mahotas.polygon import fill_polygon

class Detector(object):
    '''
    Main parent class for detectors.
    Sets the rule to what they need to have.
    '''
    def __init__(self):
        self.model = None
        self.network = None
        self.backbone = None
        self.input_shape = None
        self.input_names = None
        self.output_names = None

    def detect(self, inputs):
        with no_grad():
            return self.model(inputs)

    def save_pt(self, filename = 'model.pt'):
        torch.save(self.model, filename)

    def load_pt(self, filename ='model.pt'):
        self.model = torch.load(filename)

    def save_onnx(self, filename ='model.onnx'):
        dummy_input = torch.randn(self.input_shape)
        if next(self.model.parameters()).is_cuda:
            dummy_input = dummy_input.cuda()
        dynamic_axes = {} # variable lenght axes
        for input_name in self.input_names:
            dynamic_axes[input_name] = {0: 'batch_size'}
        for output_name in self.output_names:
            dynamic_axes[output_name] = {0: 'batch_size'}
        torch.onnx.export(self.model,  # model being run
                          dummy_input,  # model input (or a tuple for multiple inputs)
                          filename,  # where to save the model (can be a file or file-like object)
                          export_params=True,  # store the trained parameter weights inside the model file
                          opset_version=10,  # the ONNX version to export the model to
                          do_constant_folding=True,  # whether to execute constant folding for optimization
                          input_names=self.input_names,  # the model's input names
                          output_names=self.output_names,
                          dynamic_axes=dynamic_axes,
                          verbose=True)
        # TODO find a way to retrieve input and output names

    def load_onnx(self, filename = 'model.onnx'):
        import onnxruntime as rt
        self.onnx_session = rt.InferenceSession(filename)

        from onnx import load
        self.model_onnx = load(filename)

        # # NO VA
        # from onnx.checker import check_model
        # check_model(self.model_onnx)

    def check_onnx(self):
        from onnx.checker import check_model
        check_model(self.model_onnx)

    def detect_onnx(self, numpy_inputs):
        inputs = self.onnx_session.get_inputs()
        assert len(inputs) == 1, 'Expecting only one input'
        # assert list(numpy_inputs.shape) == inputs[0].shape, 'Input dimensions are probably wrong'
        input_dict = {}
        for input in inputs:
            input_dict[input.name] = numpy_inputs

        # return self.onnx_session.run(None, input_dict)
        return self.onnx_session.run(self.output_names, input_dict)
        for i in range(len(prediction)):
            prediction[i] = np.transpose(prediction[i], axes=[0,2,1])
        return prediction
        # Running with tf backend
        # from onnx_tf.backend import prepare
        # return prepare(self.onnx_model).run(input)

    def save_keras(self, filename = 'model.h5'):
        from onnx2keras import onnx_to_keras, check_torch_keras_error
        self.model_keras = onnx_to_keras(self.model_onnx, self.input_names)
        self.model_keras.save(filename)

        # # NO VA
        # from detectors.inputs.load import dummy_images
        # inputs, torch_inputs, numpy_inputs = dummy_images()
        # error = check_torch_keras_error(self.model.to('cpu'), model_keras, numpy_inputs)
        # print('Error: {0}'.format(error))  #  1e-6 :)

    def load_keras(self, filename = 'model.h5'):
        import tensorflow as tf
        self.model_keras = tf.keras.models.load_model(filename, compile=False)

    def detect_keras(self, numpy_inputs):
        return self.model_keras.predict(numpy_inputs, steps=1)

    def save_tf(self, filename = 'model.pb'):
        from onnx_tf.backend import prepare
        self.model_tf = prepare(self.model_onnx)
        # self.model_tf = prepare(self.model_onnx, include_optimizer=True)
        self.model_tf.export_graph(filename)
        print(self.model_tf)

    def load_tf(self, filename = 'model.pb'):
        import tensorflow as tf
        with tf.io.gfile.GFile(filename, 'rb') as f:
            graph_def = tf.compat.v1.GraphDef()
            graph_def.ParseFromString(f.read())
        with tf.Graph().as_default() as graph:
            tf.import_graph_def(graph_def, name='')
            # tf.compat.v1.MetaGraphDef
        self.model_tf = graph
        # self.model_tf = tf.saved_model.load(filename)

    def detect_tf(self, numpy_inputs):
        inputs = []
        for name in self.input_names:
            inputs.append(self.model_tf.get_tensor_by_name(name+':0'))
        outputs = []
        for name in self.output_names:
            outputs.append(self.model_tf.get_tensor_by_name(name+':0'))

        # Run TensorFlow model and see if it makes sense what it infers
        import tensorflow as tf
        sess = tf.compat.v1.Session(graph=self.model_tf)
        return sess.run(outputs, feed_dict={inputs: numpy_inputs})

    def print_tf(self):
        # Show tensor names in graph
        for op in self.model_tf.get_operations():
            text = op.values()
            print(text)

    def load_tf_2_tflite(self, filename='model.pb'):
        import tensorflow as tf
        converter = tf.lite.TFLiteConverter.from_frozen_graph(filename, input_arrays=self.input_names, output_arrays=self.output_names)
        self.model_tflite = converter.convert()

    def save_tflite(self, filename='model.tflite'):
        open(filename, "wb").write(self.model_tflite)

    def print(self):
        print(self.model)
        # summary(self.model, self.input_shape)

class DetectorFromFile(Detector):
    """
    Clase para importar detecciones realizadas en batch a priori por un detector.
    Estas detecciones deben estar en formato csv (separado por comas)
    Si se requiere que calcule el feature de una deteccion se puede setear el campo 'featureCalculator' con la
    referencia de la función de cálculo de la feature
    """

    def __init__(self, filter=None):
        self.frame_dict = None
        self.featureCalculator = None
        self.patch_shape = None
        self.filter = filter
        self.first_frame = 0

    def setFilename(self, filename):
        """
        Setea el fichero que contiene las detecciones y precarga un diccionario con las mismas
        :param filename: nombre del fichero tipo csv con las detecciones
        :return: None
        """
        with open(filename) as f:
            d = f.readlines()
        # 0 frame
        # 1 class_id
        # 2 class_confidence
        # 3 l
        # 4 t
        # 5 w
        # 6 h
        # 7 object_confidence
        # 8:88 probabilities vector

        d = list(map(lambda x: x.strip(), d))
        last_frame = int(d[-1].split(',')[0])
        frame_dict = {x: [] for x in range(last_frame + 1)}
        self.first_frame=last_frame
        for i in range(len(d)):
            a = list(d[i].split(','))
            a = list(map(float, a))
            frame = a[0]
            self.first_frame = int(min(self.first_frame, frame))
            class_id = a[1]
            class_confidence = a[2]
            ltwh = a[3:7]
            object_confidence = a[7]
            feature = a[8:88] # esta posicion hasta ahora era un -1
            frame_dict[frame].append({'class_id': class_id, 'class_conf': class_confidence, 'ltwh': ltwh, 'obj_conf': object_confidence, 'feature': feature})

        self.frame_dict = frame_dict

    def get_detections(self, frame_id):
        if self.frame_dict is None:
            return None
        if frame_id not in self.frame_dict.keys() or self.frame_dict[frame_id] == []:
            return []
        frame_info = self.frame_dict[frame_id]
        detections = []
        for i in range(len(frame_info)):
            class_id = frame_info[i]['class_id']
            class_conf = frame_info[i]['class_conf']
            ltwh = frame_info[i]['ltwh']
            obj_conf = frame_info[i]['obj_conf']
            if frame_info[i]['feature'] != -1:
                feature = np.array(frame_info[i]['feature'])
            else:
                feature = None
            detection = Bbox(class_id, class_conf, ltwh, obj_conf, feature, frame_number=frame_id)
            if self.filter is None:
                detections.append(detection)
            else:
                if self.filter(detection):
                    detections.append(detection)
        return detections

    def detect(self, frame, frame_id):
        """
        Recibe un frame y saca del fichero las detecciones correspondientes al frame
        :param frame:
        :param frame_id: Current frame number
        :return: Lista de detecciones
        """
        detections = self.get_detections(frame_id)
        if detections is not None:
            if self.featureCalculator is not None:
                for detection in detections:
                    if detection.feature is None:
                        sx, sy, w, h = detection.get_ltwh().astype(np.uint32)
                        ex = sx + w
                        ey = sy + h
                        img = frame[sy:ey, sx:ex]
                        shape = (self.patch_shape[1], self.patch_shape[0])
                        img = cv2.resize(img, shape)
                        # cv2.imshow('deteccion', img)
                        # cv2.waitKey()
                        patches = []  # la función espera un array de patches para hacer batch
                        patches.append(img)
                        # patches = np.asarray(patches)
                        # feature = self.featureCalculator(patches).squeeze()
                        features = self.featureCalculator.genFeatures(patches)
                        detection.feature = features.squeeze()
            # detections = self.filter_detections_outliers(detections) #TODO: esto elimina detecciones, si el detector funciona bien, se puede eliminar
        return detections

    def filter_detections_outliers(self, detections, low=.02, high=.98):
        #TODO: optimizar, demasiados recorridos a detecciones
        sizes = []
        filtered = []
        for d in detections:
            x, y, w, h = d.get_ltwh()
            sizes.append(w*h)
        if len(sizes) == 0:
            print('frame sin detecciones!')
            # logger.debug('frame sin detecciones')
        else:
            q1 = np.quantile(np.asarray(sizes), low)
            q3 = np.quantile(sizes, high)
            for d in detections:
                size = d.get_ltwh()[2] * d.get_ltwh()[3]
                if (q1 < size < q3):
                    filtered.append(d)
        return filtered








