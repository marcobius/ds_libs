

from detectors.base import Detector
from torch.hub import load


class MobilenetV2(Detector):
    def __init__(self, precision = 'fp32'):
        Detector.__init__(self)
        # https://pytorch.org/hub/pytorch_vision_mobilenet_v2/

        self.model = load('pytorch/vision:v0.5.0', 'mobilenet_v2', pretrained=True)
        self.model.to('cuda')
        self.model.contains()

