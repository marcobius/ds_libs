
from detectors.ssd import SsdResnet, SsdMobilenet
from detectors.inputs.load import dummy_images
from detectors.display.base import display_results
from detectors.transformations.transform import decode_pt, tuplept_2_tuplenp

inputs_cv2, inputs_pt, inputs_np = dummy_images(precision = 'fp32')
# inputs_cv2, inputs_pt, inputs_np = dummy_images(precision ='fp32', num_inputs=1)

################### PYTORCH ####################
# https://pytorch.org/hub/nvidia_deeplearningexamples_ssd/
# detector = SsdResnet(backbone = 'resnet50', precision = 'fp32')
# https://storage.googleapis.com/models-hao/mobilenet-v1-ssd-mp-0_675.pth
# https://storage.googleapis.com/models-hao/mb2-ssd-lite-mp-0_686.pth
# https://storage.googleapis.com/models-hao/mb2-imagenet-71_8.pth
# https://storage.googleapis.com/models-hao/vgg16-ssd-mp-0_7726.pth
# detector = SsdMobilenet(filename='models/mobilenet-v1-ssd-mp-0_675.pth', backbone = 'mb1-ssd', len_output = 21)
# detector = SsdMobilenet(filename='', backbone = 'mb1-ssd-lite', len_output = 21) # NO VA, falta xarxa entrenada
detector = SsdMobilenet(filename='models/mb2-ssd-lite-mp-0_686.pth', backbone = 'mb2-ssd-lite', len_output = 21)
# detector = SsdMobilenet(filename='models/mb2-imagenet-71_8.pth', backbone = 'mb2-ssd-lite', len_output = 21)
# detector = SsdMobilenet(filename='models/vgg16_reducedfc.pth', backbone = 'vgg16-ssd', len_output = 21)

# detector.save_pt('model.pt')
# detector.load_pt('model.pt')

prediction_pt = detector.detect(inputs_pt)
f_pred_pt = decode_pt(prediction_pt)
display_results(f_pred_pt, inputs_cv2)

# detector.print()

################### ONNX ####################
detector.save_onnx('model.onnx')
detector.load_onnx('model.onnx')
detector.check_onnx()

prediction_onnx = tuplept_2_tuplenp(detector.detect_onnx(inputs_np))
f_pred_onnx = decode_pt(prediction_onnx)
display_results(f_pred_onnx, inputs_cv2)

################### KERAS ####################
# detector.save_keras('model.h5')
# detector.load_keras('model.h5')
# NO VA
# display_results(decode_np(detector.detect_keras(numpy_inputs)), inputs)

################### TENSORFLOW ####################
detector.save_tf('model.pb')
# detector.load_tf('model.pb')

prediction_tf = tuplept_2_tuplenp(detector.detect_onnx(inputs_np))
f_pred_tf = decode_pt(prediction_tf)
display_results(f_pred_tf, inputs_cv2)

################### TFLITE ####################
detector.load_tf_2_tflite('model.pb')
detector.save_tflite('model.tflite')
