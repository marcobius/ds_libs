import tensorflow as tf
from tensorflow.python.tools import optimize_for_inference_lib

graph_def_file = './model/saved_model.pb'

tf.reset_default_graph()
graph_def = tf.GraphDef()
with tf.Session() as sess:
    # Read binary pb graph from file
    with tf.gfile.Open(graph_def_file, "rb") as f:
        data2read = f.read()
        graph_def.ParseFromString(data2read)
    tf.graph_util.import_graph_def(graph_def, name='')

    # Get Nodes
    conv_nodes = []
    other_types = []
    for n in sess.graph.get_operations():
        if n.type in ['Conv2D', 'MaxPool', 'AvgPool']:
            conv_nodes.append(n)
        else:
            if n.type not in other_types:
                print(n.type)
                other_types.append(n.type)
    # conv_nodes = [n for n in sess.graph.get_operations() if n.type in ['Conv2D', 'MaxPool', 'AvgPool']]
    for node in conv_nodes:
        # Transpose input
        assert len(node.inputs) == 1 or len(node.inputs) == 2
        org_inp_tens = sess.graph.get_tensor_by_name(node.inputs[0].name)
        inp_tens = tf.transpose(org_inp_tens, [0, 2, 3, 1], name=node.name + '_transp_input')
        op_inputs = [inp_tens]

        # Get filters for Conv but don't transpose
        if node.type == 'Conv2D':
            org_filter_tens = sess.graph.get_tensor_by_name(node.inputs[1].name)
            # filter_tens = tf.transpose(org_filter_tens, [1,0,3,2], name=node.name + '_transp_filter')
            # op_inputs.append(filter_tens)
            op_inputs.append(org_filter_tens)

        # Attributes without data_format, NWHC is default
        atts = {key: node.node_def.attr[key] for key in list(node.node_def.attr.keys()) if key not in ['data_format']}
        st = atts['strides'].list.i
        stl = [st[0], st[2], st[3], st[1]]
        atts['strides'] = tf.AttrValue(list=tf.AttrValue.ListValue(i=stl))
        if node.type in ['MaxPool', 'AvgPool']:
            kl = atts['ksize'].list.i
            ksl = [kl[0], kl[2], kl[3], kl[1]]
            atts['ksize'] = tf.AttrValue(list=tf.AttrValue.ListValue(i=ksl))
            # st = atts['strides'].list.i
            # stl = [st[0], st[2], st[3], st[1]]
            # atts['strides'] = tf.AttrValue(list=tf.AttrValue.ListValue(i=stl))

        # Create new Operation
        print(node.type, node.name, list(node.inputs), node.node_def.attr['data_format'])
        op = sess.graph.create_op(op_type=node.type, inputs=op_inputs, name=node.name + '_new', attrs=atts)
        out_tens = sess.graph.get_tensor_by_name(node.name + '_new:0')
        out_trans = tf.transpose(out_tens, [0, 3, 2, 1], name=node.name + '_transp_out')
        # assert out_trans.shape == sess.graph.get_tensor_by_name(node.name + ':0').shape
        print(out_trans.shape, sess.graph.get_tensor_by_name(node.name + ':0').shape)
        # assert out_trans.shape.ndims == sess.graph.get_tensor_by_name(node.name + ':0').shape.ndims
        # for i in range(out_trans.shape.ndims):
        #     assert out_trans.shape.dims[i].is_compatible_with(sess.graph.get_tensor_by_name(node.name + ':0').shape.dims[i])

        # Update Connections
        out_nodes = [n for n in sess.graph.get_operations() if node.outputs[0] in n.inputs]
        for out in out_nodes:
            for j, nam in enumerate(out.inputs):
                if node.outputs[0] == nam:
                    out._update_input(j, out_trans)

    # Delete old nodes
    graph_def = sess.graph.as_graph_def()
    for on in conv_nodes:
        graph_def.node.remove(on.node_def)

    # Write graph
    tf.io.write_graph(graph_def, "", graph_def_file.rsplit('.', 1)[0] + '_toco.pb', as_text=False)