import tensorflow as tf
# converter = tf.compat.v1.lite.TFLiteConverter.from_saved_model('./../ds_keras-yolov3-model-set/tf_model')
converter = tf.compat.v1.lite.TFLiteConverter.from_frozen_graph('./../ds_keras-yolov3-model-set/tf_model/saved_model.pb', input_arrays=['image_input'] , output_arrays=['prediction_26', 'prediction_52'])
tflite_model = converter.convert()
open("saved_model.tflite", "wb").write(tflite_model)