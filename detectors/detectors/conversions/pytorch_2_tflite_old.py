import torch

# https://pytorch.org/hub/pytorch_vision_mobilenet_v2/
# mobilenet_model = torch.hub.load('pytorch/vision:v0.5.0', 'mobilenet_v2', pretrained=True)
# mobilenet_model.to('cuda')
# mobilenet_model.eval()
precision = 'fp32'
model_pt = torch.hub.load('NVIDIA/DeepLearningExamples:torchhub', 'nvidia_ssd', model_math=precision)
model_pt.to('cuda')
model_pt.contains()
utils = torch.hub.load('NVIDIA/DeepLearningExamples:torchhub', 'nvidia_ssd_processing_utils')
uris = [
    'http://images.cocodataset.org/val2017/000000397133.jpg',
    'http://images.cocodataset.org/val2017/000000037777.jpg',
    'http://images.cocodataset.org/val2017/000000252219.jpg'
]
inputs = [utils.prepare_input(uri) for uri in uris]
torch_inputs = utils.prepare_tensor(inputs, precision == 'fp16')
numpy_inputs = torch_inputs.detach().cpu().numpy() if torch_inputs.requires_grad else torch_inputs.cpu().numpy()
with torch.no_grad():
    detections_batch_pt = model_pt(torch_inputs)

from detectors.display.base import display_results
from detectors.transformations.transform import decode_pt

display_results(decode_pt(detections_batch_pt), inputs)

# Export to ONNX format
# torch.onnx.export(model_pt, torch_inputs, './model/saved_model.onnx',
#                   input_names=['input'],
#                   output_names=['output'])
# torch.onnx.export(model_pt, torch_inputs, './model/saved_model.onnx',
#                   export_params=True,
#                   input_names=['input'],
#                   output_names=['output'],
#                   verbose=True)
torch.onnx.export(model_pt,  # model being run
                  torch_inputs,  # model input (or a tuple for multiple inputs)
                  'model.onnx',  # where to save the model (can be a file or file-like object)
                  export_params=True,  # store the trained parameter weights inside the model file
                  opset_version=10,  # the ONNX version to export the model to
                  do_constant_folding=True,  # whether to execute constant folding for optimization
                  input_names = ['input'],  # the model's input names
                  output_names = ['output', '646'],  # the model's output names
                  dynamic_axes={'input' : {0 : 'batch_size'},    # variable lenght axes
                                'output' : {0 : 'batch_size'}})

# Load model and make inference
import onnxruntime as rt
sess = rt.InferenceSession('model.onnx')
input_name = sess.get_inputs()[0].name
detections_batch_onx = sess.run(None, {input_name: numpy_inputs})[0]
from detectors.transformations.transform import decode_np
display_results(decode_pt(detections_batch_pt), inputs)

# Load ONNX model and convert to TensorFlow format
from onnx import load
model_onnx = load('model.onnx')
from onnx2keras import onnx_to_keras, check_torch_keras_error
model_keras = onnx_to_keras(model_onnx, ['input'])
error = check_torch_keras_error(model_pt.to('cpu'), model_keras, numpy_inputs)
print('Error: {0}'.format(error))  #  1e-6 :)

from onnx.checker import check_model
check_model(model_onnx)
from onnx_tf.backend import prepare
tf_rep = prepare(model_onnx)

# Export model as .pb file
tf_rep.export_graph('model.pb')


import tensorflow as tf
def load_pb(path_to_pb):
    with tf.io.gfile.GFile(path_to_pb, 'rb') as f:
        graph_def = tf.compat.v1.GraphDef()
        graph_def.ParseFromString(f.read())
    with tf.Graph().as_default() as graph:
        tf.import_graph_def(graph_def, name='')
        # tf.compat.v1.MetaGraphDef
        return graph

# model_tf2 = tf.saved_model.load('./model/saved_model.pb')
model_tf = load_pb('model.pb')
sess = tf.compat.v1.Session(graph=model_tf)

# Show tensor names in graph
# for op in tf_graph.get_operations():
#     text = op.values()
#     print(text)

output_tensor_4 = model_tf.get_tensor_by_name('output:0')
output_tensor_81 = model_tf.get_tensor_by_name('646:0')
input_tensor = model_tf.get_tensor_by_name('input:0')

# Run TensorFlow model and see if it makes sense what it infers
detections_batch_tf = sess.run([output_tensor_4, output_tensor_81], feed_dict={input_tensor: numpy_inputs})
# display_results(decode((torch.from_numpy(detections_batch_tf[0]),torch.from_numpy(detections_batch_tf[1]))), inputs)
# print(detections_batch_tf)

# Convert model to TF-LITE
# Image information
HEIGHT = 300
WIDTH = 300
CHANNELS = 3
IMAGE_SHAPE = (HEIGHT, WIDTH)
# import subprocess
# subprocess.run(['saved_model_cli', 'show', '--dir', './model/'])
# subprocess.run(['netron'])
# converter = tf.lite.TFLiteConverter.from_saved_model(saved_model_dir='./model', signature_keys= ['input:0', '646:0', 'output:0'], tags = ['input:0', '646:0', 'output:0'])
converter = tf.compat.v1.lite.TFLiteConverter.from_saved_model('./model')

# converter = tf.lite.TFLiteConverter(sess.graph, ['input:0', '646:0', 'output:0'])
# converter = tf.lite.TFLiteConverter(model_tf, ['input:0', '646:0', 'output:0'])
# converter = tf.lite.TFLiteConverter(model_tf, input_tensors = [numpy_inputs], output_tensors = [output_tensor_4, output_tensor_81])

converter = tf.compat.v1.lite.TFLiteConverter(graph_def = model_tf, input_tensors = [numpy_inputs], output_tensors = [output_tensor_4, output_tensor_81])
converter = tf.compat.v1.lite.TFLiteConverter.from_session(sess, input_tensors = [numpy_inputs], output_tensors = [output_tensor_4, output_tensor_81])
tflite_model = converter.convert()
open("model.tflite", "wb").write(tflite_model)