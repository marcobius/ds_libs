
# Just loading some stupid images for testing

from torch.hub import load


def dummy_images(precision='fp32', num_inputs=None):
    uris = [
        'http://images.cocodataset.org/val2017/000000397133.jpg',
        'http://images.cocodataset.org/val2017/000000037777.jpg',
        'http://images.cocodataset.org/val2017/000000252219.jpg'
    ]
    utils = load('NVIDIA/DeepLearningExamples:torchhub', 'nvidia_ssd_processing_utils')
    inputs = [utils.prepare_input(uri) for uri in uris]
    if num_inputs is not None:
        old_inputs = inputs
        inputs = []
        for i in range(num_inputs):
            inputs.append(old_inputs[i])
    torch_inputs = utils.prepare_tensor(inputs, precision == 'fp16')
    numpy_inputs = torch_inputs.detach().cpu().numpy() if torch_inputs.requires_grad else torch_inputs.cpu().numpy()
    return inputs, torch_inputs, numpy_inputs