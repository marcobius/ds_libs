import torch
import numpy as np

from detectors.filters.base import confidence

def decode_pt(detections_batch_pt):
    utils = torch.hub.load('NVIDIA/DeepLearningExamples:torchhub', 'nvidia_ssd_processing_utils')
    results_per_input = utils.decode_results(detections_batch_pt)
    # results_per_input2 = decode_results(detections_batch_pt)

    best_results_per_input = confidence(results_per_input, threshold = 0.40)
    # best_results_per_input2 = confidence(results_per_input2, threshold = 0.40)
    return best_results_per_input

# def decode_results(detections):
#     utils = torch.hub.load('NVIDIA/DeepLearningExamples:torchhub', 'nvidia_ssd_processing_utils')
#     assert len(detections) == 2, 'No clue how to decode this input'
#     boxes = detections[0].cpu().numpy()
#     assert boxes.shape[1] == 4, '4 floats are needed to define a box'
#     scores = detections[1].cpu().softmax(dim=1).numpy()
#     results_per_input = []
#     for i in range(boxes.shape[0]):
#         boxes_im = boxes[i,:,:]
#         labels_im = np.argmax(scores[i,:,:], axis=0)
#         ind = list(range(labels_im.shape[0]))
#         scores_im = scores[i,labels_im,ind]
#         results_per_input.append([boxes_im.transpose(),labels_im,scores_im])
#     return results_per_input

# def xywh_2_ltrb(bboxes_in):
#     """
#         Do scale and transform from xywh to ltrb
#         suppose input Nx4xnum_bbox Nxlabel_numxnum_bbox
#     """
#     if bboxes_in.device == torch.device("cpu"):
#         self.dboxes = self.dboxes.cpu()
#         self.dboxes_xywh = self.dboxes_xywh.cpu()
#     else:
#         self.dboxes = self.dboxes.cuda()
#         self.dboxes_xywh = self.dboxes_xywh.cuda()
#
#     bboxes_in = bboxes_in.permute(0, 2, 1)
#     scores_in = scores_in.permute(0, 2, 1)
#
#     bboxes_in[:, :, :2] = self.scale_xy * bboxes_in[:, :, :2]
#     bboxes_in[:, :, 2:] = self.scale_wh * bboxes_in[:, :, 2:]
#
#     bboxes_in[:, :, :2] = bboxes_in[:, :, :2] * self.dboxes_xywh[:, :, 2:] + self.dboxes_xywh[:, :, :2]
#     bboxes_in[:, :, 2:] = bboxes_in[:, :, 2:].exp() * self.dboxes_xywh[:, :, 2:]
#
#     # Transform format to ltrb
#     l, t, r, b = bboxes_in[:, :, 0] - 0.5 * bboxes_in[:, :, 2], \
#                  bboxes_in[:, :, 1] - 0.5 * bboxes_in[:, :, 3], \
#                  bboxes_in[:, :, 0] + 0.5 * bboxes_in[:, :, 2], \
#                  bboxes_in[:, :, 1] + 0.5 * bboxes_in[:, :, 3]
#
#     bboxes_in[:, :, 0] = l
#     bboxes_in[:, :, 1] = t
#     bboxes_in[:, :, 2] = r
#     bboxes_in[:, :, 3] = b
#
#     return bboxes_in, F.softmax(scores_in, dim=-1)


def tuplept_2_tuplenp(detections_batch_np):
    detections_batch_pt = []
    for np_tensor in detections_batch_np:
        detections_batch_pt += [torch.from_numpy(np_tensor)]
    return tuple(detections_batch_pt)

def decode_np(detections_batch_np):
    utils = torch.hub.load('NVIDIA/DeepLearningExamples:torchhub', 'nvidia_ssd_processing_utils')
    detections_batch_pt = tuple()
    for np_tensor in detections_batch_np:
        detections_batch_pt = detections_batch_pt + (torch.from_numpy(np_tensor),)
    results_per_input = utils.decode_results(detections_batch_pt)

    best_results_per_input = confidence(results_per_input, threshold = 0.40)
    return best_results_per_input
