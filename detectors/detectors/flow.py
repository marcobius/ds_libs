import math
import numpy as np
from sklearn.svm import SVC
import cv2
import flowiz as fz

class DetectorOpticalFlow(object):
    # CONSTANTS
    newVal = (0, 0, 0)
    fillValue = 255
    min_area = 1000
    max_area = 40000
    min_perimeter = math.sqrt(min_area)
    kernel_big = np.ones((15, 15), np.uint8)
    kernel_small = np.ones((10, 10), np.uint8)
    kernel_tiny = np.ones((5, 5), np.uint8)
    kernel_minuscule = np.ones((3, 3), np.uint8)
    h = 7  # Color = direction of movement TODO diria que això no va del tot pq hi ha una discontinuitat en angle
    s = 250  # Colorful-whiteness
    v = 250  # Darkness
    hsv_delta = (h, s, v)
    bgr_delta = (20, 20, 20)
    movement_threshold = 1.5
    blob_min_perc = 0.03
    max_zombie_for = 5
    max_zombie_dist = 30
    track_thickness = 2
    max_age_to_merge = 4

    def __init__(self, video_size, visualization = False, multi_blob_painting = 'carry_on'):
        # self.random_image = np.random.rand(video_size[0], video_size[1])
        self.visualization = visualization
        self.multi_blob_painting = multi_blob_painting
        self.next_track_id = 1

        self.tracks = np.zeros((video_size[0], video_size[1]), dtype='int32')
        self.tracks_new = None
        self.track_dict = {}
        self.track_dict_new = None
        self.track_lines = np.zeros_like(self.tracks)
        self.zombie_tracks = {}
        self.zombie_tracks_new = None

        self.video_size = video_size
        self.colors = {}
        self.nothing_detected_yet = True
        self.np_indices = np.indices(video_size)
        x,y = self.np_indices[0].flatten(), self.np_indices[1].flatten()
        self.points = np.vstack((x,y)).T
        # self.backSubMOG2 = cv2.createBackgroundSubtractorMOG2(history = 1000, varThreshold = 25, detectShadows = False) # DEFAULT PARAMS history = 500, varThreshold = 16, detectShadows = True
        # self.backSubKNN = cv2.createBackgroundSubtractorKNN()
        # self.acc_cam_mov = np.asarray([0,0])
        self.det_dict = None
        self.svclassifier = SVC(kernel='linear')
        self.image_flow = None

    def detect(self, of, frame_id, frame):
        # Initialize systems if first pass
        # Removing median camera movement (stabilizing image)

        camera_movement = np.asarray([np.median(of[:, :, 0]), np.median(of[:, :, 1])])
        of_stabilized = np.zeros_like(of)
        of_stabilized[:, :, 0] = of[:, :, 0].copy() - camera_movement[0]
        of_stabilized[:, :, 1] = of[:, :, 1].copy() - camera_movement[1]
        # print(np.median(of[:, :, 0]),np.median(of[:, :, 1]))

        im_shape = of_stabilized[:, :, 0].shape
        assert np.all(np.array(self.video_size)==np.array(im_shape)), 'Optical Flow shape does not match with Video shape originally given'

        detections = np.zeros(im_shape, dtype='float32')
        module = np.hypot(of_stabilized[:, :, 0], of_stabilized[:, :, 1])
        # module = cv2.dilate(module, kernel_tiny, iterations=1)
        self.image_flow = fz.convert_from_flow(of_stabilized)
        # image_flow_hsv = cv2.cvtColor(image_flow, cv2.COLOR_BGR2HSV)



        id_detector = 0



        # moving = np.where(module > movement_threshold)
        mask_contours = np.zeros(im_shape, dtype='uint8')
        mask_contours[module > self.movement_threshold] = 1

        # # frame_stable = self.stabilize(frame, camera_movement)
        # frame_stable = frame
        # fgMaskMOG2 = self.backSubMOG2.apply(frame_stable) #
        # # fgMaskKNN = self.backSubKNN.apply(frame_stable)
        # fgMaskMOG2 = cv2.erode(fgMaskMOG2, kernel_tiny, iterations=1)
        # fgMaskMOG2 = cv2.dilate(fgMaskMOG2, kernel_big, iterations=1)
        # cv2.imshow('fgMaskMOG2', fgMaskMOG2)
        # # cv2.imshow('fgMaskKNN', fgMaskKNN)
        # mask_contours = np.multiply(mask_contours, fgMaskMOG2)

        contours, hierarcies =   cv2.findContours(mask_contours,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE) # cv2.RETR_EXTERNAL, RETR_LIST, RETR_TREE
        self.det_dict = {}
        for contour in contours:
            if contour.shape[0]<self.min_perimeter:
                continue
            contour_list = list(map(tuple, contour.squeeze()[:,::-1]))
            blob = self.fill_polygon(polygon=contour_list, canvas=detections, color=id_detector+1, ret_blob=True, draw_canvas=False)
            # blob = np.where(detections==id_detector+1)
            # FILTERING BY AREA SIZE
            if blob[0].shape[0] > self.min_area and blob[0].shape[0] < self.max_area:
                id_detector += 1
                d = {}
                d['contour'] = contour
                d['blob'] = blob
                d['center'] = np.asarray((int(np.mean(blob[0])), int(np.mean(blob[1]))))
                self.det_dict[id_detector] = d
                detections[blob] = id_detector
            # else:
            #     detections[blob] = 0

        # if cfg['optical_flow']['visualization_enabled']:
        #     cv2.imshow('frame', frame)
        #     cv2.imshow('module', module)
        #     cv2.imshow('mask_contours', mask_contours.astype('float32'))
        #     cv2.imshow('detections', detections / np.max(detections) + 1e-6)
        #     if self.nothing_detected_yet:
        #         cv2.moveWindow('frame', 0, 0)
        #         cv2.moveWindow('module', 0, 600)
        #         cv2.moveWindow('mask_contours', 0, 1200)
        #         cv2.moveWindow('detections', 0, 1800)
        #     key = cv2.waitKey(1)

        # not_moving = module<movement_threshold
        # mask = np.zeros_like(image_flow[:,:,0])
        # mask[not_moving] = 128 # Posem a gris el fons pq no propagui en buscar regions
        # mask = cv2.copyMakeBorder(mask, 1, 1, 1, 1, cv2.BORDER_CONSTANT, value=0)
        # mod_clean_back = module.copy()
        # mod_clean_back[not_moving] = 0
        # mod_clean_back = cv2.erode(mod_clean_back, kernel_big, iterations=1)
        # while(np.max(mod_clean_back)>0):
        #     seedPoint = np.unravel_index(np.argmax(mod_clean_back), mod_clean_back.shape)
        #     # print(seedPoint)
        #     blob_area, _, mask, bbox = cv2.floodFill(image=image_flow_hsv, mask=mask, seedPoint=seedPoint[::-1], newVal=newVal, loDiff=hsv_delta, upDiff=hsv_delta, flags=4 | cv2.FLOODFILL_FIXED_RANGE | cv2.FLOODFILL_MASK_ONLY | (fillValue << 8)) # cv2.FLOODFILL_FIXED_RANGE
        #     # blob_area, _, mask, bbox = cv2.floodFill(image=image_flow, mask=mask, seedPoint=seedPoint[::-1], newVal=newVal, loDiff=bgr_delta, upDiff=bgr_delta, flags=8 | cv2.FLOODFILL_FIXED_RANGE | cv2.FLOODFILL_MASK_ONLY | (fillValue << 8)) # cv2.FLOODFILL_FIXED_RANGE
        #     # cv2.imshow('mask', mask)
        #     # key = cv2.waitKey(0)
        #     # print('blob_area:',blob_area, 'seedPoint:', seedPoint)
        #     mask = cv2.dilate(mask, kernel_tiny, iterations=1)
        #     blob = np.where(mask[1:-1,1:-1] == 255)
        #     mask[1:-1, 1:-1][blob] = 128
        #     # cv2.imshow('mask', mask)
        #     mod_clean_back[blob] = 0
        #     # cv2.imshow('mod_clean_back', mod_clean_back/(np.max(mod_clean_back+1e-6)))
        #     # key = cv2.waitKey(0)
        #     # Netejant filets residuals al voltant de les deteccions
        #     mod_clean_back = cv2.erode(mod_clean_back, kernel_tiny, iterations=1)
        #     mod_clean_back = cv2.dilate(mod_clean_back, kernel_tiny, iterations=1)
        #     # cv2.imshow('mod_clean_back', mod_clean_back/(np.max(mod_clean_back+1e-6)))
        #     # key = cv2.waitKey(0)
        #     if blob_area > min_area and blob_area < max_area:
        #         id_detector += 1
        #         detections[blob] = id_detector
        #         # print('next_id ', id_detector)
        #     # cv2.imshow('mask', mask)
        #     # cv2.moveWindow('mask', 0, 1200)
        #     # cv2.imshow('mod_clean_back', mod_clean_back/(np.max(mod_clean_back)+1e-6))
        #     # cv2.moveWindow('mod_clean_back', 0, 1800)


        # detections = cv2.erode(detections, kernel_tiny, iterations=1)

        # max_coords = np.unravel_index(np.argmax(module), module.shape)
        # print(of[max_coords])
        # of_stabilized[:,:,:] = 0
        origins = self.np_indices + of.astype(int).transpose((2, 0, 1))[::-1]
        origins[0][origins[0]<0] = 0
        origins[0][origins[0]>=im_shape[0]] = im_shape[0]-1
        origins[1][origins[1]<0] = 0
        origins[1][origins[1]>=im_shape[1]] = im_shape[1]-1
        # inside = np.where(np.logical_and(np.logical_and(origins[0]>=0,origins[0]<im_shape[0]), np.logical_and(origins[1]>=0,origins[1]<im_shape[1]))) # Removing references outside dimensions
        # self.random_image[inside] = self.random_image[origins[0][inside],origins[1][inside]] # Deforming tracks to present frame
        self.tracks_new = np.zeros_like(self.tracks)
        # cv2.imshow('random_image', self.random_image/(np.max(self.random_image)+1e-6))
        # cv2.moveWindow('random_image', 1800, 0)
        # cv2.imshow('tracks', self.tracks/(np.max(self.tracks)+1e-6))
        # cv2.moveWindow('tracks', 0, 1800)
        # tmp = np.ones_like(self.tracks) * 1.5
        # cv2.imshow('image_flow', image_flow)
        # cv2.imshow('detections', detections / (np.max(detections) + 1e-6))
        # cv2.imshow('mask_contours', mask_contours / (np.max(mask_contours) + 1e-6))
        # key = cv2.waitKey(1)
        # if key == 'q':
        #     exit(0)

        # OBTAINING TRACK FROM DETECTION
        self.zombie_tracks_new = self.track_dict.copy()
        self.track_dict_new = {}
        for det_index in range(1, id_detector+1):
            blob = self.det_dict[det_index]['blob']
            self.tracks_new[blob] = self.tracks[origins[0][blob], origins[1][blob]]
            unique, counts = np.unique(self.tracks_new[blob].astype('int64'), return_counts=True)

            self.update_blob(self.det_dict[det_index], blob, unique, counts, self.blob_min_perc)

        self.tracks = self.tracks_new
        self.track_dict = self.track_dict_new
        for key in self.zombie_tracks.copy():
            if self.zombie_tracks[key]['zombie_for'] > self.max_zombie_for:
                del self.zombie_tracks[key]
            else:
                self.zombie_tracks[key]['zombie_for'] += 1
        for key in self.zombie_tracks_new:
            self.zombie_tracks_new[key]['zombie_for'] = 1
            self.zombie_tracks[key] = self.zombie_tracks_new[key]


        # FINAL IMSHOW
        if self.visualization:
            # CREATING TRACKING VIDEO
            alfa = 0.7
            # tracked_frame = frame.copy()
            tracked_frame = frame.copy()
            tracks_with_lines = np.maximum(self.tracks, self.track_lines)
            where_tracks = np.where(tracks_with_lines > 0)
            from img_utils.img_utils.repaint import colorize_grayscale
            tracked_frame[where_tracks] = colorize_grayscale(tracks_with_lines, self.colors)[where_tracks]
            tracked_frame = cv2.addWeighted(tracked_frame, alfa, frame, 1 - alfa, 0)
            cv2.imshow('tracked_frame', tracked_frame)
            cv2.imshow('image_flow', self.image_flow)
            # cv2.imshow('detections', detections / (np.max(detections) + 1e-6))
            # cv2.imshow('mask_contours', mask_contours / (np.max(mask_contours) + 1e-6))
            # cv2.imshow('track_lines', self.track_lines / (np.max(self.track_lines) + 1e-6))
            # quantized_module = np.zeros_like(module)
            # value = 1
            # for det_index in range(1, 15):
            #     quantized_module[module>(det_index/3)] = value
            #     value = value*1.5
            # cv2.imshow('quantized_module', quantized_module/np.max(quantized_module))

            # cv2.imshow('tracks_error', img_utils.colorize_grayscale(np.abs(tracks_old[origins[0], origins[1]]), self.colors))
            # key = cv2.waitKey(0)
            # cv2.imshow('tracks_error', img_utils.colorize_grayscale(np.abs(self.tracks-tracks_old[origins[0], origins[1]]), self.colors))

            # cv2.imshow('frame', frame)
            # cv2.moveWindow('frame', 0,0)
            # cv2.imshow('image_flow', image_flow)
            # cv2.imshow('detections', detections/(np.max(detections+1e-6)))
            # cv2.imshow('tracks', img_utils.colorize_grayscale(self.tracks, self.colors))
            if self.nothing_detected_yet:
                cv2.moveWindow('tracked_frame', 0, 0)
                cv2.moveWindow('image_flow', 0,600)
                # cv2.moveWindow('detections', 0, 1200)
                # cv2.moveWindow('mask_contours', 0, 1800)
                # cv2.moveWindow('quantized_module', 0, 1200)
                # cv2.moveWindow('tracks', 0, 1800)
                # cv2.moveWindow('tracks_error', 0, 1800)
                self.nothing_detected_yet=False

            key = cv2.waitKey(1)
            if key == 'q':
                exit(0)
            # cv2.moveWindow('tracks', 2000, 0)
            # cv2.moveWindow('tmp ', 2000, 600)


        return self.tracks

    def update_blob(self, det_dict, blob, unique, counts, blob_min_perc=0.1):
        if counts.shape[0] > 1 and 0 in unique:  # If 0 is present together with other ids, 0 disappears
            unique, counts = self.delete_blob_id(unique, counts, np.where(unique == 0))
        if counts.shape[0] == 1 and 0 in unique:  # If 0 is the only one in there, new track is born. End of story.
            blob_id = self.next_track_id
            self.next_track_id += 1
            self.update_track(det_dict, blob_id, blob, counts, unique)
        else:  # Here we either have a single id or a track with multiple ids. In both cases 0 is not present anymore
            ################################# TRACKING ################################
            if counts.shape[0] > 1:  # Eliminating minority identities
                unique, counts = self.delete_blob_id(unique, counts, np.where((counts / np.sum(counts)) < blob_min_perc))
            if counts.shape[0] > 1:  # Two or more tracks present in the same blob
                # print('\n', np.asarray([unique, counts]).T)
                max_age = 0
                for j, (blob_id, count) in enumerate(zip(unique.copy(), counts.copy())): # If all the blob are pretty young, the older one takes over. Gathering ages here
                    if blob_id in self.track_dict_new:  # Duplicate track, need to delete one of the two
                        if self.track_dict_new[blob_id]['counts'].shape[0] == 1:  # The other one (single) should prevale, this one gets deleted
                            if counts.shape[0] > 1:
                                unique, counts = self.delete_blob_id(unique, counts, np.where(unique==blob_id))
                            # else:
                            #     print('Warning. Multiple blob is about to disappear') # No clue what to do here, probably best to not do anything
                        else:
                            # print('Warning. Duplicates exist between two clusters')
                            count_duplicate = self.track_dict_new[blob_id]['counts'][np.where(self.track_dict_new[blob_id]['unique']==blob_id)]
                            if count > count_duplicate: # By updating this track, the smaller copy gets deleted
                                self.update_track(det_dict, blob_id, blob, counts, unique)
                            else: # If the other track is bigger, delete this one
                                unique, counts = self.delete_blob_id(unique, counts, np.where(unique==blob_id))
                    elif blob_id in self.track_dict:
                        if self.track_dict[blob_id]['age']+1 > max_age:
                            max_age = self.track_dict[blob_id]['age']+1
                            max_blob_id = blob_id
            if counts.shape[0] > 1:
                # print(max_age)
                if 0 < max_age and max_age < self.max_age_to_merge: # Older blob eating the younger one
                    # print('Warning. Merging %d blobs' % (counts.shape[0]))
                    self.update_track(det_dict, max_blob_id, blob, counts, unique)
                else:
                    # print('Warning. Occurence of 2 or more ids in one same blob')
                    if self.multi_blob_painting == 'svm':
                        X_train_list = []
                        y_train_list = []
                        blob_list = []
                        # X_test = np.ascontiguousarray(np.vstack((blob[0],blob[1])).T)
                        x_coords = np.vstack((blob[0],blob[1])).T
                        X_test = np.hstack((x_coords, self.image_flow[blob]))
                        for j, (blob_id, count) in enumerate(zip(unique.copy(), counts.copy())): # Building training set for SVM
                            b = np.where(self.tracks_new[blob] == blob_id)
                            bl = blob[0][b], blob[1][b]
                            blob_list.append(bl)
                            # coords = np.ascontiguousarray(np.vstack((bl[0], bl[1])).T)
                            coords = np.vstack((bl[0], bl[1])).T
                            # X_test = self.multidim_diff(X_test, coords)
                            x_tr = np.hstack((coords, self.image_flow[bl]))
                            X_train_list.append(x_tr)
                            y_train_list.append(self.tracks_new[bl])
                        X_train = np.concatenate(X_train_list)
                        y_train = np.concatenate(y_train_list)
                        try:
                            self.svclassifier.fit(X_train, y_train)
                            y_pred = self.svclassifier.predict(X_test)
                        except ValueError: # ValueError: The number of classes has to be greater than one; got 1 class
                            blob_id = y_train[0]
                            self.update_track(det_dict, blob_id, blob, counts, unique)
                        else:
                            for j, (blob_id, count) in enumerate(zip(unique.copy(), counts.copy())): # Track is not repeated, so it is added
                                b = np.where(y_pred==blob_id)
                                bl = blob[0][b], blob[1][b]
                                self.update_track(det_dict, blob_id, bl, counts, unique) # TODO find best blobs to separate ids
                    elif self.multi_blob_painting == 'carry_on':
                        for j, (blob_id, count) in enumerate(zip(unique.copy(), counts.copy())): # Track is not repeated, so it is added
                            bl = blob[0][0], blob[1][0]
                            self.update_track(det_dict, blob_id, bl, counts, unique) # TODO find best blobs to separate ids

            if counts.shape[0] == 1:  # Only one id is present, follow track normally
                blob_id = unique[0]
                if blob_id in self.track_dict_new:  # Blob_id already exists, so second blob with same id is not acceptable
                    if self.track_dict_new[blob_id]['counts'].shape[0] > 1:  # This other one (single) should prevale, the other one gets deleted
                        self.track_dict_new[blob_id]['unique'], self.track_dict_new[blob_id]['counts'] = \
                            self.delete_blob_id(self.track_dict_new[blob_id]['unique'], self.track_dict_new[blob_id]['counts'], np.where(self.track_dict_new[blob_id]['unique'] == blob_id))
                        self.update_blob(self.track_dict_new[blob_id], self.track_dict_new[blob_id]['blob'], self.track_dict_new[blob_id]['unique'], self.track_dict_new[blob_id]['counts'], blob_min_perc)
                        self.update_track(det_dict, blob_id, blob, counts, unique)
                    else:
                        print('Warning. One blob splits in two')
                        blob_id = self.next_track_id
                        self.next_track_id += 1
                        self.update_track(det_dict, blob_id, blob, counts, unique)
                else:
                    self.update_track(det_dict, blob_id, blob, counts, unique)

    def multidim_intersect(self, arr1, arr2):
        nrows, ncols = arr1.shape
        dtype={'names':['f{}'.format(i) for i in range(ncols)], 'formats':ncols * [arr1.dtype]}
        intersected = np.intersect1d(arr1.view(dtype), arr2.view(dtype))
        return intersected.view(arr1.dtype).reshape(-1, ncols)

    def multidim_diff(self, arr1, arr2):
        nrows, ncols = arr1.shape
        dtype={'names':['f{}'.format(i) for i in range(ncols)], 'formats':ncols * [arr1.dtype]}
        intersected = np.setdiff1d(arr1.view(dtype), arr2.view(dtype))
        return intersected.view(arr1.dtype).reshape(-1, ncols)

    def delete_blob_id(self, unique, counts, id):
        unique = np.delete(unique, id)
        counts = np.delete(counts, id)
        return unique, counts

    def update_track(self, det_dict, blob_id, blob, counts, unique):
        if self.visualization:
            if blob_id not in self.colors:
                from img_utils.img_utils.internals import get_random_color
                self.colors[blob_id] = get_random_color()
        if blob_id == self.next_track_id-1: # Fixing intermitent detection
            min_dist = self.max_zombie_dist
            zombie_id_to_follow = 0
            center_new = det_dict['center'][::-1]
            center_zombie = None
            for zombie_id in self.zombie_tracks:
                offset = 0 if 'v' not in self.zombie_tracks[zombie_id] else self.zombie_tracks[zombie_id]['v'] * self.zombie_tracks[zombie_id]['zombie_for']
                center_old = self.zombie_tracks[zombie_id]['center'][::-1] + offset
                this_dist = np.linalg.norm(center_new-center_old)
                if this_dist < self.max_zombie_dist:
                    if  this_dist < min_dist:
                        min_dist = this_dist
                        zombie_id_to_follow = zombie_id
                        center_zombie = center_old - offset
            if zombie_id_to_follow != 0:
                blob_id = zombie_id_to_follow
                self.track_dict_new[blob_id] = self.zombie_tracks[blob_id]
                del self.zombie_tracks[blob_id]
                self.track_lines = cv2.line(self.track_lines, tuple(center_zombie), tuple(center_new), color=int(blob_id), thickness=self.track_thickness)
        self.tracks_new[blob] = blob_id
        self.track_dict_new[blob_id] = det_dict
        self.track_dict_new[blob_id]['counts'] = counts
        self.track_dict_new[blob_id]['unique'] = unique
        if blob_id in self.track_dict:
            self.track_dict_new[blob_id]['age'] = self.track_dict[blob_id]['age'] + 1
            center_old = self.track_dict[blob_id]['center'][::-1]
            center_new = self.track_dict_new[blob_id]['center'][::-1]
            self.track_dict_new[blob_id]['v'] = center_new-center_old
            self.track_lines = cv2.line(self.track_lines, tuple(center_old), tuple(center_new), color=int(blob_id), thickness=self.track_thickness)
        else:
            self.track_dict_new[blob_id]['age'] = 1
        if blob_id in self.zombie_tracks_new:
            del self.zombie_tracks_new[blob_id]
            # self.zombie_tracks_new = np.delete(self.zombie_tracks_new, np.where(self.zombie_tracks_new == blob_id))


    def stabilize(self, frame, camera_movement):
        self.acc_cam_mov = self.acc_cam_mov*0.99 + camera_movement
        frame_stabilized = scipy.ndimage.interpolation.shift(frame, (-self.acc_cam_mov[0], -self.acc_cam_mov[1], 0), mode='nearest')
        return frame_stabilized



    def fill_polygon(self, polygon, canvas, color=1, ret_blob=False, draw_canvas=True):
        '''
        fill_polygon([(y0,x0), (y1,x1),...], canvas, color=1)

        Draw a filled polygon in canvas

        Parameters
        ----------
        polygon : list of pairs
            a list of (y,x) points
        canvas : ndarray
            where to draw, will be modified in place
        color : integer, optional
            which colour to use (default: 1)
        '''
        # algorithm adapted from: http://www.alienryderflex.com/polygon_fill/
        if not len(polygon):
            return
        min_y = min(y for y, x in polygon)
        max_y = max(y for y, x in polygon)
        polygon = [(float(y), float(x)) for y, x in polygon]
        blob = (np.asarray([]),np.asarray([]))
        if max_y < canvas.shape[0]:
            max_y += 1
        for y in range(min_y, max_y):
            nodes = []
            j = -1
            for i, p in enumerate(polygon):
                pj = polygon[j]
                if p[0] < y and pj[0] >= y or pj[0] < y and p[0] >= y:
                    dy = pj[0] - p[0]
                    if dy:
                        nodes.append((p[1] + (y - p[0]) / (pj[0] - p[0]) * (pj[1] - p[1])))
                    elif p[0] == y:
                        nodes.append(p[1])
                j = i
            nodes.sort()
            for n, nn in zip(nodes[::2], nodes[1::2]):
                nn += 1
                if draw_canvas:
                    canvas[y, int(n):int(nn)] = color
                if ret_blob:
                    xs = np.arange(int(n), int(nn), dtype=int)
                    ys = (np.ones_like(xs) * y).astype(int)
                    blob = (np.concatenate((blob[0],ys)).astype(int),np.concatenate((blob[1],xs)).astype(int))
                    pass
        if ret_blob:
            return blob

