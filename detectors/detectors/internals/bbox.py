
import numpy as np



class Bbox(object):
    """
    This class represents a bounding box detection in a single image.

    Parameters
    ----------
    ltwh : array_like
        Bounding box in format `(x, y, w, h)`.
    confidence : float
        Detector confidence score.
    feature : array_like
        A feature vector that describes the object contained in this image.

    Attributes
    ----------
    ltwh : ndarray
        Bounding box in format `(top left x, top left y, width, height)`.
    class_conf : ndarray
        Detector confidence score.
    feature : ndarray | NoneType
        A feature vector that describes the object contained in this image.

    """

    def __init__(self, class_id, class_conf, ltwh, obj_conf, feature, frame_number=0):
        self.frame_number = frame_number
        self.class_id = class_id
        self.class_conf = float(class_conf)
        self._ltwh = np.asarray(ltwh, dtype=np.float)
        self.obj_conf = float(obj_conf)
        self.feature = np.asarray(feature, dtype=np.float32)

    def ltrb(self):
        """Convert bounding box to format `(min x, min y, max x, max y)`, i.e., `(top left, bottom right)`.
        """
        ret = self._ltwh.copy()
        ret[2:] += ret[:2]
        return ret

    def xyah(self):
        """Convert bounding box to format `(center x, center y, aspect ratio, height)`, where the aspect ratio is `width / height`.
        """
        ret = self._ltwh.copy()
        ret[:2] += ret[2:] / 2
        ret[2] /= ret[3]
        return ret

    def ltwh(self):
        return self._ltwh

    def xywh(self):
        ret = self._ltwh.copy()
        ret[:2] += ret[2:] / 2
        return ret

