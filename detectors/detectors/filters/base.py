"""
        FILTRES SOBRE LES DETECCIONS
"""

from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
from torch.hub import load

def confidence(results_per_input, threshold = 0.40):
    utils = load('NVIDIA/DeepLearningExamples:torchhub', 'nvidia_ssd_processing_utils')
    return [utils.pick_best(results, threshold) for results in results_per_input]









class DetectionFilter(object):
    def __call__(self, detection):
        raise NotImplementedError


class DetectionFilterByClass(DetectionFilter):
    """ Filtro de deteccion por id_clase """
    def __init__(self, lista_clases_admitidas):
        self.lista = lista_clases_admitidas

    def __call__(self, detection):
        """
        Retorna True si la detection recibida es válida segun el filtro
        :param detections: Detection
        :return: boolean
        """
        return detection.class_id in self.lista


class DetectionFilterByZone(DetectionFilter):
    """ Filtro de deteccion por zona """
    def __init__(self, polygon):
        self.polygon = polygon

    def __call__(self, detection):
        x,y,w,h = detection.get_ltwh()
        x = int(x)
        y = int(y)
        value = self.polygon.contains(Point(x, y))
        return value


class DetectionFilterComposer(DetectionFilter):
    def __init__(self, filters):
        """
        :param filters: Lista de objetos DetectionFilter
        """
        self.filters = filters

    def __call__(self, detection):
        pasaFiltro = True
        for f in self.filters:
            pasaFiltro = pasaFiltro and f(detection)
        return pasaFiltro


