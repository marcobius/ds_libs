from ds.dataset.blender_dataset import BlenderDataset

if __name__ == '__main__':
    # dset = BlenderDataset('/mnt/datasets/3D_generated/forklift/renders/images', ['person', 'forklift', 'cuerno', 'carga'])
    dset = BlenderDataset('/mnt/datasets/3D_generated/formula_one/renders/images', ['f1_car'])
    dset.export_2_yolo('/mnt/datasets/3D_generated/f1_generated')
    print("FIN")

