import sys
import os
from pathlib import Path
print(f"PATH: {sys.path}")
sys.path.insert(0, str((Path(os.getcwd())/"..").resolve())) #depen d'on s'executi fa falta una o l'altre
sys.path.insert(0, str((Path(os.getcwd())).resolve()))
print(f"PATH: {sys.path}")
from ds.dataset.base_dataset import BaseDataset
import csv
import pandas
from tqdm import tqdm
from img_utils.img_utils.files import get_image_shape
import numpy as np
import torch
from ds.dataset.images import TaggedImage, VisDrone2019Image

class VisDrone2019Dataset(BaseDataset):
    CLASS_NAMES = ['pedestrian', 'people', 'bicycle', 'car', 'van', 'truck', 'tricycle', 'awning-tricycle', 'bus', 'motorcycle']
    """
    Dataset de imagenes con GT del VisDataset2019
    VisDataset tiene una estructura similar a kitti pero con nombres de carpetas cambiados:
    base_dir
     |__ annotations
     |__ images

    Las annotations son txt con 8 campos:
    Xcenter, Ycenter, W, H, bool Ignore regions, CLASS, ?, ?
    Ej: 871,572,54,92,1,4,0,0
    Coordenadas de bbox en pixels.

    """
    def prepara_index(self):
        """
        Busca todas las imagenes y etiquetas y las mete en un csv que hace de indice.
        Retorna el indice cargado en un pandas
        :param path: str o Path del directorio donde se encuentre
        """
        path = Path(self.path_base) #OS agnostic
        index_filename = self.index_filename
        if not os.path.isdir(path):
            print(f'Directorio no encontrado: {path}')
            raise NotADirectoryError
        if not os.path.isfile(path / index_filename):
            # No existe indice, por tanto lo creo
            headers = ['frame_filename', 'gt_filename','width', 'height']
            file = open(path / index_filename, mode='w')
            csv_writer = csv.writer(file, delimiter=',')
            csv_writer.writerow(headers)
            # Listado de imagenes
            image_files = list((path / "images").glob("*.jpg"))
            image_files += list((path / "images").glob("*.JPG"))
            image_files.sort()
            print(f"Preparando indice para directorio {path}...")
            for i in tqdm(range(len(image_files))):
                h, w, c = get_image_shape(str(image_files[i]))
                label_filename = (path / "annotations" / image_files[i].name).with_suffix(".txt")
                if os.path.isfile(label_filename):
                    csv_writer.writerow([image_files[i], label_filename, w, h])
                else:
                    print(f"WARNING. imagen sin label file ({label_filename}).")
            file.close()
        else:
            print(f"El fichero indice ya existe. Reaprovechando {path / index_filename}")
        #Cargo en pandas el fichero de indice
        index = pandas.read_csv(path / index_filename, dtype=str)
        return index

    def get_labels(self, classes):
        self.class_names = VisDrone2019Dataset.CLASS_NAMES
        return BaseDataset.get_labels(self, classes)

    def __getitem__(self, item): 
        """
        Retorna una ds.Image
        Args:
            item: int indice
        Returns: tagged_image.Image el objeto imagen contendrá las regions, por tanto las labels o targets
        """
        image_filename = self.index_fotos.iloc[item].frame_filename
        bboxes_filename = self.index_fotos.iloc[item].gt_filename
        image = VisDrone2019Image()
        image.loadFromFile(Path(image_filename))
        # para validación y training es necesario cargar tambien los labels (o targets)
        if self.mode != 'test':
            image.load_labels(bboxes_filename)
            # Ja estan totes les dades a memoria, pero cal adaptarles per poder passarles per les albumentations
            # classes = np.zeros(len(self.class_names))
            classes_index = image.getClasses().astype(int)
            # classes[classes_index-1]=1  # posem a 1 les clases etiquetades
            #TODO: ALERTA, clarificar si guardamos un vector de classes entero por cada bbox o solo su clase, jugar con herencias
            # image.setClasses(classes) # faig que totes les imatges tinguin un vector de shape igual per les clases
            image.setClassNames(self.get_labels(classes_index-1))
            if self.transforms:
                image_dict = {'image': image.getImage(), 'bboxes': image.getRegionsX1Y1X2Y2(), 'labels': image.getClasses(), 'class_names':image.getClassNames()}
                image_dict = self.transforms(**image_dict)
                image.setImage(image_dict['image'])
                if len(image_dict['bboxes'])>0:
                    image.setRegionsX1Y1X2Y2(image_dict['bboxes'])
                    image.setClasses(image_dict['labels'])
                    image.setClassNames(image_dict['class_names'])
                else:
                    # print(f'bboxes eliminadas')
                    image.setRegionsX1Y1X2Y2(np.asarray([]))
                    image.setClasses(np.asarray([]))
                    image.setClassNames(np.asarray([]))
        else:
            if self.transforms:
                # test dataset must have some values so that transforms (albumentations) work.
                cls = torch.as_tensor([[0]], dtype=torch.float32),
                bbox = torch.as_tensor([[0, 0, 0, 0]], dtype=torch.float32),
                image_dict = {'image': image.getImage(), 'bboxes': bbox, 'labels': cls}
                image.setImage(self.transforms(**image_dict)['image'])
        return image




if __name__ == "__main__":
    from img_utils.img_utils.display import display_batch_gt, display_image_gt
    import ds.dataset.collaters as collaters
    path = "/mnt/src/tao/tao-experiments/data/VisDrone/VisDrone2019-DET-val"

    dset = VisDrone2019Dataset(path, VisDrone2019Dataset.CLASS_NAMES)

    for i in range(10):
        image = dset.__getitem__(i)
        display_image_gt(image.getImage(), image.getRegionsX1Y1X2Y2(), image.getClassNames(), waitkey=0, size=(1024,768))

    print(f"FIN (len(dataset): {len(dset)})")    