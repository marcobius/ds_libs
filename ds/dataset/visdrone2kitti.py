import sys
import os
from pathlib import Path
print(f"PATH: {sys.path}")
sys.path.insert(0, str((Path(os.getcwd())/"..").resolve())) #depen d'on s'executi fa falta una o l'altre
sys.path.insert(0, str((Path(os.getcwd())).resolve()))
print(f"PATH: {sys.path}")
from ds.dataset.VisDrone_dataset import VisDrone2019Dataset


if __name__ == '__main__':
    # Dataset de dev
    path = "/mnt/src/tao/tao-experiments/data/VisDrone/VisDrone2019-DET-test-dev"
    dset = VisDrone2019Dataset(path, VisDrone2019Dataset.CLASS_NAMES)
    dset.export_2_kitti('/mnt/src/tao/tao-experiments/data/VisDrone/VisDrone2019-DET-test_dev_kitti', force_copy=True)
    # Dataset de train
    path = "/home/dpbou/src/tao/tao-experiments/data/VisDrone/VisDrone2019-DET-train"
    dset = VisDrone2019Dataset(path, VisDrone2019Dataset.CLASS_NAMES)
    dset.export_2_kitti('/mnt/src/tao/tao-experiments/data/VisDrone/VisDrone2019-DET-train_kitti', force_copy=True)
    # Dataset de val
    # path = "/home/dpbou/src/tao/tao-experiments/data/VisDrone/VisDrone2019-DET-val"
    # dset = VisDrone2019Dataset(path, VisDrone2019Dataset.CLASS_NAMES)
    # dset.export_2_kitti('/home/dpbou/src/tao/tao-experiments/data/VisDrone/VisDrone2019-DET-val_kitti', force_copy=True)
