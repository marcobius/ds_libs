"""
    13/04/2021
    DeepSolutions
"""
import cv2
import os
import torch
from ds.dataset.images import Image


class SingleVideoDataset():
    """
    Dataset secuencial al estilo pytorch que abre un video y lo sirve frame a frame
    No es multiproceso.
    """
    def __init__(self, fname, resize_factor=1.0, loop=False, transform=None):
        if not os.path.isfile(fname):
            raise FileNotFoundError(fname)
        self.transform = transform
        self.filename = fname
        self.loop = loop
        self.resize_factor = resize_factor
        self.cap = self._load_video()

    def _load_video(self):
        self.cap = cv2.VideoCapture(self.filename)
        self.nframes = int(self.cap.get(cv2.CAP_PROP_FRAME_COUNT))
        self._original_fps = self.cap.get(cv2.CAP_PROP_FPS)
        self._original_shape = (
            int(self.cap.get(cv2.CAP_PROP_FRAME_HEIGHT)),
            int(self.cap.get(cv2.CAP_PROP_FRAME_WIDTH))
        )
        self.frame_count = 0
        return self.cap

    def get_fps(self):
        return self._original_fps

    def get_output_shape(self):
        return (int(self._original_shape[1]*self.resize_factor), int(self._original_shape[0]*self.resize_factor))

    def get_original_shape(self):
        return self._original_shape

    def __len__(self):
        return self.nframes

    def __iter__(self):
        self.frame_count = 0
        return self

    def __next__(self):
        self.frame_count += 1
        if self.frame_count > self.nframes:
            # Final del video
            if self.loop:
                self.cap = self._load_video()
                self.frame_count = 0
            else:
                raise StopIteration
        try:
            ret_val, frame = self.cap.read()
        except:
            ret_val = False
            frame = None
        if not ret_val:
            print("Error en obtener frame")
            raise StopIteration
        # Tengo frame
        if self.resize_factor != 1.0:
            frame = cv2.resize(frame, (int(frame.shape[1]*self.resize_factor),int(frame.shape[0]*self.resize_factor)))
        image = Image(cv2_img=frame)
        if self.transform is not None:
            image_dict = {'image': image.getImage()}
            image_dict = self.transform(**image_dict)
            image.setImage(image_dict['image'])
        return image

    def stop(self):
        self.cap.release()

if __name__ == '__main__':
    # Ejemplos de uso - SingleVideoDataset
    # reader = SingleVideoDataset('../tests/test_data/videos/formula1_limits.mp4', resize_factor=0.5)
    # for image in reader:
    #     cv2.imshow('Video', image.getImage())
    #     key = cv2.waitKey(1)
    #     if key==ord('q'):
    #         break
    # reader.stop()
    # cv2.destroyWindow('Video')
    import albumentations as A
    transform = A.Compose(
        [
         # A.Flip(p=0.3),
         # A.RandomBrightnessContrast(p=0.3),
         # A.ShiftScaleRotate(p=0.5),
         # A.RGBShift(r_shift_limit=30, g_shift_limit=30, b_shift_limit=30, p=0.3),
         # A.FancyPCA(p=1),
         # A.ISONoise(p=1),
         A.transforms.ToFloat(p=1.0)])
    reader = SingleVideoDataset('../tests/test_data/videos/formula1_limits.mp4', resize_factor=0.5, transform=transform)
    iterador = iter(reader)
    imagen = next(iterador)
    print('FIN')
