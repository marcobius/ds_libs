"""
Deep Solutions
Created: 18/6/21
"""
from ds.dataset.via_dataset import ViaDataset

if __name__ == '__main__':
    # crea_yolo_labels('/media/dpbou/sdd_usb/DS/datasets/3d_objects/forklift/renders/images')
    # crea_yolo_cfg_file('/media/dpbou/sdd_usb/DS/datasets/3d_objects/forklift/renders/images', ['forklift'])
    # dset = BlenderDataset('../tests/test_data/blender_data', ['box1', 'box2'])
    dset = ViaDataset('/mnt/datasets/imagenet', ['persona', 'forklift', 'cuernos', 'carga'], via_json_name='imagenet_dataset.json')
    dset.index_fotos = dset.index_fotos.sort_values(by=['frame_filename'])
    dset.index_fotos = dset.index_fotos.drop_duplicates()
    dset.index_fotos = dset.index_fotos.reset_index(drop=True)
    dset.export_2_yolo('/mnt/datasets/imagenet/yolo_format', inici=None, final=None, train_val=0.8)

