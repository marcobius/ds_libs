"""
Deep Solutions
Created: 23/6/21

Junta dataset 3D_generated/forklift y Imagenet/Forklift
Genera un dataset yolo único para entrenamiento.
"""
from ds.dataset.blender_dataset import BlenderDataset
from ds.dataset.via_dataset import ViaDataset

if __name__ == '__main__':
    dset = ViaDataset('/mnt/datasets/imagenet', ['persona', 'forklift', 'cuernos', 'carga'], via_json_name='imagenet_dataset.json')
    dset.index_fotos = dset.index_fotos.sort_values(by=['frame_filename'])
    dset.index_fotos = dset.index_fotos.drop_duplicates()
    dset.index_fotos = dset.index_fotos.reset_index(drop=True)
    dset.export_2_yolo('/mnt/datasets/yolo_trainer', inici=None, final=None, train_val=0.8)
    dset_b = BlenderDataset('/mnt/datasets/3D_generated/forklift/renders/images', ['person', 'forklift', 'cuerno', 'carga'])
    dset_b.export_2_yolo('/mnt/datasets/yolo_trainer')

    print("FIN")