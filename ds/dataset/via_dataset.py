'''
Deep Solutions
Created: 29th April 2021
Author: Deep Solutions
'''

from ds.img_utils.display import display_batch_gt, display_image_gt
from torch.utils.data import DataLoader
from ds.dataset.images import TaggedImage, VIAImage
from ds.dataset.base_dataset import BaseDataset
import os
import json
import numpy as np
import cv2
import errno
from pathlib import Path
from tqdm import tqdm
import pandas as pd
from ds.img_utils.files import get_image_shape
import ds.dataset.collaters as collaters


class ViaDataset(BaseDataset):
    '''
    Class for interacting with a Vgg Image Annotation (VIA) json project file
    All dataset contents are stored in 'self.project_dict' and maintained throughout
    '''
    def __init__(self, pathBase, class_names, transforms=None, mode='train', train_val_test_split=[1.0, 0.0, 0.0], set='all',
                 seed=1, index_filename='index.csv', via_json_name='via_dataset.json', df_to_import=None):
        self.path_base = pathBase
        self.filename = via_json_name
        self.df_to_import = df_to_import
        # Checking that the 'via_json_name' is a json file
        if str.lower(self.filename[-4:]) != 'json':
            print('Bad argument, should have passed a json file')
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), self.filename)
        BaseDataset.__init__(self, pathBase, class_names, transforms, mode, train_val_test_split, set, seed, index_filename)

    def set_project_dict(self, dict):
        self.__project_dict = dict

    def prepara_index(self):
        # Loads VIA file if exists, creates an VIA json with found images otherwise
        if not os.path.exists(self.path_base):
            os.mkdir(self.path_base)
        if self.df_to_import is None:
            if os.path.exists(os.path.join(self.path_base, self.filename)):
                print('JSON file found, loading its contents...')
                self.__project_dict = self.__readJsonVia()
            else:
                print('JSON file not found, creating empty object')
                self.__project_dict = self.__getNewProjectDict()
                # Creating a new VIA json project file with images found from the base folder down
                self.__addImages()
                self.saveProjectDict()

            cols_dict = {'index': str,
                         'frame_filename': str,
                         'width': np.int32,
                         'height': np.int32}
            df = pd.DataFrame({}, columns=cols_dict.keys())
            df['index'] = self.__project_dict['_via_image_id_list']
            images_dict = self.__project_dict['_via_img_metadata']
            im_keys_list = list(images_dict.keys())
            print('Creating dataframe from VIA dict...')
            for i in tqdm(range(len(images_dict))):
                im_key = im_keys_list[i]
                im_info = images_dict[im_key]
                filename = im_info['filename']
                h, w = get_image_shape(os.path.join(self.path_base, filename))[:2]
                df.loc[df['index']==im_key, ['frame_filename', 'width', 'height']] = [filename, w, h]

            df = df.astype(cols_dict)

        else:
            df = self.df_to_import
            print('New JSON file is being created (or replacing old one)')
            self.__project_dict = self.__getNewProjectDict()
            # Creating a new VIA json project file with images found from the base folder down
            for index, row in df.iterrows():
                self.addImage(row['frame_filename'])
            self.saveProjectDict()

        return df

    def get_project_dict(self):
        return self.__project_dict

    def readJsonVia(self):
        return self.__readJsonVia()

    def __readJsonVia(self):
        filename = os.path.join(self.path_base, self.filename)
        with open(filename) as json_file:
            project_dict = json.load(json_file)
        return project_dict

    def __addImages(self):
        # Reads all images from folders under the main folder, ignores the rest of the files
        base_split = self.path_base.split(os.sep)
        print('Reading files list from folder...')
        file_list = list(Path(self.path_base).rglob('*'))
        img_ext = ['.jpg', '.png', '.bmp', '.jpeg', '.tiff', '.tif', '.jp2']
        print('Reading files from dataset into VIA object...')
        for i in tqdm(range(len(file_list))):
            filename = str(file_list[i])
            # print(filename)
            file_split = filename.split(os.sep)
            for dir_base, dir_file in zip(base_split, file_split[:len(file_split)-1]):
                # print(dir_base, dir_file)
                assert(dir_base==dir_file)
            filename_clean = os.path.join(*file_split[len(base_split):])
            if os.path.isfile(filename):
                if str.lower(filename).endswith(tuple(img_ext)):
                    self.addImage(filename_clean)

    def get_file_key(self, filename):
        size = os.path.getsize(os.path.join(self.path_base, filename))
        key = '{}_{}'.format(filename, size)
        return key, size

    def addImage(self, filename):
        # Adds one image to the VIA internal structure
        im_dic = self.__getDicImageTemplate()
        im_dic['filename'] = filename
        key, size = self.get_file_key(filename)
        im_dic['size'] = size
        self.__project_dict['_via_img_metadata'][key] = im_dic
        self.__project_dict['_via_image_id_list'].append(key)

    def addTaggedImageBboxes(self, filename, tagged_image):
        key, size = self.get_file_key(filename)
        im_dic = self.__project_dict['_via_img_metadata'][key]
        regions = tagged_image.getRegionsCX1Y1WH()
        regions_list = []
        for row in regions:
            clss = row[0]
            class_name = self.class_names[int(clss)-1]
            xywh = row[1:]
            regions_list.append(self.__getDicRegionTemplate(class_name, xywh))
        im_dic['regions'] = regions_list
        self.__project_dict['_via_img_metadata'][key] = im_dic
        self.__project_dict['_via_image_id_list'].append(key)

    def sortImageList(self):
        self.__project_dict['_via_image_id_list'].sort()

    def addDatasetTags(self, class_names, name_of_attribute = 'label'):
        options = {}
        self.class_names = class_names
        for cls in class_names:
            options[cls] = ''
        label = {'type': 'radio', 'description':'', 'options':options, 'default_options':{class_names[0]:True}}
        self.__project_dict['_via_attributes']['region'] = {name_of_attribute:label}

    def __getDicRegionTemplate(self, class_name, xywh, name_of_attribute = 'label'):
        dic = {'shape_attributes':{'name':'rect', 'x':int(xywh[0]), 'y':int(xywh[1]), 'width':int(xywh[2]), 'height':int(xywh[3])},
               'region_attributes':{name_of_attribute:class_name}}
        return dic

    def getDictImageTemplate(self):
        return self.__getDicImageTemplate()

    def __getDicImageTemplate(self):
        return {'file_attributes': {},
                'filename': "",
                'regions': [],
                'size': 0}

    def getNewProjectDict(self):
        return self.__getNewProjectDict()

    def __getNewProjectDict(self):
        dic = {
            '_via_attributes': {
                'file': {},
                'region': {}
            },
            '_via_data_format_version': '2.0.10',
            '_via_image_id_list': [],
            '_via_img_metadata': {},
            '_via_settings': {
                'ui': {
                    'annotation_editor_height': 25,
                    'annotation_editor_fontsize': 0.8,
                    'leftsidebar_width': 18,
                    'image_grid': {
                        'img_height': 80,
                        'rshape_fill': 'none',
                        'rshape_fill_opacity': 0.3,
                        'rshape_stroke': 'yellow',
                        'rshape_stroke_width': 2,
                        'show_region_shape': True,
                            'show_image_policy': 'all'
                    },
                    'image': {
                        'region_label': '__via_region_id__',
                        'region_color': '__via_default_region_color__',
                        'region_label_font': '10px Sans',
                        'on_image_annotation_editor_placement': 'NEAR_REGION'
                    }
                },
                'core': {
                    'buffer_size': 18,
                    'filepath': {},
                    'default_filepath': ""
                },
                'project': {
                    'name': 'via_project_4Oct2019_17h27m'
                }
            },
        }
        return dic

    def saveProjectDict(self, filename=None):
        # If no filename is given, saves into original name. Otherwise into filename
        self.sortImageList()
        if filename is None:
            filename = self.filename
        path, _ = os.path.split(filename)
        if path != '':
            print('Error, you should pass only the name of the file, which is supposed to be saved inside the base path you entered in constructor')
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), filename)
        short_filename, extension = os.path.splitext(filename)
        self.__project_dict['_via_settings']['project']['name'] = short_filename
        filename = os.path.join(self.path_base, filename)
        # print(self.__project_dict) # Aquesta línia fa penjar el PyCharm
        proj = self.__project_dict
        print('Saving VIA project file...')
        with open(filename, 'w') as fp:
            # json.dump(self.__project_dict, fp, sort_keys=True)
            json.dump(proj, fp, sort_keys=True)

    def __getitem__(self, num):
        dic_images = self.__project_dict['_via_img_metadata']
        # im_name = self.__project_dict['_via_image_id_list'][num]
        im_name = self.index_fotos['index'][num] #TODO: no poner nombre 'index' a una columna!!! es una palabra reservada para el index del dataframe!
        # im_name = list(dic_images.keys())[num]
        im_dic = dic_images[im_name]
        regions_via = im_dic['regions']
        image_filename = im_dic['filename']
        im_dic['base_dir'] = self.path_base
        tagged_im = TaggedImage(cv2.imread(os.path.join(self.path_base,image_filename)))
        regions = []
        for region in regions_via:
            class_name = region['region_attributes']['label'] # name_of_attribute = 'label'
            x = region['shape_attributes']['x']
            y = region['shape_attributes']['y']
            w = region['shape_attributes']['width']
            h = region['shape_attributes']['height']
            if class_name in self.class_names:
                clss = self.class_names.index(class_name) + 1
                regions.append([clss, x, y, w, h])
        tagged_im.setRegionsCX1Y1WH(np.asarray(regions))
        return tagged_im

    def deleteImagesNoRegions(self):
        image_id_list = []
        img_metadata = self.__project_dict['_via_img_metadata'].copy()
        print(f"Longitud dataset antes: {len(self.__project_dict['_via_img_metadata'])}")
        for img_key, img_data in img_metadata.items():
            img_key_short = '_'.join(img_key.split('_')[:-1])
            if len(img_data['regions'])==0:
                img_filename = img_data['filename']
                self.index_fotos = self.index_fotos[self.index_fotos['frame_filename']!=img_filename]
                image_id_list.append(img_key)
        # REHACEMOS LA LISTA DE IDS IMAGEN (no se por que)
        for img_key in image_id_list:
            print(f"Image without regions, removing: {img_filename}")
            del self.__project_dict['_via_img_metadata'][img_key]
        print(f"Longitud dataset despues: {len(self.__project_dict['_via_img_metadata'])}")
        img_metadata = self.__project_dict['_via_img_metadata'].copy()
        image_id_list = []
        for img_key, img_data in img_metadata.items():
            # img_key_short = '_'.join(img_key.split('_')[:-1])
            image_id_list.append(img_key)
        self.__project_dict['_via_image_id_list'] = image_id_list
        self.index_fotos = self.index_fotos.reset_index()




if __name__=='__main__':
    # path = 'tests/test_data/blender_data'
    path_oid = '/mnt/datasets/open_image_dataset/'
    path_inet = '/mnt/datasets/imagenet/'
    filepath = Path(os.getcwd())
    path_oid = os.path.abspath(filepath / '..' / path_oid)
    path_inet = os.path.abspath(filepath / '..' / path_inet)
    print(f'Dir: {path_inet}, {path_oid}')
    import albumentations as A
    transform = A.Compose(
        [
         # A.Flip(p=0.3),
         # A.RandomBrightnessContrast(p=0.3),
         # A.ShiftScaleRotate(p=0.5),
         # A.RGBShift(r_shift_limit=30, g_shift_limit=30, b_shift_limit=30, p=0.3),
         # A.FancyPCA(p=1),
         # A.ISONoise(p=1),
         # A.RandomResizedCrop(1080, 1920, p=1),
         # A.RandomSizedBBoxSafeCrop(height=1080, width=1920, p=1),
         # A.MotionBlur(blur_limit=7, p=1),
         A.augmentations.transforms.ToFloat(p=1),
         ],
        bbox_params=A.BboxParams(format='pascal_voc', label_fields=['labels', 'class_names'], min_area=4500, min_visibility=0.2))
    # dset = BlenderDataset(path, ['box1', 'box2'], transforms=transform)
    from ds.dataset.open_image_dataset import OpenImageDataset
    from ds.dataset.imagenet_dataset import ImagenetDataset
    # imnt_dset = OpenImageDataset(path_oid, ['person', 'forklift'], transforms=transform)
    imnt_dset = ImagenetDataset(path_inet, ['person', 'forklift', 'cuernos', 'carga'], transforms=transform)
    imnt_dset.export_2_via()
    via_dset = ViaDataset(path_inet, ['person', 'forklift', 'cuernos', 'carga'], transforms=transform)
    import torch
    for image in via_dset:
        bboxes = torch.from_numpy(image.getRegionsX1Y1X2Y2())
        print(f'bboxes: {bboxes}')
        labels = torch.from_numpy(image.getClasses())
        image = image.switchColors().getImage()
        display_image_gt(image, bboxes, labels, waitkey=0, size=(1024, 768))


    loader = DataLoader(via_dset, batch_size=4, drop_last=True, shuffle=True, num_workers=0, collate_fn=collaters.coll_stacks_no_norm)
    n = 0
    base_filename = Path(path_inet) / 'ds_output'
    for images, bboxes, labels in loader:
        display_batch_gt(images, bboxes, labels, waitkey=0)
        print(f'Batch {n}')
        n+=1
    print('FIN')



