from ds.dataset.blender_dataset import BlenderDataset


if __name__=='__main__':
    # crea_yolo_labels('/media/dpbou/sdd_usb/DS/datasets/3d_objects/forklift/renders/images')
    # crea_yolo_cfg_file('/media/dpbou/sdd_usb/DS/datasets/3d_objects/forklift/renders/images', ['forklift'])
    dset = BlenderDataset('../tests/test_data/blender_data', ['box1', 'box2'])
    dset.export_2_yolo()

