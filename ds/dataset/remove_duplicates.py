# Usage:remove_duplicates -f /media/globalcaos/databases/image_dataset/vehicles/forklift/internet_images/

from glob import glob
import argparse


parser = argparse.ArgumentParser(description='Removes image duplicates recursively')
parser.add_argument('-f','--folder', help='Folder contatins', required=True)
args = vars(parser.parse_args())