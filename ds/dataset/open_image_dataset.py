"""
Deep Solutions
Created: 03/06/2021

Utils i datasets per treballar amb Open Images Datasets (OID)
"""
from torch.utils.data import DataLoader
from ds.dataset.base_dataset import BaseDataset
from pathlib import Path
import os
from img_utils.img_utils.display import display_batch_gt
from modules.utils import bcolors as bc
from modules.utils import images_options, logo, mkdirs, show_classes, progression_bar
from modules.csv_downloader import error_csv, TTV
from modules.show import show
import cv2
from multiprocessing.dummy import Pool as ThreadPool
from tqdm import tqdm
import pandas as pd
import ds.dataset.collaters as collaters
from glob import glob
from img_utils.img_utils.files import get_image_shape
import numpy as np
from ds.dataset.images import TaggedImage
import re

class OpenImageDataset(BaseDataset):
    """
    Dataset capaç de carregar imatges en format OID i fer conversions si cal.
    També permet baixar d'internet una classe o varies de OID i crear un dataset local.
    """
    def __init__(self, pathBase, class_names, transforms=None, mode='train', train_val_test_split=[1.0, 0.0, 0.0], set='all', seed=1, index_filename='index.csv', try_download=False):
        if not os.path.exists(pathBase):
            try_download = True
        self.try_download = try_download # Si volem descarregar el dataset o no
        self.name_file_class = 'class-descriptions-boxable.csv'
        self.CLASSES_CSV = os.path.join(pathBase, self.name_file_class)
        self.df_classes = pd.read_csv(self.CLASSES_CSV, header=None)
        BaseDataset.__init__(self, pathBase, class_names, transforms, mode, train_val_test_split, set, seed, index_filename)

    def __getitem__(self, item):
        row = self.index_fotos.iloc[item]
        image_filename = os.path.join(self.path_base, row['frame_filename'])
        tagged_image = TaggedImage(cv2.imread(image_filename))
        txt_filename = os.path.join(self.path_base, row['gt_filename'])
        regions = []
        with open(txt_filename) as f:
            bbox_lines = f.readlines()
        for bbox_line in bbox_lines:
            bbox_line = bbox_line.replace('\n','')
            match_class_name = re.compile('^[a-zA-Z]+(\s+[a-zA-Z]+)*').match(bbox_line)
            class_name = bbox_line[:match_class_name.span()[1]]
            xyxy = bbox_line[match_class_name.span()[1]:].lstrip().rstrip().split(' ')
            xyxy = [float(item) for item in xyxy]
            class_row = self.df_classes[self.df_classes[1]==class_name]
            # class_code = class_row[0]
            # class_index_1 = class_row.index
            # class_index_2 = self.class_names.index(class_name)
            # Translating index to given list
            class_index = self.oid_2_ds[class_name] + 1 # Because class '0' is 'background'
            regions.append(np.asarray([class_index] + xyxy))
            # print(bbox_line)
        tagged_image.setRegionsX1Y1X2Y2(np.asarray(regions))
        return tagged_image

    def prepara_index(self):
        '''
        Funció encarregada de poblar l'index de fotos.
        En alguns casos es una busqueda de directoris, en altres un download d'internet, etc..
        Returns: Pandas.DataFrame() amb una row per imatge i almenys les columnes: ['frame_filename', 'gt_filename', 'width', 'height']
        '''
        args = {'image_IsDepiction':False,
                'image_IsGroupOf':False,
                'image_IsInside':None,
                'image_IsOccluded':False,
                'image_IsTruncated':False,
                'limit':None, # maximum number of instances to download per class
                'n_threads':20,
                'noLabels':False,
                'yes':True}
        # return self.visualize(args)
        if self.try_download:
            self.bounding_boxes_images(args)

        df_images = self.create_df()
        return df_images

    def create_df(self):
        '''
        Funció encarregada de poblar l'index de fotos.
        En alguns casos es una busqueda de directoris, en altres un download d'internet, etc..
        Returns: Pandas.DataFrame() amb una row per imatge i almenys les columnes: ['frame_filename', 'gt_filename', 'width', 'height']
        '''
        files = glob(os.path.join(self.path_base, '**/**/*.*'), recursive=True)
        cols_dict = {'index': str,
                     'frame_filename': str,
                     'gt_filename': str,
                     'width': np.int32,
                     'height': np.int32}
        df = pd.DataFrame({}, columns=cols_dict.keys())
        # df = df.astype(cols_dict)
        print('Reading files from dataset...')
        for i in tqdm(range(len(files.copy()))):
            full_path = files[0]
            folder, filename = os.path.split(full_path)
            name, extension = os.path.splitext(filename)
            relative_path = os.path.relpath(full_path, self.path_base)
            index = name
            if extension == '.jpg':
                height, width = get_image_shape(full_path)[:2]
                if index in list(df['index']):
                    df.loc[df['index']==index,'frame_filename'] = relative_path
                else:
                    df.loc[len(df),['index', 'frame_filename', 'width', 'height']] = (index, relative_path, width, height)

            elif extension == '.txt':
                if index in list(df['index']):
                    df.loc[df['index']==index,'gt_filename'] = relative_path
                else:
                    df.loc[len(df),['index', 'gt_filename']] = (index, relative_path)

            files.pop(0)
            if len(files) == 0:
                break
        df = df.astype(cols_dict)
        # Sanity check
        assert np.sum(df['frame_filename'] == np.nan) == 0, 'Missing image file'
        assert np.sum(df['gt_filename'] == np.nan) == 0, 'Missing tags file'
        return df

    def visualize(self, args):
        logo('visualizer')

        flag = 0

        while (True):
            if flag == 0:
                # print("Which folder do you want to visualize (train, test, validation)? <exit>")
                # image_dir = input("> ")
                # flag = 1
                #
                # if image_dir == 'exit':
                #     exit(1)
                #
                # class_image_dir = os.path.join(self.path_base, image_dir)
                class_image_dir = self.path_base

                print("Which class? <exit>")
                show_classes(os.listdir(class_image_dir))

                class_name = input("> ")
                if class_name == 'exit':
                    exit(1)

            download_dir = os.path.join(self.path_base, class_name)
            label_dir = os.path.join(self.path_base, class_name, 'Label')

            if not os.path.isdir(download_dir):
                print("[ERROR] Images folder not found")
                exit(1)
            if not os.path.isdir(label_dir):
                print("[ERROR] Labels folder not found")
                exit(1)

            index = 0

            print("""
                --------------------------------------------------------
                INFO:
                        - Press 'd' to select next image
                        - Press 'a' to select previous image
                        - Press 'e' to select a new class
                        - Press 'w' to select a new folder
                        - Press 'q' to exit
                  You can resize the window if it's not optimal
                --------------------------------------------------------
                """)

            show(class_name, download_dir, label_dir, len(os.listdir(download_dir)) - 1, index)

            while True:

                progression_bar(len(os.listdir(download_dir)) - 1, index + 1)

                k = cv2.waitKey(0) & 0xFF

                if k == ord('d'):
                    cv2.destroyAllWindows()
                    if index < (len(os.listdir(download_dir)) - 2):
                        index += 1
                    show(class_name, download_dir, label_dir, len(os.listdir(download_dir)) - 1, index)
                elif k == ord('a'):
                    cv2.destroyAllWindows()
                    if index > 0:
                        index -= 1
                    show(class_name, download_dir, label_dir, len(os.listdir(download_dir)) - 1, index)
                elif k == ord('e'):
                    cv2.destroyAllWindows()
                    break
                elif k == ord('w'):
                    flag = 0
                    cv2.destroyAllWindows()
                    break
                elif k == ord('q'):
                    cv2.destroyAllWindows()
                    exit(1)
                    break

    def bounding_boxes_images(self, args):
        '''
        Args:
            args: image_IsDepiction=None, image_IsGroupOf=None, image_IsInside=None, image_IsOccluded=None, image_IsTruncated=None, limit=10, multiclasses='0', n_threads=None, noLabels=False, sub=None, type_csv='validation', yes=False
        Returns: Pandas.DataFrame() amb una row per imatge i almenys les columnes: ['frame_filename', 'gt_filename', 'width', 'height']
        '''
        logo('downloader')

        if self.class_names is None:
            print(bc.FAIL + 'Missing classes argument.' + bc.ENDC)
            exit(1)

        csv_data_files = ['train', 'validation', 'test']
        folder_reverse = {f:i for i, f in enumerate(csv_data_files)}
        file_list = [word + '-annotations-bbox.csv' for word in csv_data_files]

        print('Reading CSV files...')
        df_all = [TTV(self.path_base, file_list[folder_reverse['train']], args['yes']),
                  TTV(self.path_base, file_list[folder_reverse['validation']], args['yes']),
                  TTV(self.path_base, file_list[folder_reverse['test']], args['yes'])]

        class_list = self.class_names
        print(bc.INFO + 'Downloading {} together.'.format(class_list) + bc.ENDC)
        dataset_name = '-'.join(class_list).replace(' ','_')
        mkdirs(self.path_base, dataset_name)

        error_csv(self.name_file_class, self.path_base, args['yes'])

        class_dict = {}
        self.oid_2_ds = {}
        for index, class_name in enumerate(class_list.copy()):
            found = False
            for oid_class_name in self.df_classes[1]:
                if str.lower(oid_class_name) == class_name:
                    class_list.remove(class_name)
                    class_list.append(oid_class_name)
                    found = True
                    class_dict[oid_class_name] = self.df_classes.loc[self.df_classes[1] == oid_class_name].values[0][0]
                    self.oid_2_ds[oid_class_name] = index
                    break
            if not found:
                print(f'Label {class_name} not found. Ignoring it.')
                class_list.remove(class_name)

        for class_name in class_list:
            for i in range(3):
                df_val = df_all[i]
                self.download(args, df_val, csv_data_files[i], class_name, class_dict[class_name], dataset_name, class_list=class_list, threads=int(args['n_threads']))

    def download(self, args, df_val, folder, class_name, class_code, download_dir, class_list=None, threads=20):
        '''
        Manage the download of the images and the label maker.
        :param args: argument parser. Dataset='/home/globalcaos/datasets/open_image_dataset', classes=['Apple', 'Orange'], command='downloader', image_IsDepiction=None, image_IsGroupOf=None, image_IsInside=None, image_IsOccluded=None, image_IsTruncated=None, limit=10, multiclasses='0', n_threads=None, noLabels=False, sub=None, type_csv='validation', yes=False
        :param df_val: DataFrame Values
        :param folder: train, validation or test. 'validation'
        :param class_name: self explanatory. 'Apple'
        :param class_code: self explanatory. '/m/014j1m'
        :param class_list: list of the class if multiclasses is activated. None
        :param threads: number of threads, 20
        :return: None
        '''
        columns = 100
        l = int((int(columns) - len(class_name)) / 2)

        print('\n' + bc.HEADER + '-' * l + class_name + '-' * l + bc.ENDC)
        print(bc.INFO + 'Downloading images.' + bc.ENDC)
        df_val_images = images_options(df_val, args)

        images_list = df_val_images['ImageID'][df_val_images.LabelName == class_code].values
        images_set = set(images_list)
        print(bc.INFO + '[INFO] Found {} online images for {}.'.format(len(images_set), folder) + bc.ENDC)

        if args['limit'] is not None:
            import itertools
            print(bc.INFO + 'Limiting to {} images.'.format(args['limit']) + bc.ENDC)
            images_set = set(itertools.islice(images_list, args['limit']))

        dataset_name = '-'.join(class_list).replace(' ','_')

        self.download_img(folder, dataset_name, images_set, threads, download_dir)
        self.get_label(folder, class_name, class_code, df_val, dataset_name, args, download_dir)

    def download_img(self, folder, dataset_name, images_list, threads, download_dir):
        '''
        Download the images.
        :param folder: train, validation or test
        :param dataset_name: self explanatory
        :param images_list: list of the images to download
        :param threads: number of threads
        :return: None
        '''
        image_dir = folder
        download_dir = os.path.join(self.path_base, download_dir)
        downloaded_images_list = [f.split('.')[0] for f in os.listdir(download_dir)]
        images_list = list(set(images_list) - set(downloaded_images_list))

        pool = ThreadPool(threads)

        if len(images_list) > 0:
            print(bc.INFO + 'Download of {} images in {}.'.format(len(images_list), folder) + bc.ENDC)
            commands = []
            for image in images_list:
                image_name = str(image) + '.jpg'
                path = image_dir + '/' + image_name + ' "' + download_dir + '"'
                command = 'aws s3 --no-sign-request --only-show-errors cp s3://open-images-dataset/' + path
                commands.append(command)

            list(tqdm(pool.imap(os.system, commands), total=len(commands)))

            print(bc.INFO + 'Done!' + bc.ENDC)
            pool.close()
            pool.join()
        else:
            print(bc.INFO + 'All images already downloaded.' + bc.ENDC)

    def get_label(self, folder, dataset_name, class_code, df_val, class_list, args, download_dir):
        '''
        Make the label.txt files
        :param folder: train, validation or test
        :param dataset_name: self explanatory
        :param class_code: self explanatory
        :param df_val: DataFrame values
        :param class_list: list of the class if multiclasses is activated
        :return: None
        '''
        if not args['noLabels']:
            print(bc.INFO + 'Creating labels for {} of {}.'.format(dataset_name, folder) + bc.ENDC)

            download_dir = os.path.join(self.path_base, download_dir)
            label_dir = os.path.join(download_dir, 'Label')

            downloaded_images_list = [f.split('.')[0] for f in os.listdir(download_dir) if f.endswith('.jpg')]
            images_label_list = list(set(downloaded_images_list))

            groups = df_val[(df_val.LabelName == class_code)].groupby(df_val.ImageID)
            for image in images_label_list:
                try:
                    current_image_path = os.path.join(download_dir, image + '.jpg')
                    dataset_image = cv2.imread(current_image_path)
                    boxes = groups.get_group(image.split('.')[0])[['XMin', 'XMax', 'YMin', 'YMax']].values.tolist()
                    file_name = str(image.split('.')[0]) + '.txt'
                    file_path = os.path.join(label_dir, file_name)
                    if os.path.isfile(file_path):
                        f = open(file_path, 'a')
                    else:
                        f = open(file_path, 'w')

                    for box in boxes:
                        box[0] *= int(dataset_image.shape[1])
                        box[1] *= int(dataset_image.shape[1])
                        box[2] *= int(dataset_image.shape[0])
                        box[3] *= int(dataset_image.shape[0])

                        # each row in a file is name of the class_name, XMin, YMix, XMax, YMax (left top right bottom)
                        print(dataset_name, box[0], box[2], box[1], box[3], file=f)

                except Exception as e:
                    pass

            print(bc.INFO + 'Labels creation completed.' + bc.ENDC)


if __name__=='__main__':
    # path = 'tests/test_data/blender_data'
    path = '/mnt/datasets/datasets/open_image_dataset'
    filepath = Path(os.getcwd())
    path = os.path.abspath(filepath / '..' / path)
    print(f'Dir: {path}')
    import albumentations as A
    transform = A.Compose(
        [
         # A.Flip(p=0.3),
         # A.RandomBrightnessContrast(p=0.3),
         # A.ShiftScaleRotate(p=0.5),
         # A.RGBShift(r_shift_limit=30, g_shift_limit=30, b_shift_limit=30, p=0.3),
         # A.FancyPCA(p=1),
         # A.ISONoise(p=1),
         # A.RandomResizedCrop(1080, 1920, p=1),
         # A.RandomSizedBBoxSafeCrop(height=1080, width=1920, p=1),
         # A.MotionBlur(blur_limit=7, p=1),
         A.augmentations.transforms.ToFloat(p=1),
         ],
        bbox_params=A.BboxParams(format='pascal_voc', label_fields=['labels', 'class_names'], min_area=4500, min_visibility=0.2))
    # dset = OpenImageDataset(path, ['Person', 'Human eye', 'Human mouth', 'Human foot', 'Human leg', 'Human ear', 'Human hair', 'Human head', 'Human face', 'Human arm', 'Human nose', 'Human hand'], transforms=transform)
    dset = OpenImageDataset(path, ['person', 'forklift'], transforms=transform)
    # for image in dset:
    #     bboxes = torch.from_numpy(image.getRegionsX1Y1X2Y2())
    #     labels = torch.from_numpy(image.getClasses())
    #     image = image.switchColors().getImage()
    #     display_image_gt(image, bboxes, labels, waitkey=0, size=(1024, 768))
    loader = DataLoader(dset, batch_size=4, drop_last=True, shuffle=True, num_workers=0, collate_fn=collaters.coll_stacks_no_norm)
    n = 0
    base_filename = Path(path) / 'ds_output'
    for images, bboxes, labels in loader:
        display_batch_gt(images, bboxes, labels, waitkey=0)
        print(f'Batch {n}')
        n+=1
    print('FIN')



