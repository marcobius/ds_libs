"""
Deep Solutions
Created: 03/3/22
Genera un dataset kitti para entrenamiento de FORKLIFT
"""
from ds.dataset.blender_dataset import BlenderDataset
import sys
from ds.dataset.via_dataset import ViaDataset
from pathlib import Path
import cv2

from ds.img_utils.display import plot_one_box

sys.path.append('../../')
print(f'PYTHONPATH: {sys.path}')


def show_kitti_labeled_image(img_filename, bbox_filename):
    names = ['Car', 'Van', 'Truck', 'Pedestrian', 'Person_sitting', 'Cyclist', 'Tram', 'Misc', 'DontCare']
    img = cv2.imread(f'{img_filename}')
    with open(bbox_filename, 'r') as f:
        labels = f.readlines()
    for line in labels:
        line = line.split()
        lab, _, _, _, x1, y1, x2, y2, _, _, _, _, _, _, _, _ = line
        x1, y1, x2, y2 = map(int, list(map(float, [x1, y1, x2, y2])))
        if lab != 'DontCare':
            plot_one_box(img, (x1, y1, x2, y2), color=(128, 255, 1), line_thickness=2)
    img = cv2.resize(img, (640, 480))
    cv2.imshow('', img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def show_kitti(path_base, index=None):
    images = list(Path(Path(path_base) / 'image_2').glob('*'))
    labels = list(Path(Path(path_base) / 'label_2').glob('*'))
    images.sort()
    labels.sort()
    if index is None:
        index = range(0,len(images))
    for i in index:
        show_kitti_labeled_image(images[i], labels[i])


if __name__ == '__main__':
    dset = ViaDataset('/mnt/datasets/imagenet', ['persona', 'forklift', 'cuernos', 'carga', 'conductor', 'carga_fl'], via_json_name='forklift_imagenet_con_conductor_carga_fl.json')
    dset.index_fotos = dset.index_fotos.sort_values(by=['frame_filename'])
    dset.index_fotos = dset.index_fotos.drop_duplicates()
    dset.index_fotos = dset.index_fotos.reset_index(drop=True)
    dset.export_2_kitti('/mnt/datasets/kitti_train_forklift_carga_fl', resolution=(1920, 1080), inici=None, final=None)
    # Ajuntem dos datasets pero exportem en dos tems
    # aixo ho podem fer per que kitti te estructura de imatges en carpetes de img+labels, no hi ha un index general (es optatiu)
    dset_b = BlenderDataset('/mnt/datasets/vehicles/forklift/3D_generated/renders/images', ['person', 'forklift', 'cuernos', 'carga', 'conductor', 'carga_fl'])
    dset_b.export_2_kitti('/mnt/datasets/kitti_train_forklift_carga_fl', resolution=(1920, 1080), inici=None, final=None)

    # Visualizo
    num_images = len(dset)+len(dset_b)
    import random
    index = []
    for i in range(0,10):
        index.append(random.randint(0, num_images))
    show_kitti('/mnt/datasets/kitti_train_forklift_carga_fl', index=index)
    print("FIN")
