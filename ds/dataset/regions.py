"""
Deep Solutions
Created: 30/6/21
"""
import numpy as np
import logging

logger = logging.getLogger(__name__)

class Bbox(object): # Region Of Interest
    """
    This class represents a detection or tag in a single image.

    Parameters
    ----------
    ltwh : array_like
        Bounding box in format `(x, y, w, h)`.
    confidence : float
        Detector confidence score.
    clase: int
        Class of the detection

    Attributes
    ----------
    ltwh : ndarray
        Bounding box in format `(top left x, top left y, width, height)`.
    confidence : ndarray
        Detector confidence score.
    clase: int
        Class of the detection
    """

    def __init__(self, ltwh, confidence, clase, unique_id):
        self.ltwh = np.asarray(ltwh, dtype=float)
        self.confidence = float(confidence)
        self.clase = clase
        self.unique_id = unique_id
        logger.debug(f"Region created with ltwh={ltwh} and clase={clase} and id={unique_id}")
        # self.color = (1,1,1,1)  #TODO: eliminar este atributo, no tiene sentido en una Region genérica
        # self.position = np.asarray([0,0]) #TODO: eliminar este atributo, la region sabe de ltwh, no sabe nada de np arrays de 2 posiciones

    def to_tlbr(self):
        """Convert bounding box to format `(min x, min y, max x, max y)`, i.e.,
        `(top left, bottom right)`.
        """
        ret = self.ltwh.copy()
        ret[2:] += ret[:2]
        return ret

    def to_xyah(self):
        """Convert bounding box to format `(center x, center y, aspect ratio,
        height)`, where the aspect ratio is `width / height`.
        """
        ret = self.ltwh.copy()
        ret[:2] += ret[2:] / 2
        ret[2] /= ret[3]
        return ret

    def serialize(self):
        """
        Serializes the detection (compatible with msg brokers)
        Returns: {"ltwh":[T,L,W,H], "confidence":confidence, "clase":clase}
        """
        L,T,W,H = self.ltwh.tolist()
        return {"ltwh":[L,T,W,H], "confidence":self.confidence, "clase":self.clase, "unique_id":self.unique_id}

    @staticmethod
    def de_serialize(dic):
        """
        Crea un objeto Region a partir de un diccionario serializado por serialize()
        Args:
            dic: {"ltwh":[L,T,W,H], "conf":confidence, "clase":clase}
        Returns: Region
        """
        return Bbox(**dic)

    def get_box_points(self):
        l,t,w,h = self.ltwh.tolist()
        return np.asarray([[l,t],[l+w,t],[l+w,t+h],[l,t+h]])

    def get_clase(self):
        return self.clase

class ExtendedBbox():
    def __init__(self, camera_id: int, bbox: Bbox, pixel_position, real_position, timestamp):
        self.camera_id = camera_id
        self.bbox = bbox
        self.pixel_position = pixel_position
        self.real_position = real_position
        self.timestamp = timestamp

    def get_class(self):
        return self.bbox.clase

    def get_sc_track_id(self):
        return self.bbox.unique_id

    def get_id(self):
        return self.get_sc_track_id()

    def get_cam_id(self):
        return self.camera_id

    def get_real_position(self):
        return self.real_position

class FrameMetadata(object):
    """
    Agrupacio de regions d'un frame o imatge
    """
    def __init__(self, frame_number):
        self.frame_number = frame_number
        self.bboxes = []
        logger.debug(f"FrameMetadata created")

    # def set_region(self, index, bbox):
    #     self.bboxes[index] = bbox
    #     logger.debug(f"FrameMetadata Region set on position {index}: {bbox}")

    def add(self, bbox):
        self.bboxes.append(bbox)
        logger.debug(f"FrameMetadata Region added: {bbox}, {len(self.bboxes)} regions")

    def __len__(self):
        return len(self.bboxes)

    def serialize(self):
        """
        Serializes the FrameMetaData
        Returns: {"frame_number": frame_number, "regions": [{"ltwh":[L,T,W,H], "confidence":confidence, "clase":clase}]}
        """
        lista = []
        for bbox in self.bboxes:
            lista.append(bbox.serialize())
        ser = {'frame_number': self.frame_number, 'regions':lista}
        return ser

    @staticmethod
    def de_serialize(frame_data):
        """
        Crea un objeto FrameMetadata poblado con las regions que correspondan a partir de un listado de diccionarios
        Args:
            frame: {"frame_number": frame_number, "regions": [{"ltwh":[L,T,W,H], "confidence":confidence, "clase":clase}]}
        Returns: FrameMetadata
        """
        frame = FrameMetadata(frame_data['frame_number'])
        lista = frame_data['regions']
        for i, dic in enumerate(lista):
            frame.add(Bbox.de_serialize(dic))
        return frame

    def has_tracks(self):
        return len(self.bboxes) > 0


class BatchMetadata(object):
    """
    Agrupacio de FrameMetadata
    """
    def __init__(self):
        self.frames = []
        logger.debug(f"BatchMetadata created")

    def set_frame(self, index, frame_data):
        self.frames[index] = frame_data

    def add(self, frame_data):
        self.frames.append(frame_data)
        logger.debug(f"Frame added to batch, total {len(self.frames)} frames")

    def __len__(self):
        return len(self.frames)

    def serialize(self):
        """
        Serializes de object
        Returns: {"frames":[[{"ltwh":[L,T,W,H], "confidence":confidence, "clase":clase}]]}
        """
        lista_frames = []
        for frame in self.frames:
            lista_frames.append(frame.serialize())
        return {'frames': lista_frames}
        # return lista_frames

    @staticmethod
    def de_serialize(dic):
        batch_meta = BatchMetadata()
        lista = dic['frames']
        for i, frame in enumerate(lista):
            batch_meta.add(FrameMetadata.de_serialize(frame))
        return batch_meta

    def has_tracks(self):
        te_data = False
        for frame in self.frames:
            te_data = te_data or frame.has_tracks()
        return te_data

