import pandas
import os
from torch.utils.data.dataset import Dataset as torch_dataset
from torch.utils.data import DataLoader
from pathlib import Path
from tqdm import tqdm
import csv
import numpy as np
import cv2
import torch

from ds.dataset.base_dataset import BaseDataset
from ds.dataset.images import BlenderImage
import ds.dataset.collaters as collaters
from img_utils.img_utils.display import display_batch_gt, display_image_gt
from img_utils.img_utils.files import get_image_shape

INDEX_FILENAME = 'index.csv'  #TODO: hacer que sea un parametro de la constructora del dataset



class BlenderDataset(BaseDataset):
    """
    Dataset de imagenes con GT generados desde Blender utilizando vision_blender
    """

    def prepara_index(self):
        """
        Busca todas las fotos y las mete en un csv, retorna un pandas con las fotos.
        Se supone que en la carpeta del render (path) puede haber varias sesiones/renders, diferenciados por diferentes prefijos.
        Cada prefijo, ademas es también un nombre de directorio (donde encontrar los npz).
        Por tanto esta función hace un bucle por prefijos encontrados en el directorio pasado por parametro.
        Si el indice (csv) ya existia no lo recrea
        :param path: str o Path
        :return: Pandas.DataFrame
        """
        path = self.path_base
        index_filename = self.index_filename
        path = Path(path)  # OS agnostic
        if not os.path.isdir(path):
            print(f'Directorio no encontrado: {path}')
            raise NotADirectoryError
        if not os.path.isfile(path / index_filename):
            # Si no existe el fichero de indice, lo creo
            labels = ['frame_filename', 'gt_filename', 'frame_number', 'width', 'height']
            file = open(path / index_filename, mode='w')
            csv_writer = csv.writer(file, delimiter=',')
            csv_writer.writerow(labels)
            # Lista de renders que hay en el directorio (directorios distintos)
            lista_prefijos = [x for x in path.iterdir() if x.is_dir()]
            try:
                lista_prefijos.pop(lista_prefijos.index(path / 'labels'))
            except ValueError:
                pass
            for prefijo in lista_prefijos:
                files = list(path.glob(
                    f'{prefijo.name}????.jpg'))  # TODO: filtrar solo las imagenes, lo tenia hecho en algun sitio, hay funcion
                files += list(path.glob(f'{prefijo.name}????.gif'))
                files += list(path.glob(f'{prefijo.name}????.png'))
                files.sort()
                print(f'Preparando indice para prefijo {prefijo} ...')
                for i in tqdm(range(len(files))):
                    dir, filename = os.path.split(files[i])
                    h, w, _ = get_image_shape(str(files[i]))
                    # Calcular el nombre del npz con la GT para esta imagen
                    frame_number = filename[len(prefijo.name):]
                    frame_number, ext = os.path.splitext(frame_number)
                    npz_filename = Path(prefijo.name) / f'{frame_number}.npz'  # prefijo no ha dejado de ser un Path
                    if os.path.isfile(path / npz_filename):
                        csv_writer.writerow([files[i].name, npz_filename, f'{frame_number}', w, h])
                    else:
                        print(
                            f'WARNING: imagen sin fichero npz ({npz_filename})')  # TODO: evitar que el glob coja el *_index.csv
                        # csv_writer.writerow([os.path.abspath(files[i]), '', f'{frame_number}'])
            file.close()
        else:
            print(f'El fichero de indice ya existe. Reaprovechando {path / INDEX_FILENAME}')
        # Cargo en un pandas el fichero de indide y lo retorno
        index = pandas.read_csv(path / INDEX_FILENAME, dtype=str)  # para que tome todas las columnas como strings
        return index

    def __getitem__(self, item): #TODO: implementar una cache amb un diccionari o hash per imatges (a no ser que ja ho facin els loaders de torch)
        """
        Retorna una ds.Image
        Args:
            item: int indice

        Returns: tagged_image.Image el objeto imagen contendrá las regions, por tanto las labels o targets
        """
        image_filename = self.index_fotos.iloc[item].frame_filename
        bboxes_filename = self.index_fotos.iloc[item].gt_filename
        image = BlenderImage()
        image.loadFromFile(Path(self.path_base) /image_filename)
        # para validación y training es necesario cargar tambien los labels (o targets)
        if self.mode != 'test':
            image.load_labels_v2(str(Path(self.path_base) / bboxes_filename))
            # Ja estan totes les dades a memoria, pero cal adaptarles per poder passarles per les albumentations
            # classes = np.zeros(len(self.class_names))
            classes_index = image.getClasses().astype(int)
            # classes[classes_index-1]=1  # posem a 1 les clases etiquetades
            #TODO: ALERTA, clarificar si guardamos un vector de classes entero por cada bbox o solo su clase, jugar con herencias
            # image.setClasses(classes) # faig que totes les imatges tinguin un vector de shape igual per les clases
            image.setClassNames(self.get_labels(classes_index-1))
            if self.transforms:
                image_dict = {'image': image.getImage(), 'bboxes': image.getRegionsX1Y1X2Y2(), 'labels': image.getClasses(), 'class_names':image.getClassNames()}
                image_dict = self.transforms(**image_dict)
                image.setImage(image_dict['image'])
                if len(image_dict['bboxes'])>0:
                    image.setRegionsX1Y1X2Y2(image_dict['bboxes'])
                    image.setClasses(image_dict['labels'])
                    image.setClassNames(image_dict['class_names'])
                else:
                    # print(f'bboxes eliminadas')
                    image.setRegionsX1Y1X2Y2(np.asarray([]))
                    image.setClasses(np.asarray([]))
                    image.setClassNames(np.asarray([]))

        else:
            if self.transforms:
                # test dataset must have some values so that transforms (albumentations) work.
                cls = torch.as_tensor([[0]], dtype=torch.float32),
                bbox = torch.as_tensor([[0, 0, 0, 0]], dtype=torch.float32),
                image_dict = {'image': image.getImage(), 'bboxes': bbox, 'labels': cls}
                image.setImage(self.transforms(**image_dict)['image'])
        return image



# ##########################################################################################
# ##########    MAIN       #################################################################
# ##########################################################################################

if __name__=='__main__':
    # path = '/mnt/src/vision_blender/renders'
    path = '/mnt/datasets/forklift/renders/images'
    path = '/mnt/datasets/3D_generated/formula_one/renders/images'
    filepath = Path(os.getcwd())
    path = os.path.abspath(filepath / '..' / path)
    print(f'Dir: {path}')
    import albumentations as A
    transform = A.Compose(
        [
         # A.Flip(p=0.3),
         # A.RandomBrightnessContrast(p=0.3),
         # A.ShiftScaleRotate(p=0.5),
         # A.RGBShift(r_shift_limit=30, g_shift_limit=30, b_shift_limit=30, p=0.3),
         # A.FancyPCA(p=1),
         # A.ISONoise(p=1),
         # A.RandomResizedCrop(1080, 1920, p=1),
         # A.RandomSizedBBoxSafeCrop(height=1080, width=1920, p=1),
         # A.MotionBlur(blur_limit=7, p=1),
         A.augmentations.transforms.ToFloat(p=1),
         ],
        bbox_params=A.BboxParams(format='pascal_voc', label_fields=['labels', 'class_names'], min_area=4500, min_visibility=0.2))
    # dset = BlenderDataset(path, ['box1', 'box2'], transforms=transform)
    # dset = BlenderDataset(path, ['persona', 'forklift', 'cuernos', 'carga'], transforms=transform)
    dset = BlenderDataset(path, ['f1_car'], transforms=transform)
    # for image in dset:
    #     bboxes = torch.from_numpy(image.getRegionsX1Y1X2Y2())
    #     labels = torch.from_numpy(image.getClasses())
    #     image = image.switchColors().getImage()
    #     display_image_gt(image, bboxes, labels, waitkey=0, size=(1024, 768))
    loader = DataLoader(dset, batch_size=4, drop_last=True, shuffle=True, num_workers=0, collate_fn=collaters.coll_stacks_no_norm)
    n = 0
    base_filename = Path(path) / 'ds_output'
    for images, bboxes, labels in loader:
        display_batch_gt(images, bboxes, labels, waitkey=0)
        print(f'Batch {n}')
        n+=1
    print('FIN')
