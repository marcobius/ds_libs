"""
Deep Solutions
Created: 03/06/2021

Dataset Base del que penjen els datasets de DS
"""
from operator import contains
from torch.utils.data.dataset import Dataset as torch_dataset
import numpy as np
import os
from tqdm import tqdm
from pathlib import Path
from ds.dataset.base_dataset_exports import crea_yolo_cfg_file

class BaseDataset(torch_dataset):
    """
    Dataset generic. Es abstracte en algunes funcions.
    Si s'utilitza directament dispara excepcions de codi no implementat.
    """
    def __init__(self, pathBase, class_names, transforms=None, mode='train', train_val_test_split=[1.0, 0.0, 0.0], set='all', seed=1, index_filename='index.csv'):
        """
        Constructora
        Args:
            pathBase: str o Path del directori base on buscar les imatges
            class_names: llistat de str amb els noms de les clases
            transform: albumentations.Compose() de les transformacions
            mode: str -> train/val/test. El comportament del getitem() canvia en funció d'aquest estat
            train_val_test_split: [float, float, float] de la subdivisió del dataset
            set: defineix quines imatges retorna el dataset dintre de la divisió anterior , ['train', 'val', 'validation', 'test', 'all']
            seed: llavor per als randoms
            index_filename: opcional, nom del fitxer d'index que es crearà
        """
        self.path_base = pathBase
        self.class_names = class_names
        self.transforms = transforms
        self.mode = mode
        self.idx = 0 # punter a la posició de la foto actual
        self.set = set
        self.index_filename = index_filename
        self.index_fotos = self.prepara_index() #pandas dataframe
        # minim_columns = ['frame_filename', 'gt_filename', 'width', 'height']
        minim_columns = ['frame_filename', 'width', 'height']
        rebudes = self.index_fotos.columns.to_list()
        intersec = [value for value in minim_columns if value in rebudes]
        if len(minim_columns)!=len(intersec):
            # si la intersecció no es tot el minim, tenim una funció de prepare mal implementada
            raise ValueError(f"Falten columnes obligatories a l'index. Columnes obligatories: {minim_columns}. Columnes rebudes {rebudes}")
        # Separació train/val/test
        assert sum(train_val_test_split) == 1, 'Bad argument train_val_test_split'
        np.random.seed(seed)
        # Nova columna per separar sets
        self.index_fotos['tr_va_te'] = np.random.choice(['train', 'val', 'test'], size=len(self.index_fotos), p=train_val_test_split)
        # Limitacio de dataset per subsets de torch
        assert set in ['train', 'val', 'validation', 'test', 'all'], "Invalid set param"
        self.set = set
        if self.set == 'train':
            self.index_fotos = self.index_fotos[self.index_fotos['tr_va_te'] == 'train']
        elif self.set == 'val' or self.set == 'validation':
            self.index_fotos = self.index_fotos[self.index_fotos['tr_va_te'] == 'val']
        elif self.set == 'test':
            self.index_fotos = self.index_fotos[self.index_fotos['tr_va_te'] == 'test']
        else:
            # Se usa el dataset entero
            pass

    def __len__(self):
        return len(self.index_fotos)

    def __iter__(self):
        self.idx=0
        return self

    def __next__(self):
        if self.idx == len(self.index_fotos):
            raise StopIteration
        self.idx += 1
        return self.__getitem__(self.idx-1)

    def __getitem__(self, item):
        """
        Args:
            item:

        Returns:

        """
        raise NotImplementedError

    def prepara_index(self):
        """
        Funció encarregada de poblar l'index de fotos.
        En alguns casos es una busqueda de directoris, en altres un download d'internet, etc..
        Returns: Pandas.DataFrame() amb una row per imatge i almenys les columnes: ['frame_filename', 'gt_filename', 'width', 'height']
        """
        raise NotImplementedError

    def get_labels(self, classes):
        """
        Retorna els class_names associats a cada index del llistat de clases
        Exemple:
        class_names = ['persona', 'cotxe', 'barco']
        classes = [0,3]
        get_labels(classes) --> ['persona', 'barco']
        Args:
            classes: [int]

        Returns: [str]
        """
        lista = []
        for c in classes:
            lista.append(self.class_names[int(c)])
        return lista

    def export_2_kitti(self, path_destino, inici=None, final=None, train_val=0.8, resolution=(1920, 1080), rescale=True, force_copy=False):
        """
        Genera una còpia del dataset amb format kitti (en lloc de copiar els fitxers d'imatge fa un link)
        El dataset kitti té estructura carpeta de classes i carpeta de labels amb un index json a la arrel.

        |--dataset root
          |-- images
              |-- 000000.jpg
              |-- 000001.jpg
                    .
                    .
              |-- xxxxxx.jpg
          |-- labels
              |-- 000000.txt
              |-- 000001.txt
                    .
                    .
              |-- xxxxxx.txt
          |-- kitti_seq_to_map.json

        Cada txt te el format:
        #Values    Name      Description
        ----------------------------------------------------------------------------
            1    type(str)         Describes the type of object: 'Car', 'Van', 'Truck',
                                'Pedestrian', 'Person_sitting', 'Cyclist', 'Tram',
                                'Misc' or 'DontCare'
            1    truncated(float)    Float from 0 (non-truncated) to 1 (truncated), where
                                truncated refers to the object leaving image boundaries
            1    occluded(int)     Integer (0,1,2,3) indicating occlusion state:
                                0 = fully visible, 1 = partly occluded
                                2 = largely occluded, 3 = unknown
            1    alpha(float)        Observation angle of object, ranging [-pi..pi]
            4    bbox         2D bounding box of object in the image (0-based index):
                                contains left, top, right, bottom pixel coordinates
            3    dimensions   3D object dimensions: height, width, length (in meters)
            3    location     3D object location x,y,z in camera coordinates (in meters)
            1    rotation_y   Rotation ry around Y-axis in camera coordinates [-pi..pi]
            1    score        Only for results: Float, indicating confidence in
                                detection, needed for p/r curves, higher is better.
        Args:
            path_destino:
            inici:
            final:
            train_val:
            resolution:

        Returns: None
        """
        if inici is None:
            inici = 0
        if final is None:
            final = len(self.index_fotos)-1
        assert 0 <= inici < len(self.index_fotos), f"Inici ha d'estar dins l'interval [0..{len(self.index_fotos) - 1}]"
        assert 0 < final < len(self.index_fotos), f"Final ha d'estar dins l'interval [0..{len(self.index_fotos) - 1}]"
        assert final > inici, f"Final ha de ser major que inici."
        # Preparar directori desti
        path_destino = Path(path_destino)
        os.makedirs(path_destino, exist_ok=True)
        path_labels = path_destino / 'label_2'
        os.makedirs(path_labels, exist_ok=True)
        path_images = path_destino/'image_2'
        os.makedirs(path_images, exist_ok=True)
        # Crear links i generar fitxers de labels
        print(f"Generating a links to images or copy of images in {path_images} and labels files in {path_labels}")
        print("Link if the size is correct, new file image if it has to be resized")
        for i in tqdm(range(inici, final)):
            tagged_im = self.__getitem__(i)
            image_filename = self.index_fotos.iloc[i].frame_filename
            h,w = tagged_im.getImageShape()
            dst_image_filename = path_images / Path(image_filename).name
            if force_copy:
                if rescale and (h,w)!=resolution:
                    tagged_im.scale(size=resolution)
                tagged_im.save_jpg(filename=f'{dst_image_filename}')
            else:
                if rescale and (h,w)!=resolution:
                    # Tamany diferent --> reescalo i creo fitxer nou
                    tagged_im.scale(size=resolution)
                    tagged_im.save_jpg(filename=f'{dst_image_filename}')
                else:
                    # No reescalo --> faig link a imatge pq no canvia
                    try:
                        dst_image_filename.symlink_to((Path(self.path_base) / Path(image_filename)).resolve().absolute())
                    except Exception as e:
                        print(f"WARNING: problem creating link to file: {(Path(self.path_base) / Path(image_filename)).resolve().absolute()}")
            # Crea el fitxer de labels
            filename = path_labels / (Path(image_filename).stem + '.txt')
            with open(filename, "w") as file:
                for region in tagged_im.getRegionsList():
                    file.write(region.asKitty())
        """The kitti_seq_to_map.json file contains a sequence to frame id mapping for the frames in the images directory. 
        This is an optional file, and is useful if the data needs to be split into N folds sequence wise. 
        In case the data is to be split into a random 80:20 train:val split, then this file may be ignored.
        """
        # crea_kitti_json_file(path_destino, train_val)


    def export_2_yolo(self, path_destino, inici=None, final=None, train_val=0.8):
        """
        Genera una copia del dataset amb format yolo (enlloc de copiar els fitxers d'imatge, fa un link)
        Genera un directorio labels con un fichero .txt para cada imagen (a la altura del propio directorio de imagenes).
        Cada fichero contiene las labels de la imagen en formato CenterX, CenterY, Width, Height
        Normalizado a coordenadas entre 0 y 1
        Cada linea del fichero es:
        <clase> <cx> <cy> <w> <h>
        Returns: None
        """
        if inici is None:
            inici = 0
        if final is None:
            final = len(self.index_fotos)-1
        assert inici>=0 and inici<len(self.index_fotos), f"Inici ha d'estar dins l'interval [0..{len(self.index_fotos)-1}]"
        assert final>0 and final<len(self.index_fotos), f"Final ha d'estar dins l'interval [0..{len(self.index_fotos)-1}]"
        assert final>inici, f"Final ha de ser major que inici."
        # Preparar directori desti
        path_destino = Path(path_destino)
        os.makedirs(path_destino, exist_ok=True)
        path_labels = path_destino / 'labels'
        os.makedirs(path_labels, exist_ok=True)
        path_images = path_destino/'images'
        os.makedirs(path_images, exist_ok=True)
        # Crear links i generar fitxers de labels
        print(f"Generating a link to images in {path_images} and labels files in {path_labels}")
        for i in tqdm(range(inici, final)):
            bl_image = self.__getitem__(i)
            bboxes = bl_image.getRegionscXcYWH_Norm()
            classes = bl_image.getClasses()
            image_filename = self.index_fotos.iloc[i].frame_filename
            # Crea link a la imatge
            dst_image_filename = path_images / Path(image_filename).name
            try:
                dst_image_filename.symlink_to((Path(self.path_base) / Path(image_filename)).resolve().absolute())
            except Exception as e:
                print(f"WARNING: problem creating link to file: {(Path(self.path_base) / Path(image_filename)).resolve().absolute()}")
            # Crea el fitxer de labels
            filename = path_labels / (Path(image_filename).stem + '.txt')
            with open(filename, "w") as file:
                for j in range(len(classes)):
                    cx, cy, w, h = bboxes[j, :]
                    clase = classes[j]-1
                    if clase==-1:
                        raise ValueError(f"File with CLASS==0 in: {image_filename}")
                    line = f'{clase} {cx} {cy} {w} {h}\n'
                    file.write(line)
        crea_yolo_cfg_file(path_destino, self.class_names, train_val=train_val)
        return None

    def export_2_via(self): #TODO: añadir un directorio de destino de manera que se cree una copia de las imagenes con los labels correspondientes
        """
        Genera un arxiu .json amb les bboxes i referències a imatges al directori base
        Returns: None
        """
        from ds.dataset.via_dataset import ViaDataset
        via_dset = ViaDataset(self.path_base, self.class_names, df_to_import=self.index_fotos)

        print('Adding tags to VIA...')
        via_dset.addDatasetTags(self.class_names)
        # for i, row in tqdm(self.index_fotos.iterrows()):
        for i in tqdm(range(len(self.index_fotos))):
            row = self.index_fotos.iloc[i]
            filename = row['frame_filename']
            tagged_image = self.__getitem__(i)
            # bboxes = tagged_image.getRegionsX1Y1X2Y2()
            # classes = tagged_image.getClasses()
            via_dset.addTaggedImageBboxes(filename, tagged_image)

        via_dset.saveProjectDict()
        return None


