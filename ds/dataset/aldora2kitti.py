import sys
import os
from pathlib import Path
print(f"PATH: {sys.path}")
sys.path.insert(0, str((Path(os.getcwd())/"..").resolve())) #depen d'on s'executi fa falta una o l'altre
sys.path.insert(0, str((Path(os.getcwd())).resolve()))
print(f"PATH: {sys.path}")
from ds.dataset.via_dataset import ViaDataset


if __name__ == '__main__':
    # Dataset de dev
    path = "/mnt/datasets/drone/aldoratech/batch1"

    dset = ViaDataset(path, ['persona', 'sportsball', 'car'], via_json_name='via_dataset.json')
    dset.index_fotos = dset.index_fotos.sort_values(by=['frame_filename'])
    dset.index_fotos = dset.index_fotos.drop_duplicates()
    dset.index_fotos = dset.index_fotos.reset_index(drop=True)
    dset.export_2_kitti('/mnt/src/tao/tao-experiments/data/mixed_splited_drone/train_pruebas', force_copy=True, resolution=(1920, 1080), rescale=True)

