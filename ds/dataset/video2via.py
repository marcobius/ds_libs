#!/usr/bin/env python3
"""
Deep Solutions
Created: 3/11/21

Utilitat per extreure frames d'un video i guardar-los a un directori d'imatges on trobarem un dataset via sense etiquetar
"""
from ds.dataset.via_dataset import ViaDataset
from ds.dataset.video_loaders import SingleVideoDataset
import ds.dataset.res
import random
from pathlib import Path
import argparse
import shutil
from importlib_resources import files


def parse_args():
    parser = argparse.ArgumentParser(
        # usage="%(prog)s [OPTION] [INPUT_FILE] [OUTPUT_DIR]...",
        description="Saves frames of a video as jpg files and creates a via dataset (if there is a previous via dataset there IT WILL BE DELETED). You can select the proportion of frames to be saved."
    )
    parser.add_argument('-i', '--input_file')
    parser.add_argument('-o', '--output_dir')
    parser.add_argument('-c', '--classes',
                        nargs='+',
                        default=[],
                        help="Space separated list of strings without ''")
    parser.add_argument('-t', '--threshold',
                        help='Proportion of frames to be saved (1.0: saves all frames)',
                        default=0.1)
    parser.add_argument('-s', '--show_images',
                        dest='show_images',
                        action='store_true',
                        help='Shows the saved images.')
    args = parser.parse_args()
    return args


def main():
    args = parse_args()
    # path_destino = Path('../tests/test_data/images/formula')
    path_destino = Path(args.output_dir)
    path_destino.mkdir(parents=True, exist_ok=True)
    # path_video = Path('../tests/test_data/videos/formula1_limits.mp4')
    path_video = Path(args.input_file)
    reader = SingleVideoDataset(f'{path_video}', resize_factor=1.0, transform=None)
    threshold = float(args.threshold)
    random.seed(1)
    num_images = 0
    frames_in_video = 0
    print(f'Number of frames in video: {len(reader)}')
    for image in reader:
        frames_in_video += 1
        if random.random() < threshold:
            num_images += 1
            # display_image_gt(image, [], [], waitkey=0, size=(1024, 768), imshow=True)
            filename = path_destino / Path(f'{path_video.stem}_{frames_in_video-1:04}.jpg')
            image.save_jpg(filename=f'{filename}')
            if args.show_images:
                image.cv2_imshow()
    print(f"Total images saved {num_images} from {frames_in_video} frames in video")
    print(f"Images where saved to directory: {path_destino.resolve()}")

    # CREACIÓ DEL VIA DATASET
    # ALERTA: BORRA EL FITXER JSON ANTERIOR
    index_filename = 'index.csv'
    path_csv_index = path_destino / Path(index_filename)
    via_json_name = 'via_dataset.json'
    path_json_name = path_destino/Path(via_json_name)
    # Guarda copia del json por si acaso (solo una copia)
    if path_json_name.is_file():
        path_json_name.rename((path_destino)/Path(via_json_name+'.bk'))
    # Borra el indice json y crea nuevo (el indice.csv siempre lo borramos)
    path_json_name.unlink(missing_ok=True)
    path_csv_index.unlink(missing_ok=True)
    # Crea dataset (escaneará imagenes i crea los dos ficheros)
    classes = args.classes
    # classes = ['person', 'forklift', 'cuernos', 'carga', 'conductor']
    via_dset = ViaDataset(f'{path_destino}', classes)
    #TODO: corregir error en constructora, no añade las classes al diccionario
    via_dset.addDatasetTags(classes)
    via_dset.saveProjectDict()
    # Copiamos el via.html
    f = files(ds.dataset.res).joinpath('via11.html')
    shutil.copy(str(f), str(path_destino / Path('via11.html')))
    print(f'Via editor saved to {(path_destino / Path("via11.html")).resolve()}')

if __name__ == '__main__':
    main()
