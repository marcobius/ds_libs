"""
Deep Solutions
Created: 10/05/2021

Funciones collate_fn para relacionar Dataset-Trainer
Todos los datasets retornan Image, pero no todos los trainers la saben interpretar.
Estas funciones son las utilizadas por los Dataloaders para unir los batches
"""
import numpy as np
import torch

def coll_np_torch_X1Y1X2Y2(data):
    """
    Recibe lista de imagenes (TaggedImage) y retorna diccionario de np arrays
    Las bboxes las retorna en formato X1Y1X2Y2 normalizados a 1
    Args:
        data: [TaggedImage]

    Returns: {'img':np.array, 'bbox':np.array, 'areas':np.array, 'classes':np.array, 'class_names': list}
    """
    areas, bboxes, classes, imagenes, class_names = [], [], [], [], []
    for image in data:
        areas.append(torch.from_numpy(image.getAreas()))
        bboxes.append(torch.from_numpy(image.getRegionsX1Y1X2Y2_Norm()))
        classes.append(torch.from_numpy(image.getClasses()))
        imagenes.append(image.switchColors().getImage())
        assert type(image.getImage()) == np.ndarray, "Images must be ndarray, do not use ToTensor in Albumentations"
        class_names.append(image.getClassNames())
    bboxes = torch.stack(bboxes)
    imagenes = np.stack(imagenes)
    imagenes = np.transpose(imagenes, (0, 3, 1, 2))
    imagenes = torch.from_numpy(imagenes)
    classes = torch.stack(classes)
    areas = torch.stack(areas)
    return {'img': imagenes, 'bbox': bboxes, 'areas': areas, 'classes': classes, 'class_names': class_names}

def coll_np2torch_infer(data):
    """
    Recibe lista de Image (en formato np) y retorna torch.tensor de las mismas.
    No añade mas atributos pq la funcion es para inferencia
    Args:
        data: [Image]

    Returns: np.array
    """
    imagenes = []
    for image in data:
        imagenes.append(image.switchColors().getImage())
    imagenes = np.stack(imagenes)
    imagenes = np.transpose(imagenes, (0, 3, 1, 2))
    return torch.from_numpy(imagenes)

def coll_np2torch_1_0_infer(data):
    """
    Recibe lista de Image (en formato np) y retorna torch.tensor de las mismas.
    No añade mas atributos pq la funcion es para inferencia
    Args:
        data: [Image]

    Returns: np.array
    """
    import numpy as np
    imagenes = []
    for image in data:
        imagenes.append(image.switchColors().getImage())
    imagenes = np.stack(imagenes)
    imagenes = np.transpose(imagenes, (0, 3, 1, 2))
    imagenes = imagenes.astype(np.float32) / 255.
    return torch.from_numpy(imagenes)

def coll_rcnn(data):
    """
    Args:
        data: llista d'imatges [Image]
    Returns:
        Imatges: XYXY totes en un torch tensor en format BCHW
        BBoxes i Labels: llista de diccionaris de targets [{'boxes':tensor, 'labels':tensor},...] un diccionari per imatge
    """
    imagenes = []
    targets = []
    for image in data:
        dic_targets = {
            'boxes': torch.from_numpy(image.getRegionsX1Y1X2Y2_Norm()),
            'labels': torch.from_numpy(image.getClasses())
        }
        imagenes.append(image.switchColors().getImage())
        targets.append(dic_targets)
    imagenes = np.stack(imagenes)
    imagenes = np.transpose(imagenes, (0, 3, 1, 2))
    imagenes = torch.from_numpy(imagenes)
    return {'images':imagenes, 'targets':targets}

def coll_rcnn_test(data):
    """
    Args:
        data: llista d'imatges [Image]
    Returns:
        Imatges: XYXY totes en un torch tensor en format BCHW
    """
    imagenes = []
    for image in data:
        imagenes.append(image.switchColors().getImage())
    imagenes = np.stack(imagenes)
    imagenes = np.transpose(imagenes, (0, 3, 1, 2))
    imagenes = torch.from_numpy(imagenes)
    return {'images':imagenes}

def coll_stacks_no_norm(data):
    """
    Args:
        data: llista d'imatges [Image]
    Returns:
    """
    imagenes = []
    bboxes = []
    labels = []
    for image in data:
        imagenes.append(image.switchColors().getImage())
        bboxes.append(torch.from_numpy(image.getRegionsX1Y1X2Y2()))
        labels.append(torch.from_numpy(image.getClasses()))
    try:
        imagenes = np.stack(imagenes)
    except ValueError as e:
        # Si arribem aquí eś que les imatges no tenen la mateixa mida
        # Afegim padding a la dreta i abaix pq així no hem de canviar les bboxes
        # TODO: Fer totes aquestes operacions es fan a GPU
        imagenes_padded = []
        max_w, max_h = 0, 0
        for i in range(len(imagenes)):
            image = imagenes[i]
            w, h = image.shape[:2]
            if max_w < w:
                max_w = w
            if max_h < h:
                max_h = h
        for image in imagenes:
            w, h = image.shape[:2]
            if max_w > w or max_h > h:
                print(max_h-h)
                print(max_w-w)
                imagenes_padded.append(np.pad(image, [(0, max_w-w), (0, max_h-h), (0,0)], mode='constant', constant_values=0))
                print(imagenes_padded[-1].shape)
        imagenes = np.stack(imagenes_padded)
    imagenes = np.transpose(imagenes, (0, 3, 1, 2))
    imagenes = torch.from_numpy(imagenes)
    return imagenes, bboxes, labels
