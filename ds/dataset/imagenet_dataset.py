"""
Deep Solutions
Created: 03/06/2021

Utils i datasets per treballar amb Open Images Datasets (OID)
"""
from torch.utils.data import DataLoader
from ds.dataset.base_dataset import BaseDataset
from pathlib import Path
import os
from img_utils.img_utils.display import display_batch_gt
from img_utils.img_utils.files import get_image_shape, download_URL, download_torrent
import torchvision
import pandas as pd
import numpy as np
from ds.dataset.images import TaggedImage
import cv2
from tqdm import tqdm
import ds.dataset.collaters as collaters


class ImagenetDataset(BaseDataset):
    """
    Dataset capaç de carregar imatges en format OID i fer conversions si cal.
    També permet baixar d'internet una classe o varies de OID i crear un dataset local.
    """
    def __init__(self, pathBase, class_names, transforms=None, mode='train', train_val_test_split=[1.0, 0.0, 0.0], set='all', seed=1, index_filename='index.csv'):
        BaseDataset.__init__(self, pathBase, class_names, transforms, mode, train_val_test_split, set, seed, index_filename)

    def __getitem__(self, item):
        row = self.index_fotos.iloc[item]
        image_filename = os.path.join(self.path_base, row['frame_filename'])
        tagged_image = TaggedImage(cv2.imread(image_filename))
        regions = [[int(row['classes']), 0, 0, row['width']-1, row['height']-1]]
        tagged_image.setRegionsX1Y1X2Y2(np.asarray(regions))
        return tagged_image

    def prepara_index(self):
        print('Loading dataset (and downloading it if necessary)...')
        imagenet_data_train, imagenet_data_validation = self.load_or_download()
        cols_dict = {'frame_filename': str,
                     'classes': str,
                     'width': np.int32,
                     'height': np.int32}
        df = pd.DataFrame({}, columns=cols_dict.keys())

        self.all_class_names = imagenet_data_train.class_to_idx
        classes_to_filter = []
        imagenet_to_class = {}
        for index, class_word in enumerate(self.class_names):
            try:
                classes_to_filter.append(self.all_class_names[class_word])
                imagenet_to_class[self.all_class_names[class_word]] = index
            except Exception as e:
                # print(e)
                print(f'Could not find class \'{class_word}\' in this dataset. Skipping it')
        images_to_use = set()
        frame_filename_abs = imagenet_data_train.imgs + imagenet_data_validation.imgs
        classes = np.asarray([element[1] for element in frame_filename_abs])
        # classes = np.asarray(imagenet_data_train.targets + imagenet_data_validation.imgs, dtype=int)
        for class_to_extract in classes_to_filter:
            images_to_extract = np.where(classes == class_to_extract)
            for im_index in images_to_extract[0]:
                images_to_use.add(im_index)
        # Fent la conversió entre índex d'imagenet i el que es requereix per aquest dataset
        for class_to_extract in classes_to_filter:
            classes[classes==class_to_extract] = imagenet_to_class[class_to_extract]

        images_to_use = list(images_to_use)
        print('Calculating relative pathnames...')
        # frame_filename = [os.path.relpath(element[0], self.path_base) for element in frame_filename_abs]
        frame_filename = [element[0][len(self.path_base)+1:] for element in frame_filename_abs]
        frame_filename = np.asarray(frame_filename, dtype=str)
        frame_filename = frame_filename[images_to_use]
        classes = classes[images_to_use] + 1 # Because class '0' is 'background'
        print(f'Total number of images found: {len(images_to_use)}')

        width = []
        height = []
        print('Loading image sizes...')
        for im_rel_path in tqdm(frame_filename):
            h, w = get_image_shape(os.path.join(self.path_base, im_rel_path))[:2]
            height.append(h)
            width.append(w)
        df['frame_filename'] = frame_filename
        df['classes'] = classes
        df['width'] = width
        df['height'] = height
        df = df.astype(cols_dict)
        return df

    def load_or_download(self):
        # URL links: https://onedrive.hyper.ai/home/ImageNet/data/ImageNet2012
        download_links = ['https://onedrive.hyper.ai/show/ImageNet/data/ImageNet2012/ILSVRC2012_bbox_test_dogs.zip',
                          'https://onedrive.hyper.ai/show/ImageNet/data/ImageNet2012/ILSVRC2012_bbox_train_dogs.tar.gz',
                          'https://onedrive.hyper.ai/show/ImageNet/data/ImageNet2012/ILSVRC2012_bbox_train_v2.tar.gz',
                          'https://onedrive.hyper.ai/show/ImageNet/data/ImageNet2012/ILSVRC2012_bbox_val_v3.tgz',
                          'https://onedrive.hyper.ai/show/ImageNet/data/ImageNet2012/ILSVRC2012_devkit_t3.tar.gz',
                          'https://onedrive.hyper.ai/show/ImageNet/data/ImageNet2012/ILSVRC2012_devkit_t12.tar.gz',
                          'https://onedrive.hyper.ai/show/ImageNet/data/ImageNet2012/ILSVRC2012_img_test.tar',
                          'https://onedrive.hyper.ai/show/ImageNet/data/ImageNet2012/ILSVRC2012_img_train_t3.tar',
                          'https://onedrive.hyper.ai/show/ImageNet/data/ImageNet2012/ILSVRC2012_img_val.tar']
        # Torrent links: https://academictorrents.com/collection/imagenet-2012
        torrent_links = {'ILSVRC2012_img_train.tar':'magnet:?xt=urn:btih:a306397ccf9c2ead27155983c254227c0fd938e2&tr=http%3A%2F%2Facademictorrents.com%2Fannounce.php&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969&tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337%2Fannounce&tr=udp%3A%2F%2Ftracker.leechers-paradise.org%3A6969',
                         'ILSVRC2012_img_validation.tar':'magnet:?xt=urn:btih:5d6d0df7ed81efd49ca99ea4737e0ae5e3a5f2e5&tr=http%3A%2F%2Facademictorrents.com%2Fannounce.php&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969&tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337%2Fannounce&tr=udp%3A%2F%2Ftracker.leechers-paradise.org%3A6969'}
        success = False
        while not success:
            try:
                imagenet_data_train = torchvision.datasets.ImageNet(self.path_base, split='train')
                imagenet_data_validation = torchvision.datasets.ImageNet(self.path_base, split='val')
                success = True
                return imagenet_data_train, imagenet_data_validation
            except RuntimeError as e:
                err_txt = str(e)
                words = err_txt.split(' ')
                file_to_download = ''
                for word in words:
                    if word[-6:] == 'tar.gz' or word[-4:] == '.tar':
                        file_to_download = word
                        break
                print(err_txt)
                file_found = False
                destination_folder = os.path.join(self.path_base, file_to_download)
                for link in download_links:
                    if file_to_download in link:
                        print(f'Downloading {file_to_download}...')
                        download_URL(link, destination_folder)
                        file_found = True
                        break
                if not file_found:
                    if file_to_download in torrent_links.keys():
                        download_torrent(torrent_links[file_to_download], self.path_base)
                        file_found = True
                if not file_found:
                    raise Exception(f'Could no download {file_to_download}')

if __name__=='__main__':
    # path = 'tests/test_data/blender_data'
    path = '/mnt/datasets/imagenet/'
    filepath = Path(os.getcwd())
    path = os.path.abspath(filepath / '..' / path)
    print(f'Dir: {path}')
    import albumentations as A
    transform = A.Compose(
        [
         # A.Flip(p=0.3),
         # A.RandomBrightnessContrast(p=0.3),
         # A.ShiftScaleRotate(p=0.5),
         # A.RGBShift(r_shift_limit=30, g_shift_limit=30, b_shift_limit=30, p=0.3),
         # A.FancyPCA(p=1),
         # A.ISONoise(p=1),
         # A.RandomResizedCrop(1080, 1920, p=1),
         # A.RandomSizedBBoxSafeCrop(height=1080, width=1920, p=1),
         # A.MotionBlur(blur_limit=7, p=1),
         A.augmentations.transforms.ToFloat(p=1),
         ],
        bbox_params=A.BboxParams(format='pascal_voc', label_fields=['labels', 'class_names'], min_area=4500, min_visibility=0.2))
    # dset = BlenderDataset(path, ['box1', 'box2'], transforms=transform)
    dset = ImagenetDataset(path, ['person', 'forklift'], transforms=transform)
    # for image in dset:
    #     bboxes = torch.from_numpy(image.getRegionsX1Y1X2Y2())
    #     labels = torch.from_numpy(image.getClasses())
    #     image = image.switchColors().getImage()
    #     display_image_gt(image, bboxes, labels, waitkey=0, size=(1024, 768))
    loader = DataLoader(dset, batch_size=4, drop_last=True, shuffle=True, num_workers=0, collate_fn=collaters.coll_stacks_no_norm)
    n = 0
    base_filename = Path(path) / 'ds_output'
    for images, bboxes, labels in loader:
        display_batch_gt(images, bboxes, labels, waitkey=0)
        print(f'Batch {n}')
        n+=1
    print('FIN')



