"""
Deep Solutions
Created: 28/6/21

Genera un dataset yolo para entrenamiento.
"""
from ds.dataset.blender_dataset import BlenderDataset
import sys
from ds.dataset.via_dataset import ViaDataset

sys.path.append('../../')
print(f'PYTHONPATH: {sys.path}')


if __name__ == '__main__':
    dset = ViaDataset('/mnt/datasets/imagenet', ['persona', 'forklift', 'cuernos', 'carga', 'conductor', 'carga_fl'], via_json_name='forklift_imagenet_con_conductor_carga_fl.json')
    dset.index_fotos = dset.index_fotos.sort_values(by=['frame_filename'])
    dset.index_fotos = dset.index_fotos.drop_duplicates()
    dset.index_fotos = dset.index_fotos.reset_index(drop=True)
    dset.export_2_yolo('/mnt/datasets/yolo_train_carga_fl', inici=None, final=None, train_val=0.85)
    dset_b = BlenderDataset('/mnt/datasets/vehicles/forklift/3D_generated/renders/images', ['person', 'forklift', 'cuernos', 'carga', 'conductor', 'carga_fl'])
    dset_b.export_2_yolo('/mnt/datasets/yolo_train_carga_fl')

    print("FIN")
