"""
Deep Solutions
Created: 4/5/2021

Objetos estándar retornados por los datasets de Deep Solutions
"""

import numpy as np
import cv2
import os


class Image(object):
    """
    Imatge sense etiquetes
    Guarda internament un array numpy en format cv2 sense canviar a RGB
    """
    #TODO: añadir funciones para visualizar imagen

    #TODO: usar el sistema de @property para las propiedades
    """
    @property
    def datamodule(self) -> Any:
        return self._datamodule

    @datamodule.setter
    def datamodule(self, datamodule: Any) -> None:
        self._datamodule = datamodule
    """
    def __init__(self, cv2_img=None):
        self._filename = ''
        self._w = 0
        self._h = 0
        if cv2_img is None:
            self._image = None  # carregada lazy mode quan sigui necessaria
        else:
            self._image = cv2_img
            self._w = cv2_img.shape[1]
            self._h = cv2_img.shape[0]
            assert self._w > 0 and self._h > 0, 'Image did not initialize correctly'
        self._regions = []

    def setFilename(self, filename):
        self._filename = filename

    def getFilename(self):
        return self._filename

    def getImage(self):
        return self._image

    def setImage(self, new_image):
        self._image = new_image

    def loadImage(self) -> None:
        """
        Comprova si la imatge ja esta carregada i sino la carrega a memoria
        Returns:None
        """
        if self._image is not None:
            return
        if os.path.isfile(self._filename):
            self._image = cv2.imread(str(self._filename))
            self._h, self._w = self._image.shape[:2]
            assert self._w > 0 and self._h > 0, 'Image did not initialize correctly'
        else:
            raise FileNotFoundError(f"Image not found, filename: {self._filename}")

    def loadFromFile(self, filename):
        """
        Carrega la imatge a partir del nom de fitxer donat
        Args:
            filename: str
        Returns: None
        """
        if os.path.isfile(filename):
            self._filename = filename
            self.loadImage()
        else:
            raise FileNotFoundError(f"Image not found: {filename}")

    def scale(self, size=None, factor=None):
        """
        Escala tant la imatge com les regions
        Args:
            size: tuple(widht:float, height:float)
            factor: tuple(scale_factor_w:float, scale_factor_h:float)
        Returns: None
        """
        if size is not None:
            # Calculo els factors a partir de les mides donades
            scale_factor_w = size[0] / self._w
            scale_factor_h = size[1] / self._h
            # scale_factor_w = self._w / size[0]
            # scale_factor_h = self._h / size[1]
        elif factor is not None:
            scale_factor_w = factor[0]
            scale_factor_h = factor[1]
        else:
            raise ValueError("Either size or factor must be not None")
        self.scaleImage(scale_factor_w, scale_factor_h)
        self.scaleRegions(scale_factor_w, scale_factor_h)

    def scaleRegions(self, scale_factor_w: float, scale_factor_h: float):
        """
        Escala nomes les regions, no la imatge
        Args:
            scale_factor_w: float [0..1]
            scale_factor_h: float [0..1]
        Returns: None
        """
        for region in self._regions:
            region.scale(scale_factor_w, scale_factor_h)

    def scaleImage(self, scale_factor_w: float, scale_factor_h: float):
        self.loadImage()
        self._w = int(self._w * scale_factor_w)
        self._h = int(self._h * scale_factor_h)
        assert self._w > 0 and self._h > 0, 'Image did not initialize correctly'
        self._image = cv2.resize(self._image, (self._w, self._h))

    def getImageShape(self):
        self.loadImage()
        self._h, self._w = self._image.shape[:2]
        assert self._w > 0 and self._h > 0, 'Image did not initialize correctly'
        return self._h, self._w

    def cv2_imshow(self, title=None, delay=0):
        cv2.imshow(self._filename if title is None else title, self._image)
        cv2.waitKey(delay)

    def switchColors(self):
        """
        Salta entre RGB y BGR indistintamente
        Returns: Image
        """
        self._image = self._image[..., ::-1].copy()  # RGB <--> BGR
        return self

    def save_jpg(self, filename=None):
        if filename is None:
            filename = self._filename
        if filename is None:
            raise ValueError('Cannot save an image without a file name.')
        cv2.imwrite(filename, self._image)



class TaggedImage(Image):
    """
    Especialtzació d'Image que incorpora bboxes en format numpy X1Y1X2Y2 i float[0..1]
    Les clases (una per bbox) les guarda en un array independent
    """
    def __init__(self, cv2_img=None):
        super().__init__(cv2_img=cv2_img)
        self._np_bboxes_xyxy_norm = None
        self._classes = None

    def setRegionsX1Y1X2Y2(self, r:np.array):
        """
        Guarda les regions amb parametre en format xyxy, numpy[N,4], normalizado a 1 [0..1]
        Si l'array que rep es [N,5] llavors extreu les clases i les guarda separades
        Args:
            array: np.array [N,4] o [N,5] on N es el nombre de bboxes

        Returns:

        """
        if r is None:
            self._np_bboxes_xyxy_norm = np.zeros((0, 4))
            return
        r = np.array(r)
        if len(r) == 0:
            self._np_bboxes_xyxy_norm = np.zeros((0, 4))
            return
        if r.shape[1] > 4:
            # Inclou les classes, per tant separo i guardo la columna de les classes
            self.setClasses(r[:, 0])
            r = r[:, 1:] # redueixo dimensions a 4
        if r.max() > 1:
            # Encara no esta normalitzat a 1
            r = r.copy().astype(np.float32) # anem a modificarlo
            r[:, [0,2]] = r[:, [0,2]] / self._w
            r[:, [1,3]] = r[:, [1,3]] / self._h
        self._np_bboxes_xyxy_norm = r

    def setRegionsX1Y1WH(self, r:np.array):
        """
        Args:
            r: LTWH
        """
        r = r.copy().astype(np.float32)  # anem a modificarlo
        if r.shape[1] > 4:
            # Inclou les classes, per tant separo i guardo la columna de les classes
            self.setClasses(r[:, 0])
            r = r[:,1:]
        if r.max() > 1:
            # Encara no esta normalitzat a 1
            r[:, [0,2]] = r[:, [0,2]] / self._w
            r[:, [1,3]] = r[:, [1,3]] / self._h
        # Ara he de transformar a xyxy
        r[:,2] = r[:,0] + r[:,2]
        r[:,3] = r[:,1] + r[:,3]
        self._np_bboxes_xyxy_norm = r

    def setRegionsCX1Y1WH(self, r:np.array):
        if len(r)==0 or r is None:
            return
        self.setRegionsX1Y1WH(r[:,1:])
        self._classes = r[:,0]

    def getRegionsX1Y1X2Y2_Norm(self):
        return self._np_bboxes_xyxy_norm

    def getRegionsX1Y1X2Y2(self):
        r = self._np_bboxes_xyxy_norm.copy()
        r[:,[0,2]] *= self._w
        r[:,[1,3]] *= self._h
        return np.rint(r)

    def getRegionscXcYWH_Norm(self):
        if self._np_bboxes_xyxy_norm is None or len(self._np_bboxes_xyxy_norm)==0:
            return None
        r = self._np_bboxes_xyxy_norm.copy()
        cx = (r[:,2] + r[:,0]) / 2.0
        cy = (r[:,3] + r[:,1]) / 2.0
        w = r[:,2] - r[:,0]
        h = r[:,3] - r[:,1]
        r[:, 0] = cx
        r[:, 1] = cy
        r[:, 2] = w
        r[:, 3] = h
        return r

    def getRegionsCX1Y1X2Y2_Norm(self):
        a = np.zeros((self._np_bboxes_xyxy_norm.shape[0], self._np_bboxes_xyxy_norm.shape[1] + 1), dtype=np.float32)
        a[:,0] = self._classes
        a[:,1:] = self._np_bboxes_xyxy_norm
        return a

    def getRegionsCX1Y1X2Y2(self):
        r = self.getRegionsCX1Y1X2Y2_Norm()
        r[:,[1,3]] *= self._w
        r[:,[2,4]] *= self._h
        return r

    def has_regions(self):
        return self._np_bboxes_xyxy_norm is not None

    def getRegionsCX1Y1WH_Norm(self):
        r = self._np_bboxes_xyxy_norm.copy()
        a = np.zeros((self._np_bboxes_xyxy_norm.shape[0], self._np_bboxes_xyxy_norm.shape[1] + 1), dtype=np.float32)
        a[:,0] = self._classes
        a[:,1] = r[:,0]
        a[:,2] = r[:,1]
        a[:,3] = r[:,2] - r[:,0]
        a[:,4] = r[:,3] - r[:,1]
        return a

    def getRegionsCX1Y1WH(self):
        r = self.getRegionsCX1Y1WH_Norm()
        r[:,[1,3]] *= self._w
        r[:,[2,4]] *= self._h
        return r

    def getRegionsList(self):
        regions = []
        if self.has_regions():
            try:
                for cbbox, class_name in zip(self.getRegionsCX1Y1X2Y2(), self._class_names):
                    reg = Region().init_from_CX1Y1X2Y2(cbbox)
                    reg.class_name = class_name
                    regions.append(reg)
            except:
                for cbbox in self.getRegionsCX1Y1X2Y2():
                    reg = Region().init_from_CX1Y1X2Y2(cbbox)
                    regions.append(reg)
        return regions


    def setClasses(self, classes):
        """
        Guarda l'array de classes en format np.array
        Args:
            classes: Pot ser qualsevol enumerable valid per Numpy (llista, tupla o np.array)
        Returns: None
        """
        if classes is None:
            self._classes = []
        else:
            self._classes = np.array(classes)

    def getClasses(self):
        if self._classes is None:
            return []
        return self._classes

    def getClassNames(self):
        return self._class_names

    def setClassNames(self, class_names):
        self._class_names = class_names


class YoloImage(TaggedImage):
    """
    Especialitzada en formats Yolo
    Internament guarda les regions directament com a arrays np de Nxn_labels per accelerar els procesos d'entrenament
    Si es demanan Regions, fa la traducció i crea els objectes.
    """
    def __init__(self, cv2_img=None):
        super().__init__(cv2_img=cv2_img)
        self.np_regions = None  #guardarà regions en format np.array(N,5) on cls,xyxy


    def loadLabels(self):
        """
        Yolo espera que els labels estiguin a una carpeta labels a nivell de images i en format cx1y1wh
        Returns: None
        """
        if self._filename is None:
            raise ValueError("Image not loaded or without filename assigned. Labels cannot be loaded.")
        label_path = self.img2label_paths(self._filename)
        l = None
        if os.path.isfile(label_path):
            with open(label_path, 'r') as f:
                l = [x.split() for x in f.read().strip().splitlines()]
                l = np.array(l, dtype=np.float32)
                if len(l):
                    assert l.shape[1] == 5, 'labels require 5 columns each'
                    assert (l >= 0).all(), 'negative labels'
                    assert (l[:, 1:] <= 1).all(), 'non-normalized or out of bounds coordinate labels'
                    assert np.unique(l, axis=0).shape[0] == l.shape[0], 'duplicate labels'
        else:
            raise FileNotFoundError(f'Labels file not found: {label_path}')
        # self.np_regions = l
        self.setRegionsX1Y1WH(l)

    def img2label_paths(self, x):
        """
        Yolo espera que els labels estiguin a una carpeta labels a nivell de images,
        aquesta funcio canvia al path 'images' per 'labels'
        Args:
            x: str (el path de la imatge)
        Returns: str
        """
        sa, sb = os.sep + 'images' + os.sep, os.sep + 'labels' + os.sep  # /images/, /labels/ substrings
        path = 'txt'.join(x.replace(sa, sb, 1).rsplit(x.split('.')[-1], 1))
        return path


class VIAImage(Image):
    """
    Objecte imatge estandard retornat pels datasets
    Una imatge conté a mes un llistat de Regions
    """

    def __init__(self):
        super().__init__()
        self.__attributes = {}
        self.__base_dir = ''
        self.__regions_raw = {}
        self.__file_size = 0

    def initFromVIA(self, via_dic):
        """
        Carrega tots els camps de la imatge a partir d'un diccionari en format VIA.
        No del projecte sencer, sino el diccionari d'una imatge nomes
        Args:
            via_dic: #TODO: especificar la estructura del diccionari

        Returns: None
        """
        self.__base_dir = via_dic['base_dir']  #TODO: mirar aquest base_dir on es fa servir. El diccionari de via no ho te com a camp individual d'imatge.
        self._filename = via_dic['filename']
        self.__file_size = via_dic['size']
        self.__regions_raw = via_dic['regions']
        self.__attributes = via_dic['file_attributes']
        self._h, self._w = self.getImageShape()
        self._regions = self.getRegionsList()

    def getFileSize(self):
        return self.__file_size

    def __str__(self):
        """
        Tradueix l'objecte a string per poder ser impres.
        Returns: String
        """
        s = f'Base dir: {self.__base_dir}\n'
        s += f'Filename: {self._filename}\n'
        s += f'File size: {self.__file_size}\n'
        s += f'Regions: {self.__regions_raw}\n'
        s += f'Attributes: {self.__attributes}\n'
        return s

    def print(self):
        """
        Imprimeix a sortida estandar l'objecte
        Returns: None
        """
        print(self)

    def getBaseDir(self):
        return self.__base_dir

    def getRegionsRaw(self):
        return self.__regions_raw

    def getAttributes(self):
        return self.__attributes

    def getRegionsList(self):
        """
        Retorna les regions en format llista d'objectes Region
        Returns: [Region]
        """
        regions_list = []
        for region in self.getRegionsRaw():
            regions_list.append(VIARegion(region))
        return regions_list

class VisDrone2019Image(TaggedImage):
    def __init__(self, cv2_img=None):
        super().__init__(cv2_img)

    def load_labels(self, filename):
        """
        Carrega classes i bboxes a partir del fitxer txt de annotations
        Args:
            filename: str path del fitxer txt
        """
        bboxes_xyxy = []
        classes = []
        with open(filename, 'r') as file:
            for row in [x.split(',') for x in file.read().strip().splitlines()]:
                if row[4] == '0':  # VisDrone 'ignored regions' class 0                       
                    continue
                cls = int(row[5])
                box = tuple(map(int, row[:4])) #xcycwh en pixels
                x0 = int(box[0])
                y0 = int(box[1])
                x1 = x0 + box[2]
                y1 = y0 + box[3]
                bbox_xyxy = x0,y0,x1,y1
                bboxes_xyxy.append(bbox_xyxy)
                classes.append(cls)
        self.setRegionsX1Y1X2Y2(np.array(bboxes_xyxy))
        self.setClasses(classes)


class BlenderImage(TaggedImage):

    def __init__(self, cv2_img=None):
        """
        Defineixo la estructura per guardar les regions en format numpy
        Aquesta classe no fa servir Regions #TODO: veure com integrar Regions i poder generalitzar elements
        """
        super().__init__(cv2_img=cv2_img)

    def loadLabels(self, filename):
        """
        Carrega les classes i etiquetes a partir del fitxer npz generat per Blender
        Args:
            filename: str path del fitxer npz
        Returns:
        """
        data = np.load(filename)
        sg_masks = data['segmentation_masks']
        bboxes = []
        areas = []
        # Convertir las masks a bboxes
        for i in range(1, sg_masks.max() + 1):  # 0 es el fondo
            car_mask = np.zeros_like(sg_masks, np.uint8)
            car_mask[sg_masks == i] = 1
            bbox = cv2.boundingRect(car_mask)  # yxhw
            # y1, x1, h, w = bbox
            x1, y1, w, h = bbox
            if w==0 or h==0:
                print(f"WARNING: w={w} h={h}")
            y2 = y1 + h  # OJO que supongo que xy son extremos y no centro
            x2 = x1 + w  # OJO que supongo que xy son extremos y no centro
            bbox = (x1, y1, x2, y2)
            if not np.equal(bbox, np.zeros(4)).all():
                bboxes.append(bbox)
                areas.append(h * w)
        self.setRegionsX1Y1X2Y2(bboxes)
        self.setClasses(np.ones(len(bboxes), dtype=np.int8)) # por ahora, hasta que no corrigamos vision_blender, cada instancia tiene un id de clase consecutivo, nosotros los igualamos todos a 1 (dataset mono-clase)
        self.areas = np.asarray(areas, dtype=np.float32)

    def load_labels_v2(self, filename):
        """
        Carrega les classes i etiquetes a partir del fitxer npz generat per Blender.
        Utilitza la segona versio del vision_blender(DS) on les bboxes ja s'exporten directament i no s'ha de treballar
        amb les masks
        Format de la BBOX al fitxer blender: xyxy on el (0,0) es (LEFT, TOP) i entre 0.0 i 1.0
        Si alguna clase es 0 pasa de esa instancia. Puede ser fondo o un error en blender.
        Args:
            filename: str path del fitxer npz
        Returns:
        """
        data = np.load(filename)
        try:
            bboxes_xyxy = data['bboxes_xyxy']
            if (bboxes_xyxy[:,0]==bboxes_xyxy[:,2]).any():
                print(f"WARNING: x1==x2 in filename {filename}")
        except:
            raise ValueError(f'Fichero: {filename} no contiene la key bboxes_xyxy')
        classes = data['classes']
        # FILTRO CLASSES <=0, suelen ser errores en el dibujado en blender
        idx = np.where(classes > 0) # en los datasets de DS 0 es el fondo
        if len(idx[0])!=len(classes):
            # Hemos descartado alguna bbox
            print(f"WARNING: bbox descartada por clase erronea en {filename}")
        bboxes_xyxy = bboxes_xyxy[idx]
        classes = classes[idx]
        # FILTRO DE BBOXES NEGATIVAS OR W==0 OR H==0
        idx = np.where(bboxes_xyxy[:,0]!=bboxes_xyxy[:,2])
        bboxes_xyxy = bboxes_xyxy[idx]
        classes = classes[idx]
        idx = np.where(bboxes_xyxy[:,1]!=bboxes_xyxy[:,3])
        bboxes_xyxy = bboxes_xyxy[idx]
        classes = classes[idx]
        # FILTRO DE BBOXES REPETIDAS
        _,idx = np.unique(bboxes_xyxy, axis=0, return_index=True)
        if len(idx)!=len(classes):
            # Hemos descartado alguna bbox
            print(f"WARNING: bbox descartada por repetición en {filename}")
        bboxes_xyxy = bboxes_xyxy[idx]
        classes = classes[idx]
        # print(bboxes_xyxy)
        self.setRegionsX1Y1X2Y2(bboxes_xyxy)
        self.setClasses(classes)
        areas = (bboxes_xyxy[:,2]-bboxes_xyxy[:,0]) * (bboxes_xyxy[:,3]-bboxes_xyxy[:,1])
        self.areas = areas

    def getAreas(self): #TODO: generalizar
        return self.areas


class Region(object):
    """
    Objecte Regio d'una imatge.
    Correspon a una detecció o una etiqueta.
    Pot ser un poligon, un quadrat (bbox) o una màscara (segmentation).
    """

    def __init__(self):
        self.__attributes = {}
        # Float from 0 (non-truncated) to 1 (truncated), where truncated refers to the object leaving image boundaries
        self.truncated = 0.0
        # Integer (0,1,2,3) indicating occlusion state:
        #  0 = fully visible
        #  1 = partly occluded
        #  2 = largely occluded
        #  3 = unknown
        self.occluded = 3
        # Observation angle of object, ranging [-pi..pi]
        self.alpha = 0.0
        # 2D bounding box of object in the image (0-based index): left, top, right, bottom pixel coordinates
        self.bbox = [0.0, 0.0, 0.0, 0.0]
        # 3D object dimensions: height, width, length (in meters)
        self.dimensions = [0.0, 0.0, 0.0]
        # 3D object location x,y,z in camera coordinates (in meters)
        self.location = [0.0, 0.0, 0.0]
        # Rotation ry around Y-axis in camera coordinates [-pi..pi]
        self.rotation_y = 0.0
        # Only for results: Float, indicating confidence in detection, needed for p/r curves, higher is better.
        self.score = 0.0
        self.classe = 0
        self.class_name=None

    def init_from_CX1Y1X2Y2(self, cbbox):
        self.classe = cbbox[0]
        self.bbox = cbbox[1:]
        return self
    
    def set_class_name(self, name):
        self.class_name = name

    def getRegionAttributes(self):
        return self.__attributes

    def scale(self, scale_factor_w, scale_factor_h):
        raise NotImplementedError

    def asKitty(self):
        """
        #Values    Name      Description
        ----------------------------------------------------------------------------
            1    type(str)         Describes the type of object: 'Car', 'Van', 'Truck',
                                'Pedestrian', 'Person_sitting', 'Cyclist', 'Tram',
                                'Misc' or 'DontCare'
            1    truncated(float)    Float from 0 (non-truncated) to 1 (truncated), where
                                truncated refers to the object leaving image boundaries
            1    occluded(int)     Integer (0,1,2,3) indicating occlusion state:
                                0 = fully visible, 1 = partly occluded
                                2 = largely occluded, 3 = unknown
            1    alpha(float)        Observation angle of object, ranging [-pi..pi]
            4    bbox         2D bounding box of object in the image (0-based index):
                                contains left, top, right, bottom pixel coordinates
            3    dimensions   3D object dimensions: height, width, length (in meters)
            3    location     3D object location x,y,z in camera coordinates (in meters)
            1    rotation_y   Rotation ry around Y-axis in camera coordinates [-pi..pi]
            1    score        Only for results: Float, indicating confidence in
                                detection, needed for p/r curves, higher is better.
        """
        kitty_region = ''
        if self.class_name is not None:
            kitty_region += f'{self.class_name}'
        else:
            kitty_region += f'{self.classe}'
        kitty_region += ' ' + str(self.truncated)
        kitty_region += ' ' + str(self.occluded)
        kitty_region += ' ' + str(self.alpha)
        kitty_region += ' ' + ' '.join(str(num) for num in self.bbox)
        kitty_region += ' ' + ' '.join(str(num) for num in self.dimensions)
        kitty_region += ' ' + ' '.join(str(num) for num in self.location)
        kitty_region += ' ' + str(self.rotation_y) + '\n'
        # kitty_region += ' ' + str(self.score) + '\n'
        return kitty_region

class VIARegion(Region):
    def __init__(self, region_dict):
        super().__init__()
        # Info is stored in VIA and Kitty format at the same time
        self.shape_attributes = region_dict['shape_attributes']
        self.region_attributes = region_dict['region_attributes']
        # Float from 0 (non-truncated) to 1 (truncated), where truncated refers to the object leaving image boundaries
        self.truncated = 0.0
        # Integer (0,1,2,3) indicating occlusion state:
        #  0 = fully visible
        #  1 = partly occluded
        #  2 = largely occluded
        #  3 = unknown
        self.occluded = 3
        # Observation angle of object, ranging [-pi..pi]
        self.alpha = 0.0
        # 2D bounding box of object in the image (0-based index): left, top, right, bottom pixel coordinates
        self.bbox = [0.0, 0.0, 0.0, 0.0]
        # 3D object dimensions: height, width, length (in meters)
        self.dimensions = [0.0, 0.0, 0.0]
        # 3D object location x,y,z in camera coordinates (in meters)
        self.location = [0.0, 0.0, 0.0]
        # Rotation ry around Y-axis in camera coordinates [-pi..pi]
        self.rotation_y = 0.0
        # Only for results: Float, indicating confidence in detection, needed for p/r curves, higher is better.
        self.score = 0.0
        if self.isRect():
            x = float(self.shape_attributes['x'])
            y = float(self.shape_attributes['y'])
            width = float(self.shape_attributes['width'])
            height = float(self.shape_attributes['height'])
            self.bbox = [x, y, x + width, y + height]

    def __str__(self):
        """
        Tradueix l'objecte a string per poder ser impres.
        Returns: String
        """
        s = f'Shape attributes: {self.shape_attributes}\n'
        s += f'Region attributes: {self.region_attributes}\n'
        return s

    def print(self):
        """
        Imprimeix a sortida estandar l'objecte
        Returns: None
        """
        print(self)

    def isRect(self):
        return self.shape_attributes['name'] == 'rect'

    def isPolygon(self):
        return self.shape_attributes['name'] == 'polygon'

    def isMask(self):
        return self.region_attributes['class'] == 'mask'

    def getClass(self):
        return self.region_attributes['class']

    def getBBoxP1P2(self):
        # Return format is ((x1,y1),(x2,y2))
        p1 = tuple(np.asarray(self.bbox[:2], dtype='int32'))
        p2 = tuple(np.asarray(self.bbox[2:], dtype='int32'))
        return p1, p2

    def getPolyPoints(self):
        all_points_x = np.asarray(self.shape_attributes['all_points_x'], dtype='int32')
        all_points_y = np.asarray(self.shape_attributes['all_points_y'], dtype='int32')
        points = np.vstack((all_points_x, all_points_y)).transpose()
        return points

    def asKitty(self):
        kitty_region = ''
        kitty_region += self.region_attributes['class']
        kitty_region += ' ' + str(self.truncated)
        kitty_region += ' ' + str(self.occluded)
        kitty_region += ' ' + str(self.alpha)
        kitty_region += ' ' + ' '.join(str(num) for num in self.bbox)
        kitty_region += ' ' + ' '.join(str(num) for num in self.dimensions)
        kitty_region += ' ' + ' '.join(str(num) for num in self.location)
        kitty_region += ' ' + str(self.rotation_y)
        kitty_region += ' ' + str(self.score) + '\n'
        return kitty_region

    def scale(self, scale_factor_w, scale_factor_h):
        if self.shape_attributes['name'] == 'rect':
            self.shape_attributes['x'] *= scale_factor_w
            self.shape_attributes['y'] *= scale_factor_h
            self.shape_attributes['width'] *= scale_factor_w
            self.shape_attributes['height'] *= scale_factor_h
        elif self.shape_attributes['name'] == 'polygon':
            all_points_x = np.asarray(self.shape_attributes['all_points_x'])
            all_points_y = np.asarray(self.shape_attributes['all_points_y'])
            all_points_x = np.multiply(all_points_x, scale_factor_w)
            all_points_y = np.multiply(all_points_y, scale_factor_h)
            self.shape_attributes['all_points_x'] = list(all_points_x)
            self.shape_attributes['all_points_y'] = list(all_points_y)
        else:
            print('Error. Region type unexpected')
            exit(-1)
