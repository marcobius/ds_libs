"""
Deep Solutions
Created: 03/06/2021

Funcions auxiliars per la exportació de datasets
"""
import os
from pathlib import Path
import numpy as np
import pandas as pd


def crea_yolo_cfg_file(pathbase, class_names, train_val=0.8):
    # Inicializa
    print("Generando fichero de configuracion")
    p = Path(pathbase)
    path_images = p / 'images'
    img_files = []
    # Busco las imagenes (los labels los busca el recursivamente desde las labels)
    lista_total = list(path_images.glob('*.jpg'))
    lista_total += (list(path_images.glob('*.JPG')))
    lista_total += (list(path_images.glob('*.jpeg')))
    lista_total += (list(path_images.glob('*.JPEG')))
    lista_total += (list(path_images.glob('*.png')))
    lista_total += (list(path_images.glob('*.PNG')))
    lista_total.sort()
    # Crea Dataframe con todos los nombres
    df = pd.DataFrame([str(p) for p in lista_total], columns=['filename'])
    # Marca columna nueva con train/val
    df['train_val']=np.random.choice(['train', 'val'], size=len(df), p=[train_val, 1-train_val])
    # Separa listas
    train_list = df[df['train_val'] == 'train'].filename
    train_list = [path+'\n' for path in train_list]
    val_list = df[df['train_val'] == 'val'].filename.to_list()
    val_list = [path+'\n' for path in val_list]
    # Exporta ficheros de lista de imagenes
    cfg_filename = os.path.abspath(p/'cfg_file.yaml')
    train_filename = os.path.abspath(p/'train_list.txt')
    val_filename = os.path.abspath(p/'val_list.txt')
    with open(train_filename, 'w') as f:
        f.writelines(train_list)
    with open(val_filename, 'w') as f:
        f.writelines(val_list)
    # Exporta fichero de configuracion
    lines = []
    lines.append("# File Generated with ds_libs.ds.dataset.base_dataset_exports.py \n")
    lines.append(f"# {len(img_files)} images with train_val split of {train_val}. \n")
    lines.append(f"# Resulting in {len(train_list)} train images and {len(val_list)} validation images  \n")
    lines.append("# train and val data as 1) directory: path/images/, 2) file: path/images.txt, or 3) list: [path1/images/, path2/images/]\n")
    lines.append(f"train: {train_filename} \n")
    lines.append(f"val: {val_filename} \n")
    lines.append("\n")
    lines.append("# number of classes \n")
    lines.append(f"nc: {len(class_names)}\n")
    lines.append("\n")
    lines.append(" # class names\n")
    lines.append(f"names: {class_names}\n")
    with open(cfg_filename, 'w') as f:
        f.writelines(lines)
    print(f'Fichero guardado como: {(cfg_filename)}')

def crea_yolo_cfg_file_v0(pathbase, class_names, train_val=0.8):
    print("Generando fichero de configuracion")
    p = Path(pathbase)
    path_images = p / 'images'
    img_files = []
    # Busco las imagenes (los labels los busca el recursivamente desde las labels)
    lista_total = list(path_images.glob('*.jpg'))
    lista_total += (list(path_images.glob('*.JPG')))
    lista_total += (list(path_images.glob('*.jpeg')))
    lista_total += (list(path_images.glob('*.JPEG')))
    lista_total += (list(path_images.glob('*.png')))
    lista_total += (list(path_images.glob('*.PNG')))
    lista_total.sort()
    for f in lista_total:
        img_files.append(str(f.absolute())+"\n")
    # Separo train - val
    punter = int(len(img_files)*train_val)
    train_list = img_files[:punter]
    val_list = img_files[punter:]
    cfg_filename = os.path.abspath(p/'cfg_file.yaml')
    train_filename = os.path.abspath(p/'train_list.txt')
    val_filename = os.path.abspath(p/'val_list.txt')
    lines = []
    lines.append("# File Generated with ds_libs.ds.dataset.base_dataset_exports.py \n")
    lines.append(f"# {len(img_files)} images with train_val split of {train_val}. \n")
    lines.append(f"# Resulting in {len(train_list)} train images and {len(val_list)} validation images  \n")
    lines.append("# train and val data as 1) directory: path/images/, 2) file: path/images.txt, or 3) list: [path1/images/, path2/images/]\n")
    lines.append(f"train: {train_filename} \n")
    lines.append(f"val: {val_filename} \n")
    lines.append("\n")
    lines.append("# number of classes \n")
    lines.append(f"nc: {len(class_names)}\n")
    lines.append("\n")
    lines.append(" # class names\n")
    lines.append(f"names: {class_names}\n")
    with open(train_filename, 'w') as f:
        f.writelines(train_list)
    with open(val_filename, 'w') as f:
        f.writelines(val_list)
    with open(cfg_filename, 'w') as f:
        f.writelines(lines)
    print(f'Fichero guardado como: {(cfg_filename)}')

if __name__ == '__main__':
    '/mnt/datasets/yolo_trainer'
    crea_yolo_cfg_file('/mnt/datasets/yolo_trainer', ['persona', 'forklift', 'cuernos', 'carga'], train_val=0.8)
