'''
This little app is used to remove duplicates of images downloaded from internet.
It first changes their names so that they are ordered by CRC, so that we can visually detect duplicates.
Then it looks into all the folders that conform the dataset, and deletes duplicates.
The hash used is based on appearence, not so much in bytes, so the same image in jpg and gif format should result in same hash
'''

# Source https://towardsdatascience.com/removing-duplicate-or-similar-images-in-python-93d447c1c3eb
# Inspired from https://github.com/JohannesBuchner/imagehash repository
import imagehash
from PIL import Image, UnidentifiedImageError
import numpy as np
import glob
import os
import random
import string

DATASET_FOLDER = '/mnt/datasets/vehicles/forklift/internet_images/'

def alpharemover(image):
    if image.mode != 'RGBA':
        return image
    canvas = Image.new('RGBA', image.size, (255,255,255,255))
    canvas.paste(image, mask=image)
    return canvas.convert('RGB')

def with_ztransform_preprocess(hashfunc, hash_size=8):
    def function(path):
        image = alpharemover(Image.open(path))
        image = image.convert("L").resize((hash_size, hash_size), Image.ANTIALIAS)
        data = image.getdata()
        quantiles = np.arange(100)
        quantiles_values = np.percentile(data, quantiles)
        zdata = (np.interp(data, quantiles_values, quantiles) / 100 * 255).astype(np.uint8)
        image.putdata(zdata)
        return hashfunc(image)
    return function

# Renaming with hash
dhash_z_transformed = with_ztransform_preprocess(imagehash.dhash, hash_size = 8)
img_paths = glob.glob(DATASET_FOLDER+'**/*', recursive=True)
for img_path_name in img_paths:
    if os.path.isfile(img_path_name):
        folder, filename = os.path.split(img_path_name)
        filename, ext = os.path.splitext(filename)
        try:
            visual_hash = dhash_z_transformed(img_path_name)
            random_hash = ''.join(random.choices(string.ascii_letters + string.digits, k=8))
            new_path_name = os.path.join(folder, f'{visual_hash}_{random_hash}.{ext}')
            os.rename(img_path_name, new_path_name)
        except UnidentifiedImageError as e:
            print(e)
            os.remove(img_path_name)


# Removing duplicates
img_paths = glob.glob(DATASET_FOLDER+'**/*', recursive=True)
last_visual_hash = ''
for img_path_name in img_paths:
    if os.path.isfile(img_path_name):
        folder, filename = os.path.split(img_path_name)
        filename, ext = os.path.splitext(filename)
        visual_hash, random_hash = filename.split('_')
        if visual_hash != last_visual_hash:
            last_visual_hash = visual_hash
            img_paths_hash = glob.glob(DATASET_FOLDER+'**/'+visual_hash+'*')
            if len(img_paths_hash) > 1:
                for i in range(1,len(img_paths_hash)):
                    os.remove(img_paths_hash[i])



