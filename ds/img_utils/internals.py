import random
import colorsys
import numpy as np
import cv2


def color_distance(color1, color2):
    return np.linalg.norm(np.asarray(color1)-np.asarray(color2))

def get_random_color():
    h, s, l = random.random(), 0.5 + random.random() / 2.0, 0.4 + random.random() / 5.0
    # r, g, b = [int(256 * i) for i in colorsys.hls_to_rgb(h, l, s)]
    r, g, b = colorsys.hls_to_rgb(h, l, s)
    return b,g,r

generated_colors = [(0,0,0)]

def get_different_color(attempts=50):
    global generated_colors
    print(generated_colors)
    color_selected = None
    max_min_distance = 0
    for i in range(attempts):
        new_color = get_random_color()
        min_distance = 10000
        for gen_color in generated_colors:
            dist = color_distance(gen_color, new_color)
            min_distance = min(min_distance, dist)
        if min_distance > max_min_distance:
            color_selected = new_color
            max_min_distance = min_distance
    generated_colors.append(color_selected)
    # print(color_selected)
    return color_selected

def add_manual_color(color):
    global generated_colors
    generated_colors.append(color)


def draw_text(img, point, text, font= cv2.FONT_HERSHEY_SIMPLEX, color=(1, 1, 1), scale=1, thickness=1, reference='center'):
    textsize = cv2.getTextSize(text, font, fontScale=scale, thickness=thickness)[0]
    if reference == 'center':
        point = np.asarray([point[0] - textsize[0] / 2, point[1] + textsize[1] / 2]).astype(np.int32)
    elif reference == 'bottom_right':
        point = np.asarray([point[0] - textsize[0], point[1]]).astype(np.int32)
    elif reference == 'top_left':
        point = np.asarray([point[0], point[1] + textsize[1]]).astype(np.int32)
    img = cv2.putText(img, text=text, org=point, fontFace=font, fontScale=scale, color=color, thickness=thickness)
    return img