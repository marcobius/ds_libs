"""
Deep Solutions
Created: 05/05/2021
"""

import unittest
from ds.dataset.images import Image, Region, YoloImage, TaggedImage
import cv2
import numpy as np

class TestImage(unittest.TestCase):
    def test_load_image(self):
        im_test = cv2.imread('test_data/images/no_people_0345.jpg')
        img = Image()
        self.assertIsNone(img._image)
        img.loadFromFile('test_data/images/no_people_0345.jpg')
        self.assertIsNotNone(img._image)
        self.assertTupleEqual(tuple(im_test.shape[:2]), (img._h, img._w))

class TestTaggedImage(unittest.TestCase):
    def test_load_tagged_image(self):
        im_test = cv2.imread('test_data/images/no_people_0345.jpg')
        img = TaggedImage(im_test)
        bboxes_xyxy = np.array([[100, 100, 400, 400], [250, 250, 750, 750]])
        bboxes_cxyxy = np.array([[1, 100, 100, 400, 400], [2, 250, 250, 750, 750]])
        classes = [1,2]
        img.setRegionsX1Y1X2Y2(bboxes_xyxy)
        img.setClasses(classes)
        b = np.all(img.getRegionsX1Y1X2Y2() == np.array(bboxes_xyxy))
        self.assertTrue(b)
        img.setRegionsX1Y1X2Y2(bboxes_cxyxy)
        b = np.all(img.getClasses() == np.array(classes))
        self.assertTrue(b)
        b = np.all(img.getRegionsX1Y1X2Y2() == np.array(bboxes_xyxy))
        self.assertTrue(b)

class TestYoloImage(unittest.TestCase):
    def test_load_labels(self):
        img = YoloImage()
        img.loadFromFile('test_data/images/no_people_0345.jpg')
        img.loadLabels() # yolo espera que les imatges estiguin a 'images' y les etiquetes a 'labels'
        a = np.array([0, 0.8130208253860474, 0.7537037134170532, 0.18541669845581055, 0.32592594623565674])
        b = img.getRegionsCX1Y1WH_Norm()[0]
        self.assertTrue(np.array_equal(a,b))
        b = img.getRegionsCX1Y1X2Y2_Norm()[0]
        a2 = np.array([0, 0.8130208253860474, 0.7537037134170532, 0.8130208253860474+0.18541669845581055, 0.7537037134170532+0.32592594623565674])
        self.assertTrue(np.array_equal(a2, b))





