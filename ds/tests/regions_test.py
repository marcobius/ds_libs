"""
Deep Solutions
Created: 30/6/21
"""
import unittest
from ds.dataset.regions import Bbox

class TestRegion(unittest.TestCase):
    def test_serialize(self):
        ltwh=[0.1, 0.1, 0.5, 0.5]
        r1 = Bbox(ltwh, 1.0, 1, 1)
        dic = r1.serialize()
        r2 = Bbox.de_serialize(dic)
        self.assertTrue((r1.ltwh==r2.ltwh).all())