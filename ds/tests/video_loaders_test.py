"""
Deep Solutions
Created: 15/05/2021
"""

import unittest
from ds.dataset.images import Image, Region, YoloImage, TaggedImage
import ds.dataset.collaters as collaters
import cv2
import numpy as np

from ds.dataset.video_loaders import SingleVideoDataset


class TestSingleVideoLoader(unittest.TestCase):
    def test_general(self):
        reader = SingleVideoDataset('../tests/test_data/videos/formula1_limits.mp4', resize_factor=0.5)
        i = iter(reader)
        image = next(i)
        self.assertTupleEqual(image.getImage().shape, (360, 640, 3))
        for image in reader:
            cv2.imshow('Video', image.getImage())
            key = cv2.waitKey(1)
            if key == ord('q'):
                break
        reader.stop()
        cv2.destroyWindow('Video')

    def test_batch_collater(self):
        reader = SingleVideoDataset('../tests/test_data/videos/formula1_limits.mp4', resize_factor=0.5)
        iterador = iter(reader)
        lista = []
        for i in range(4):
            lista.append(next(iterador))
        X = collaters.coll_rcnn_test(lista)
        self.assertEqual(X['images'].shape[0],4)
