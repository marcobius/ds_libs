"""
Deep Solutions
Created: 06/05/2021
"""

import unittest
import cv2
from ds.dataset.blender_dataset import BlenderDataset, display_image_gt, display_batch_gt
from torch.utils.data import DataLoader
import ds.dataset.collaters as collaters

class TestBlenderDataset(unittest.TestCase):
    def test_all(self):
        path='test_data/blender_data'
        dset = BlenderDataset(path, ['forklift'])
        self.assertEqual(len(dset), 4)
        image = dset[0]
        self.assertEqual(len(image.getClassNames()), 5)
        display_image_gt(image.getImage(), image.getRegionsX1Y1X2Y2(), image.getClassNames())
        loader = DataLoader(dset, batch_size=4, drop_last=True, shuffle=False, num_workers=0, collate_fn=collaters.coll_np_torch_X1Y1X2Y2)
        i = iter(loader)
        batch = next(i)
        self.assertEqual(len(batch['class_names']),4)
        display_batch_gt(batch['img'], batch['bbox'], batch['class_names'], )
        infer_loader = DataLoader(dset, batch_size=4, drop_last=True, shuffle=False, num_workers=0, collate_fn=collaters.coll_np2torch_infer)
        i = iter(infer_loader)
        batch_img = next(i)
        self.assertTupleEqual((4,3,1080, 1920,), batch_img.shape)
        cv2.destroyAllWindows()

    def test_albumentations(self):
        import albumentations as A
        path='test_data/blender_data'
        transform = A.Compose([A.Flip(p=1.0), A.RandomBrightnessContrast(p=1.0), A.augmentations.transforms.Normalize(p=1.0)], bbox_params=A.BboxParams(format='pascal_voc', label_fields=['labels']))
        dset = BlenderDataset(path, ['forklift'], transform=transform)
        image = dset[0]
        display_image_gt(image.getImage(), image.getRegionsX1Y1X2Y2(), image.getClassNames())
        image_dict = {'image': image.getImage(), 'bboxes': image.getRegionsX1Y1X2Y2(), 'labels': image.getClasses(),
                      'class_names': image.getClassNames()}
        print(f"Bboxes antes: {image_dict['bboxes']}")
        image_dict = transform(**image_dict)
        image.setImage(image_dict['image'])
        image.setRegionsX1Y1X2Y2(image_dict['bboxes'])
        print(f"Bboxes despues: {image_dict['bboxes']}")
        image.setClasses(image_dict['labels'])
        image.setClassNames(image_dict['class_names'])
        # display_image_gt(image.getImage(), image.getRegionsX1Y1X2Y2(), image.getClassNames())




