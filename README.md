# ds_libs

Repositori de llibreries de Deep Solutions.  
Cada carpeta principal del projecte correspon a un paquet instal·lat pel setup.py (o pip install .).  


# Instalació
Amb l'environment de destí de la instalció actiu, fer:   
```
pip install --upgrade pip # la versió 9 de pip no instala be les dependencies de l'opencv
pip install <directori de ds_libs>  
```
O be es pot instal·lar directament de git amb: 
```shell
pip install git+https://gitlab.com/marcobius/ds_libs.git#egg=ds_lib
```

Es poden excloure totes les dependencies amb `--no-dependencies`

# Scripts de la llibrería

## video2via
```shell
usage: video2via [-h] [-i INPUT_FILE] [-o OUTPUT_DIR] [-c CLASSES [CLASSES ...]] [-t THRESHOLD] [-s]

Saves frames of a video as jpg files and creates a via dataset (if there is a previous via dataset there IT WILL BE DELETED). You can select the
proportion of frames to be saved.

optional arguments:
  -h, --help            show this help message and exit
  -i INPUT_FILE, --input_file INPUT_FILE
  -o OUTPUT_DIR, --output_dir OUTPUT_DIR
  -c CLASSES [CLASSES ...], --classes CLASSES [CLASSES ...]
                        Space separated list of strings without ''
  -t THRESHOLD, --threshold THRESHOLD
                        Proportion of frames to be saved (1.0: saves all frames)
  -s, --show_images     Shows the saved images.

```


# Objectes de la llibreria
### Image

Imatge base que serviran tots els datasets del paquet. Hi ha vàries versions especialitzades que hereten d'aquesta.  
Com ara son YoloImage, VIAImage, BlenderImage. Es tracta d'un paquet en evolució:  

Proxims passos:

- Generalitzar YoloImage, VIAImage, BlenderImage per fer pujar funcionalitats cap a Image

### Region

Classe destinada a representar o be etiquetes o be deteccions sobre una imatge. Les imatges contindran llistes de Regions.

### BlenderDataset

Dataset destinat a interpretar i carregar a memoria les imatges generades per vision_blender.

# Versions

## v0.0.2
Incorporació del paquet ds_data.dataset que inclou objectes estándar per entrenament de xarxes.  

## v0.2.0
vision_blender (versió DS) ja genera les bboxes directament, per tant s'adapta el BlenderDataset i ImageDataset per 
les noves estructures de dades

## v0.9.0
Incorporació de ExtendedBbox, amb informació de càmera, posició en món real i timestamp


# Pujar paquet al server

```
python setup.py sdist bdist_wheel
twine upload --repository-url <URL_servidor> dist/*
```


# Per descarregar imatges d'internet (bing)

* Install/update Chrome: https://www.google.com/chrome/
* Install/update chromedriver: https://chromedriver.chromium.org/
* Modificar ds/dataset/bing_cfgs/generic.json
* python ds/datataset/bing_scraper.py -cf ds/dataset/bing_cfgs/generic.json