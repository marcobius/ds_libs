"""

    9/11/19
    DeepSolutions
    David Perez
    
    
"""

import requests
import cv2
import numpy as np
import time
import threading
import logging

LOGGER = logging.getLogger(__name__)
#
# host = cfg['monitoring']['carbon_host']
# port = cfg['monitoring']['graphite_api_port']
# project_prefix = cfg['monitoring']['prefix']

def get_ocupation_cv2_img(host, port, project_prefix, w=400, h=250, minutes=-20):
    url = 'http://' + host + ':'+ str(port)
    url = url + '/render?'
    # url = url + 'target=' + project_prefix + '*.hist.upper&from={}minutes&format=png&width={}&height={}'.format(minutes, w, h)
    url = url + 'target=' + project_prefix + '*.upper&from={}minutes&format=png&width={}&height={}'.format(minutes, w, h)
    url = url + '&fontSize=13'
    url = url + '&lineWidth=5'
    url = url + '&bgcolor=grey&fgcolor=000000'
    # url = url + '&target=movingAverage(portBCN.cam244.*.upper%2C 300)'
    #  http://localhost:8080/render?target=portBCN.cam244.*.upper&from=-20minutes&format=png&width=400&height=250&fontSize=20&lineWidth=5&bgcolor=grey
    # http://localhost:8080/render?target=portBCN.cam244.*.upper&from=-20minutes&format=png&width=400&height=250&fontSize=13&lineWidth=5&bgcolor=grey&fgcolor=000000&target=movingAverage(portBCN.cam244.*.upper%2C 10)
    try:
        r = requests.get(url)
        if r.status_code == 200:
            arr = np.asarray(bytearray(r.content), dtype=np.uint8)
            img = cv2.imdecode(arr, -1)  # 'Load it as it is'
            return img
    except:
        LOGGER.error("Error de conexion al graphite (servidor de gráficas)")

    return None

# http://localhost:8080/?showTarget=alias(portBCN.cam0.center.upper%2C%22CENTRO%22)&showTarget=maxSeries(portBCN.cam0.center.upper)&width=588&height=310&fontSize=18&lineWidth=5&target=alias(portBCN.cam0.center.upper%2C%22CENTRO%22)&target=alias(portBCN.cam0.left.upper%2C%20%22LEFT%22)&target=alias(portBCN.cam0.right.upper%2C%20%22DCHA%22)



class GraphRender:
    def __init__(self, host, port, project_prefix):
        # host = cfg['monitoring']['carbon_host']
        # port = cfg['monitoring']['graphite_api_port']
        # project_prefix = cfg['monitoring']['prefix']
        self.host = host
        self.port = port
        self.project_prefix = project_prefix
        self.img = None
        self.fin = False
        t = threading.Thread(target=self.worker, args=())
        t.daemon = True
        self.enabled = False
        t.start()

    def worker(self):
        while not self.fin:
            if self.enabled:
                self.img = get_ocupation_cv2_img(self.host, self.port, self.project_prefix)
            time.sleep(1)

    def stop(self):
        self.fin = True

    def get_graph(self):
        return self.img






