"""

    7/10/19
    DeepSolutions
    David Perez
    
    Driver para exportar métricas a un server graphite.
    docker run -d --name graphite --restart=always -p 8080:80 -p 2003-2004:2003-2004 -p 2023-2024:2023-2024 -p 8125:8125/udp -p 8126:8126 graphiteapp/graphite-statsd

"""
import socket
import time
import logging
import datetime
import graphyte

LOGGER = logging.getLogger(__name__)

from monitoring.influxdb_threaded_client import Sender as infDBSender
###############################################################################
####              INFLUXDB (usada para series con fecha antigua)
###############################################################################

def database_exists(client, dbname):
    list = client.get_list_database()
    for elem in list:
        if elem['name']==dbname:
            return True
    return False

class ThreadedInfluxDBMetricsWriterConfig(object):
    def __init__(self, use_video_time=True,
                        influxdb_host='localhost',
                        influxdb_port=8086,
                        influxdb_user='user',
                        influxdb_pass='password',
                        influxdb_database='ds_metricas',
                        carbon_host='localhost',
                        carbon_port=2003):
        self.use_video_time = use_video_time
        self.influxdb_host = influxdb_host
        self.influxdb_port = influxdb_port
        self.influxdb_user = influxdb_user
        self.influxdb_pass = influxdb_pass
        self.influxdb_database = influxdb_database
        self.carbon_host = carbon_host
        self.carbon_port = carbon_port


class ThreadedInfluxDBMetricsWriter(object):
    def __init__(self, config=ThreadedInfluxDBMetricsWriterConfig()):
        self.config = config
        self.enabled = True
        if config.use_video_time:
            self.sender = infDBSender(config.influxdb_host,
                                      config.influxdb_port,
                                      config.influxdb_user,
                                      config.influxdb_pass,
                                      config.influxdb_database,
                                      interval=None # un int provoca que trabaje en thread
                                      )
        else:
            self.sender = graphyte.Sender(config.carbon_host, port=config.carbon_port, interval=5) # 5 segundos

    def w(self, metric_path, value, timestamp=None, tags={}):
        self.send(metric_path, value, timestamp, tags=tags)

    def send(self, metric_path, value, timestamp=None, tags={}):
        if self.enabled:
            if self.config.use_video_time:
                tags = {'id': metric_path}
                # date = datetime.datetime.fromtimestamp(float(timestamp))
                self.sender.send('ds_tracker_metricas', float(value), timestamp=timestamp, tags=tags)
            else:
                self.sender.send(metric_path, value, timestamp=timestamp, tags=tags)
            if timestamp is None:
                LOGGER.debug('Message Sent: {} (date: {}, value:{})'.format(metric_path, timestamp, value))
            else:
                LOGGER.debug('Message Sent: {} (date: {}, value:{})'.format(metric_path,
                                                                            datetime.datetime.fromtimestamp(timestamp).strftime("%Y-%m-%dT%H:%M:%SZ"),
                                                                            value))

    def stop(self):
        # indicate that the thread should be stopped
        self.sender.stop()


###############################################################################
####              GRAPHITE (CARBON)
###############################################################################

class GraphiteMetricsWriter(object):
    def __init__(self, carbon_host='localhost', carbon_port=2003):
        self.enabled = True
        self.CARBON_SERVER = carbon_host
        self.CARBON_PORT = carbon_port

    def w(self, metric_path, value, timestamp=None):
        if timestamp is None:
            timestamp = int(time.time())
        message = '%s %s %d\n' % (metric_path, value, timestamp)
        sock = socket.socket()
        sock.connect((self.CARBON_SERVER, self.CARBON_PORT))
        sock.sendall(message.encode())
        sock.close()
        LOGGER.debug('Message Sent: {} (date: {})'.format(message, datetime.datetime.fromtimestamp(timestamp)))



class ThreadedGraphiteMetricsWriter(object):
    def __init__(self, carbon_host='localhost', carbon_port=2003, queueSize=128, interval=5): # 5 segundos
        self.enabled = True
        self.sender = graphyte.Sender(carbon_host, port=carbon_port, interval=interval)

    def w(self, metric_path, value, timestamp=None):
        self.send(metric_path, value, timestamp)

    def send(self, metric_path, value, timestamp=None):
        if self.enabled:
            self.sender.send(metric_path, value, timestamp=timestamp)

    def stop(self):
        # indicate that the thread should be stopped
        self.sender.stop()

class Timer(object):
    def __init__(self, metric_path, writer=ThreadedGraphiteMetricsWriter(), enabled=True):
        self.init_time = 0
        self.sender = writer
        self.metric_path = metric_path
        self.enabled = enabled

    def start(self):
        self.init_time = time.time()

    def stop(self):
        if self.enabled:
            self.sender.send(self.metric_path, time.time()-self.init_time)

    def __enter__(self):
        self.start()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.stop()

class Gauge(object):
    def __init__(self, metric_path, writer=ThreadedGraphiteMetricsWriter(), enabled=True):
        self.value = 0
        self.sender = writer
        self.metric_path = metric_path
        self.enabled = enabled

    def set(self, value, timestamp=None):
        if self.enabled:
            self.sender.send(self.metric_path, value, timestamp)
