"""

    12/2/20
    DeepSolutions
    David Perez
    
    
"""

import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="monitoring-deepsolutions", # Replace with your own username
    version="0.0.7",
    author="DeepSolutions",
    author_email="bcn@deepsolutions.io",
    description="Functions and Classes useful for export metrics to different DB (i.e. Graphite)",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/marcobius/ds_libs",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.5',
    install_requires=[
        'opencv-python>=4.2.0',
        'opencv-contrib-python>=4.2.0',
        'numpy>=1.18.1'
    ],
)