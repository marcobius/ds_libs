# Monitoring utils

Functions and Classes useful for reading and exporting metrics to different DB (i.e. Graphite).

## monitor_metrics package

Includes exporters and various objects used to recolect the data (metrics).

### class ThreadedInfluxDBMetricsWriter(object)
Class used as threaded buffered writer to send metrics to an InfluxDB.

### class GraphiteMetricsWriter(object)
Class used as a writer to send metrics to an InfluxDB.


### class ThreadedGraphiteMetricsWriter(object)
Class used as threaded buffered writer to send metrics to a Graphite DB.


### class Timer(object)
Class capable of taking meditions of time passed between two events. It is also to send the metrics to a timestamp DB server by itself.

### class Gauge(object)
Class used to record the status of a varialble (i.e. speed) over time. It is also to send the metrics to a timestamp DB server by itself.
